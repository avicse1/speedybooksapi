<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessExpense extends Model {

    protected $table = 'business_expenses';

    protected $fillable = array('business_profile_id', 'tax_rate_id', 'expense_no', 'account_id', 'reconcile_id', 'date', 'reference', 'due_date', 'payment_term', 'discount', 'type', 'tax_amount', 'attachment', 'amount','status', 'banking_type', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'business_profile_id' => 'required',
        'date' => 'required',
        'expense_no' => 'required',
        'payment_term' => 'required',
        'amount' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function businessExpenseItem()
    {
        return $this->hasMany('\App\BusinessExpenseItem');
    }

    public function vendor()
    {
        return $this->belongsToMany('\App\Vendor', 'business_expense_vendors', 'business_expense_id', 'vendor_id');
    }

    public function businessExpenseVendor()
    {
        return $this->hasMany('\App\BusinessExpenseVendor');
    }

    public function notes()
    {
        return $this->morphToMany('\App\Note', 'taggable');
    }

    public function businessExpenseConfiguration()
    {
        return $this->hasOne('\App\BusinessExpenseConfiguration');
    }

    public function createdBy()
    {
        return $this->belongsTo('\App\User', 'created_by', 'id');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function taxRate()
    {
        return $this->belongsTo('\App\TaxRate');
    }

    public function businessReconcile()
    {
        return $this->belongsTo('\App\BusinessReconcile');
    }
}