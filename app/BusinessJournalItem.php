<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessJournalItem extends Model {

    protected $table = 'business_journal_items';

    protected $fillable = array('business_journal_id', 'account_id', 'reconcile_id', 'description', 'debit', 'credit');

    public $timestamps = false;

    public static $rules = [
        'business_journal_id' => 'required',
        'account_id' => 'required',
        'description' => 'required',
        'debit' => 'required',
        'credit' => 'required',
    ];

    public function businessJournal()
    {
        return $this->belongsTo('\App\BusinessJournal');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function businessReconcile()
    {
        return $this->belongsTo('\App\BusinessReconcile');
    }
}