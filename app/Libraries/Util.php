<?php  namespace App\Libraries;

use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\Guard;

class Util
{
    public static function sms($number,$message){

        $username = env('SMS_USERNAME');
        $sender_id = env('SMS_SENDER');
        $password = env('SMS_PASSWORD');

        $url = env('SMS_URL');

        try {
            // create a client
            $client = new \GuzzleHttp\Client();

            // set up and run the request
            $res = $client->request('GET', $url, [
                'query' => ['username' => $username,
                    'password' => $password,
                    'sender' => $sender_id,
                    'sendto' => '91'.$number ,
                    'message' => $message]
            ]);

            \Log::info($number .' - '. $message);
            // assuming it returns a status code
            return $res->getStatusCode();

        } catch (\GuzzleHttp\Exception\ClientException $e) {
//            \Log::info($e);
            return '404 Not Found';
        }
    }

    /**
     * Function for uploading attachment
     * take $file,$destination_file,$prefix,$dir as argument.
     *
     * @return upload file path.
     */

    public static function upload_file($file,$destination_file,$prefix=null,$dir=null)
    {
        // define empty path.
        $path = '';

        // check if $file exists.
        if($file)
        {
            // check dir is not empty.
            if(!empty($dir))
            {
                //define $dir path into $path.
                $path = $dir;
            }
            else
            {
                //define path.
                $path = 'upload'.DIRECTORY_SEPARATOR.date('Y').DIRECTORY_SEPARATOR.date('m').DIRECTORY_SEPARATOR.date('d');
            }

            //check $prefix is not empty.
            if(!empty($prefix))
            {
                //define path with prefix.
                $path = $prefix.DIRECTORY_SEPARATOR.$path;
            }

            // Upload file in a public path folder
            $upload_success = $file->move(public_path().DIRECTORY_SEPARATOR.$path,$destination_file);
            $upload_success = str_replace(public_path().DIRECTORY_SEPARATOR,'',$upload_success);

            // return upload file path.
            return str_replace('\\', '/', $upload_success);
        }
    }

    /**
     * function for generating unique file name based on timestamp.
     * @param $ext fileextension.
     *
     * @return string filename.
     */

    public static function rand_filename($ext=null)
    {
        if($ext)
        {
            return time().'-'.'1'.rand(0,999999999).'.'.$ext;
        }
        else
        {
            return time().'-'.'1'.rand(0,999999999);
        }
    }

    /**
     * Get Month report for expense
     * @param first_date, last_date, object
     *
     * @return array
     */

    public static function getMonthChart($monthFirstDate, $monthLastDate, $expenseMonthlyReports)
    {
        $firstDay = \Carbon\Carbon::parse($monthFirstDate)->format('d');
        $lastDay = \Carbon\Carbon::parse($monthLastDate)->format('d');

        $monthReport = array();

        for($i=$firstDay; $i<=$lastDay; $i++) {
            $monthReport[$i] = 0;
            if(!empty($expenseMonthlyReports)) {
                foreach ($expenseMonthlyReports as $expenseMonthlyReport) {
                    $due_date = \Carbon\Carbon::parse($expenseMonthlyReport->due_date)->format('d');
                    if ($i == $due_date) {
                        $monthReport[$i] = (int)$expenseMonthlyReport->amount;
                    }
                }
            }
        }

        return $monthReport;
    }


    /**
     * Get Year report for expense
     * @param object
     *
     * @return array
     */

    public static function getYearChart($expenseYearlyReports)
    {
        $yearReport = array();

        for($i=1; $i<=12; $i++) {
            $yearReport[$i] = 0;
            if(!empty($expenseYearlyReports)) {
                foreach ($expenseYearlyReports as $expenseYearlyReport) {
                    $due_date = \Carbon\Carbon::parse($expenseYearlyReport->due_date)->format('m');
                    if ($i == $due_date) {
                        $yearReport[$i] = (int)$expenseYearlyReport->amount;
                    }
                }
            }
        }

        return $yearReport;
    }

    /**
     * Get weekly report for expense
     * @param first_date, last_date, object
     *
     * @return array
     */

    public static function getWeekChart($monthFirstDate, $monthLastDate, $expenseWeeklyReports)
    {
        $firstDay = \Carbon\Carbon::parse($monthFirstDate)->format('d');
        $lastDay = \Carbon\Carbon::parse($monthLastDate)->format('d');

        $weekReport = array();

        for($i=$firstDay; $i<=$lastDay; $i++) {
            $weekReport[$i] = 0;
            if(!empty($expenseWeeklyReports)) {
                foreach ($expenseWeeklyReports as $expenseWeeklyReport) {
                    $due_date = \Carbon\Carbon::parse($expenseWeeklyReport->due_date)->format('d');
                    if ($i == $due_date) {
                        $weekReport[$i] = (int)$expenseWeeklyReport->amount;
                    }
                }
            }
        }

        return $weekReport;
    }

    public static function getMonthList($from, $to, $headerArray)
    {
        $monthName = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        $i = date("Ym", strtotime($from));
        $month = [];
        while($i <= date("Ym", strtotime($to))){
            $month[] = substr($i, -2);;
            if(substr($i, 4, 2) == "12") {
                $i = (date("Y", strtotime($i . "01")) + 1) . "01";
            } else {
                $i++;
            }
        }
        foreach ($month as $key=>$value)
        {
            foreach ($monthName as $k=>$v) {
                if($k + 1 == $value) {
                    $headerArray[] = $v;
                }
            }
        }

        return $headerArray;
    }
}