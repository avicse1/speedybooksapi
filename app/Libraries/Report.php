<?php  namespace App\Libraries;

use App\Account;
use App\AccountBalance;
use App\BusinessInvoice;
use App\BusinessItem;
use App\BusinessExpense;
use App\BusinessJournal;
use App\TaxRate;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\Guard;

class Report
{
    /**
     * Customer invoice Report
     *
     * @param $input
     * @return array
     */
    public static function invoiceReport($input)
    {
        $dateType = $input['date_type'];
        $from = $input['from'];
        $to = $input['to'];
        $status = $input['status'];
        $orderBy = $input['order_by'];
        $businessProfileId = $input['business_profile_id'];

        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE');

        if(!empty($dateType) && $dateType == 'invoice_date') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }

        if(!empty($dateType) && $dateType == 'due_date') {
            $invoices = $invoices->where('payment_term', '!=', 'Paid');
        }

        if(!empty($status) && $status != 'All') {
            $invoices = $invoices->where('status', $status);
        }

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($orderBy)) {
            $invoices = $invoices->orderBy($orderBy);
        }

        $invoices = $invoices->get();

        $total = 0;
        foreach ($invoices as $invoice) {
            $total = $total +  $invoice->amount;
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['invoices' => $invoices, 'total' => $total, 'date' => $date];
    }

    /**
     * Sales item Report
     *
     * @param $input
     * @return array
     */
    public static function salesByItemsReport($input)
    {

        $from = $input['from'];
        $to = $input['to'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];

        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        /*if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }*/

        $invoices = $invoices->get();

        $salesByItems = array();

        foreach ($invoices as $invoice) {
            $invoiceItems = $invoice->businessInvoiceItem;

            foreach ($invoiceItems as $invoiceItem) {
                $Item = BusinessItem::find($invoiceItem->business_item_id);
                $salesByItems[] = ['id' => $Item['id'],
                    'name_code' => $Item['item_code'] . '-'. $Item['name'],
                    'current_price' => $Item['selling_price'],
                    'quantity' => $invoiceItem['quantity'],
                    'amount' => $invoiceItem['amount'],
                ];
            }
        }

        $items = array();
        $totalAmount = 0;
        $totalQuantity = 0;
        foreach ($salesByItems as $salesByItem) {

            if(!isset($items[$salesByItem['id']])) {
                $totalAmount = $totalAmount + $salesByItem['amount'];
                $totalQuantity = $totalQuantity + $salesByItem['quantity'];

                $items[$salesByItem['id']] = ['name_code'=> $salesByItem['name_code'], 'current_price' => $salesByItem['current_price'], 'quantity' => $salesByItem['quantity'], 'total' => $salesByItem['amount']];

            } else {
                $totalAmount = $totalAmount + $salesByItem['amount'];
                $totalQuantity = $totalQuantity + $salesByItem['quantity'];

                $items[$salesByItem['id']]['quantity'] = $items[$salesByItem['id']]['quantity'] + $salesByItem['quantity'];
                $items[$salesByItem['id']]['total'] = $items[$salesByItem['id']]['total'] + $salesByItem['amount'];
            }
        }
        $items =  array_values($items);
        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['items' => $items, 'totalAmount' => $totalAmount, 'totalQuantity' => $totalQuantity, 'date' => $date];
    }

    /**
     *Profit and Loss Report
     *
     * @param $input
     * @return array
     */
    public static function profitandLossReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $basis = $input['basis'];
        $type = $input['type'];
        $invoiceList = array();
        $expenseList = array();
        $businessProfileId = $input['business_profile_id'];
        $totalIncome = 0;
        $totalExpense = 0;

        $profitandLoss = array();
        $incomeArray = array();
        $expenseArray = array();

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    $account = Account::find($item->account_id);
                    $parentAccount = Account::find($account->parent_id);

                    if(!empty($parentAccount)) {
                        if (!array_key_exists($parentAccount->name, $incomeArray)) {
                            $incomeArray[$parentAccount->name] = array();
                        }

                        if (!array_key_exists($account->name, $incomeArray[$parentAccount->name])) {
                            $incomeArray[$parentAccount->name][$account->name] = $item->amount;
                            $totalIncome += $item->amount;
                        } else {
                            $incomeArray[$parentAccount->name][$account->name] += $item->amount;
                            $totalIncome += $item->amount;
                        }

                        array_push($invoiceList, ['id' => $invoice->id,
                            'date' => \Carbon\Carbon::parse($invoice->created_at)->format('Y-m-d'),
                            'transactionType' => 'Invoice',
                            'no' => $invoice->invoice_no,
                            'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                            'account' => $account->name,
                            'amount' => $item->amount,
                        ]);
                    }
                }
            }
        }

        //get all expenses

        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }

        $expenses = $expenses->get();
        $expenseNotIncludeAccount = ['Drawings', 'Office Equipment', 'Vehicle', 'Investment'];
        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    $account = Account::find($item->account_id);
                    if(!in_array($account->name, $expenseNotIncludeAccount)) {
                        $parentAccount = Account::find($account->parent_id);
                        if(!empty($parentAccount)) {
                            if (!array_key_exists($parentAccount->name, $expenseArray)) {
                                $expenseArray[$parentAccount->name] = array();
                            }

                            if (!array_key_exists($account->name, $expenseArray[$parentAccount->name])) {
                                $expenseArray[$parentAccount->name][$account->name] = $item->amount;
                                $totalExpense += $item->amount;
                            } else {
                                $expenseArray[$parentAccount->name][$account->name] += $item->amount;
                                $totalExpense += $item->amount;
                            }

                            array_push($expenseList, ['id' => $expense->id,
                                'date' => \Carbon\Carbon::parse($expense->created_at)->format('Y-m-d'),
                                'transactionType' => 'Expense',
                                'no' => '',
                                'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                'account' => $account->name,
                                'amount' => $item->amount,
                            ]);
                        }
                    }
                }
            }
        }

        $journalIncome = ['Sales', 'Interest Income', 'Misc. Income', 'Sales Return'];
        $journalExpense = ['Advertising & Marketing', 'Meals & Rent', 'Travel & Vehicle Expense', 'Insurance', 'Entertainment',
            'Office Expense', 'Legal Expense', 'Telephone & Internet', 'Subscription', 'Salary and wages',
            'Freight & Delivery', 'Loan & Borrowing Interest', 'BadDebts', 'Depreciation Expense', 'Less Accumulated depreciation'
        ];
        $journals = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');
        if(!empty($from) && !empty($to)) {
            $journals = $journals->whereBetween('created_at', [$from, $to]);
        }

        $journals = $journals->get();

        $cashBankEq = ['Cash Account', 'Bank Account', 'Cash & Cash Equivalents'];
        foreach ($journals as $journal) {
            $journalItems = $journal->businessJournalItem;
            foreach ($journalItems as $journalItem) {
                if($journalItem->account_id > 0) {
                    $account = Account::find($journalItem->account_id);
                    $parentAccount = Account::find($account->parent_id);
                    if (!empty($parentAccount)) {
                        if (in_array($account->name, $journalIncome)) {
                            if ($journalItem->credit > 0) {
                                if (!array_key_exists($parentAccount->name, $incomeArray)) {
                                    $incomeArray[$parentAccount->name] = array();
                                }

                                if (!array_key_exists($account->name, $incomeArray[$parentAccount->name])) {
                                    $incomeArray[$parentAccount->name][$account->name] = $journalItem->credit;
                                    $totalIncome += $journalItem->credit;
                                } else {
                                    $incomeArray[$parentAccount->name][$account->name] += $journalItem->credit;
                                    $totalIncome += $journalItem->credit;
                                }

                                array_push($invoiceList, ['id' => $journal->id,
                                    'date' => \Carbon\Carbon::parse($journal->created_at)->format('Y-m-d'),
                                    'transactionType' => 'Journal',
                                    'no' => '',
                                    'name' => '',
                                    'account' => $account->name,
                                    'amount' => $journalItem->credit,
                                ]);
                            }
                        }

                        if (in_array($account->name, $journalExpense)) {
                            if ($journalItem->debit > 0) {
                                if (!array_key_exists($parentAccount->name, $expenseArray)) {
                                    $expenseArray[$parentAccount->name] = array();
                                }

                                if (!array_key_exists($account->name, $expenseArray[$parentAccount->name])) {
                                    $expenseArray[$parentAccount->name][$account->name] = $journalItem->debit;
                                    $totalExpense += $journalItem->debit;
                                } else {
                                    $expenseArray[$parentAccount->name][$account->name] += $journalItem->debit;
                                    $totalExpense += $journalItem->debit;
                                }

                                array_push($expenseList, ['id' => $journal->id,
                                    'date' => \Carbon\Carbon::parse($journal->created_at)->format('Y-m-d'),
                                    'transactionType' => 'Journal',
                                    'no' => '',
                                    'name' => '',
                                    'account' => $account->name,
                                    'amount' => $journalItem->debit,
                                ]);
                            }
                        }
                    }
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        $profitandLoss['income'] = !empty($incomeArray) ? $incomeArray : array();
        $profitandLoss['expense'] = !empty($expenseArray) ? $expenseArray : array();

        return ['profitandLoss' => $profitandLoss,
            'date' => $date,
            'totalIncome' => $totalIncome,
            'totalExpense' => $totalExpense,
            'invoiceList' => $invoiceList,
            'expenseList' => $expenseList];
    }

    /**
     * Balance Sheet Report
     *
     * @param $input
     * @return array
     */
    public static function balanceSheetReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];
        $balanceSheetAssets = array();
        $balanceSheetLiabilities = array();
        $balanceSheetOwners = array();

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }

        $invoices = $invoices->get();

        $totalAssets = 0;
        $totalLiabilities = 0;
        $totalOwners = 0;
        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;

            if(!empty($items)) {
                foreach ($items as $item) {
                    if($invoice->payment_term == 'Paid') {
                        if(!empty($invoice->account_id) && $invoice->account_id != 0) {
                            $account = Account::find($invoice->account_id);
                            $parentAccount = Account::find($account->parent_id);
                            if(!empty($parentAccount)) {
                                if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                    $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }

                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $item->amount;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $item->amount;
                                    $totalAssets += $item->amount;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] += $item->amount;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $item->amount;
                                    $totalAssets += $item->amount;
                                }

                                if (!empty($item->account_id)) {
                                    $equityAccount = Account::find($item->account_id);
                                    if (!empty($equityAccount) && $equityAccount->name == 'Capital & Equity') {
                                        $EquparentAccount = Account::find($equityAccount->parent_id);
                                        if (!array_key_exists($EquparentAccount->name, $balanceSheetOwners)) {
                                            $balanceSheetOwners[$EquparentAccount->name] = array('account' => [], 'total' => 0);
                                        }

                                        if (!array_key_exists($account->name, $balanceSheetOwners[$EquparentAccount->name]['account'])) {
                                            $balanceSheetOwners[$EquparentAccount->name]['account'][$equityAccount->name] = $item->amount;
                                            $balanceSheetOwners[$EquparentAccount->name]['total'] += $item->amount;
                                            $totalOwners += $item->amount;
                                        } else {
                                            $balanceSheetOwners[$EquparentAccount->name]['account'][$equityAccount->name] += $item->amount;
                                            $balanceSheetOwners[$EquparentAccount->name]['total'] += $item->amount;
                                            $totalOwners += $item->amount;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if($invoice->payment_term != 'Paid') {
                        if (!array_key_exists('Current Assets', $balanceSheetAssets)) {
                            $balanceSheetAssets['Current Assets'] = array('account' => [], 'total' => 0);
                        }

                        if (!array_key_exists('Account Receivables ( Debtors)', $balanceSheetAssets['Current Assets']['account'])) {
                            $balanceSheetAssets['Current Assets']['account']['Account Receivables ( Debtors)'] = $item->amount;
                            $balanceSheetAssets['Current Assets']['total'] += $item->amount;
                            $totalAssets += $item->amount;
                        } else {
                            $balanceSheetAssets['Current Assets']['account']['Account Receivables ( Debtors)'] += $item->amount;
                            $balanceSheetAssets['Current Assets']['total'] += $item->amount;
                            $totalAssets += $item->amount;
                        }
                    }
                }
            }
        }

        //get all Expenses

        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }

        $expenses = $expenses->get();

        $accountPayableList = ['Advertising & Marketing', 'Meals & Rent', 'Travel & Vehicle Expense', 'Insurance', 'Entertainment',
            'Office Expense', 'Legal Expense', 'Telephone & Internet', 'Subscription', 'Reserve', 'Cost of goods sold', 'Purchase',
            'Freight & Delivery'
        ];
        $assestAccount = ['Office Equipment', 'Vehicle', 'Investment'];

        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;

            if(!empty($items)) {
                foreach ($items as $item) {
                    if($expense->payment_term == 'Paid') {
                        if(!empty($expense->account_id) && $expense->account_id != 0) {
                            $account = Account::find($expense->account_id);
                            $parentAccount = Account::find($account->parent_id);
                            if(!empty($parentAccount)) {
                                if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                    $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }

                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $item->amount;
                                    $balanceSheetAssets[$parentAccount->name]['total'] -= $item->amount;
                                    $totalAssets -= $item->amount;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] -= $item->amount;
                                    $balanceSheetAssets[$parentAccount->name]['total'] -= $item->amount;
                                    $totalAssets -= $item->amount;
                                }
                            }
                        }

                        if(!empty($item->account_id)) {
                            $equityAccount = Account::find($item->account_id);
                            if(!empty($equityAccount) && $equityAccount->name == 'Drawings') {
                                $EquparentAccount = Account::find($equityAccount->parent_id);
                                if(!empty($EquparentAccount)) {
                                    if (!array_key_exists($EquparentAccount->name, $balanceSheetOwners)) {
                                        $balanceSheetOwners[$EquparentAccount->name] = array('account' => [], 'total' => 0);
                                    }

                                    if (!array_key_exists($account->name, $balanceSheetOwners[$EquparentAccount->name]['account'])) {
                                        $balanceSheetOwners[$EquparentAccount->name]['account'][$equityAccount->name] = $item->amount;
                                        $balanceSheetOwners[$EquparentAccount->name]['total'] -= $item->amount;
                                        $totalOwners -= $item->amount;
                                    } else {
                                        $balanceSheetOwners[$EquparentAccount->name]['account'][$equityAccount->name] += $item->amount;
                                        $balanceSheetOwners[$EquparentAccount->name]['total'] -= $item->amount;
                                        $totalOwners -= $item->amount;
                                    }
                                }
                            }
                        }
                    }

                    if($expense->payment_term != 'Paid') {
                        $account = Account::find($item->account_id);
                        if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                            $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                        }
                        if(!empty($account) && in_array($account->name, $accountPayableList)) {
                            if (!array_key_exists('Account payable ( creditors)', $balanceSheetLiabilities['Liabilities']['account'])) {
                                $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] = $item->amount;
                                $balanceSheetLiabilities['Liabilities']['total'] += $item->amount;
                                $totalLiabilities += $item->amount;
                            } else {
                                $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] += $item->amount;
                                $balanceSheetLiabilities['Liabilities']['total'] += $item->amount;
                                $totalLiabilities += $item->amount;
                            }
                        }

                        if (!empty($account) && $account->name == 'Salary and wages') {
                            $balanceSheetLiabilities['Liabilities']['account']['Salary Payable'] = $item->amount;
                            $balanceSheetLiabilities['Liabilities']['total'] += $item->amount;
                            $totalLiabilities += $item->amount;
                        }
                        if (!empty($account) && $account->name == 'Salary Payable') {
                            $balanceSheetLiabilities['Liabilities']['account']['Salary Payable'] = $item->amount;
                            $balanceSheetLiabilities['Liabilities']['total'] += $item->amount;
                            $totalLiabilities += $item->amount;
                        }

                        if(!empty($account) && in_array($account->name, $assestAccount)) {
                            if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                                $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                            }

                            if (!array_key_exists('Account payable ( creditors)', $balanceSheetLiabilities['Liabilities']['account'])) {
                                $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] = $item->amount;
                                $balanceSheetLiabilities['Liabilities']['total'] += $item->amount;
                                $totalLiabilities += $item->amount;
                            } else {
                                $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] += $item->amount;
                                $balanceSheetLiabilities['Liabilities']['total'] += $item->amount;
                                $totalLiabilities += $item->amount;
                            }

                            $parentAccount = Account::find($account->parent_id);

                            if(!empty($parentAccount)) {
                                if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                    $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }

                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $item->amount;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $item->amount;
                                    $totalAssets += $item->amount;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] += $item->amount;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $item->amount;
                                    $totalAssets += $item->amount;
                                }
                            }
                        }
                    }
                }
            }
        }

        $journalIncome = ['Cash Account', 'Account Receivables ( Debtors)', 'Bank Account', 'Cash & Cash Equivalents', 'Investment', 'Baddebts', 'AR Allowance'];
        $journalExpense = ['Cash Account', 'Account payable ( creditors)', 'Salary Payable', 'Bank Account', 'Cash & Cash Equivalents', 'Investment', 'Baddebts'];
        $journalFixedAssets = ['Office Equipment', 'Vehicle', 'Buildings', 'Depreciation Expense', 'Less Accumulated depreciation'];
        $journalEntries = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');
        if(!empty($from) && !empty($to)) {
            $journalEntries = $journalEntries->whereBetween('created_at', [$from, $to]);
        }

        $journalEntries = $journalEntries->get();

        foreach ($journalEntries as $journalEntry) {
            $journalItems = $journalEntry->businessJournalItem;

            foreach ($journalItems as $journalItem) {
                if($journalItem->account_id > 0) {
                    $account = Account::find($journalItem->account_id);
                    $parentAccount = Account::find($account->parent_id);

                    if (!empty($parentAccount)) {
                        if (in_array($account->name, $journalIncome)) {
                            if ($basis != 'cash') {
                                if ($account->name == 'Account Receivables ( Debtors)') {
                                    if ($journalItem->debit > 0) {
                                        if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                            $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                        }

                                        if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                            $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->debit;
                                            $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                            $totalAssets += $journalItem->debit;
                                        } else {
                                            $balanceSheetAssets[$parentAccount->name]['account'][$account->name] += $journalItem->debit;
                                            $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                            $totalAssets += $journalItem->debit;
                                        }
                                    } else {
                                        if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                            $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                        }

                                        if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                            $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->credit;
                                            $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                            $totalAssets -= $journalItem->credit;
                                        } else {
                                            $balanceSheetAssets[$parentAccount->name]['account'][$account->name] -= $journalItem->credit;
                                            $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                            $totalAssets -= $journalItem->credit;
                                        }
                                    }
                                }
                            }
                            if ($account->name == 'Cash Account' || $account->name == 'Bank Account' || $account->name =='Cash & Cash Equivalents' || $account->name == 'AR Allowance' || ($account->name == 'Baddebts' && $basis != 'cash')) {
                                if ($journalItem->debit > 0) {
                                    if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                        $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                    }

                                    if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                        $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->debit;
                                        $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                        $totalAssets += $journalItem->debit;
                                    } else {
                                        $balanceSheetAssets[$parentAccount->name]['account'][$account->name] += $journalItem->debit;
                                        $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                        $totalAssets += $journalItem->debit;
                                    }
                                }
                            }
                        }

                        if (in_array($account->name, $journalExpense)) {
                            if ($basis != 'cash') {
                                if ($account->name == 'Account payable ( creditors)') {
                                    if ($journalItem->credit > 0) {
                                        if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                                            $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                                        }
                                        if (!array_key_exists('Account payable ( creditors)', $balanceSheetLiabilities['Liabilities']['account'])) {
                                            $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] = $journalItem->credit;
                                            $balanceSheetLiabilities['Liabilities']['total'] += $journalItem->credit;
                                            $totalLiabilities += $journalItem->credit;
                                        } else {
                                            $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] += $journalItem->credit;
                                            $balanceSheetLiabilities['Liabilities']['total'] += $journalItem->credit;
                                            $totalLiabilities += $journalItem->credit;
                                        }
                                    } else {
                                        if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                                            $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                                        }
                                        if (!array_key_exists('Account payable ( creditors)', $balanceSheetLiabilities['Liabilities']['account'])) {
                                            $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] = $journalItem->debit;
                                            $balanceSheetLiabilities['Liabilities']['total'] -= $journalItem->debit;
                                            $totalLiabilities -= $journalItem->debit;
                                        } else {
                                            $balanceSheetLiabilities['Liabilities']['account']['Account payable ( creditors)'] -= $journalItem->debit;
                                            $balanceSheetLiabilities['Liabilities']['total'] -= $journalItem->debit;
                                            $totalLiabilities -= $journalItem->debit;
                                        }
                                    }
                                }

                                if ($account->name == 'Salary Payable') {
                                    if ($journalItem->credit > 0) {
                                        if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                                            $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                                        }
                                        if (!array_key_exists('Salary Payable', $balanceSheetLiabilities['Liabilities']['account'])) {
                                            $balanceSheetLiabilities['Liabilities']['account']['Salary Payable'] = $journalItem->credit;
                                            $balanceSheetLiabilities['Liabilities']['total'] += $journalItem->credit;
                                            $totalLiabilities += $journalItem->credit;
                                        } else {
                                            $balanceSheetLiabilities['Liabilities']['account']['Salary Payable'] += $journalItem->credit;
                                            $balanceSheetLiabilities['Liabilities']['total'] += $journalItem->credit;
                                            $totalLiabilities += $journalItem->credit;
                                        }
                                    } else {
                                        if (!array_key_exists('Salary Payable', $balanceSheetLiabilities['Liabilities']['account'])) {
                                            $balanceSheetLiabilities['Liabilities']['account']['Salary Payable'] = $journalItem->debit;
                                            $balanceSheetLiabilities['Liabilities']['total'] -= $journalItem->debit;
                                            $totalLiabilities -= $journalItem->debit;
                                        } else {
                                            $balanceSheetLiabilities['Liabilities']['account']['Salary Payable'] -= $journalItem->debit;
                                            $balanceSheetLiabilities['Liabilities']['total'] -= $journalItem->debit;
                                            $totalLiabilities -= $journalItem->debit;
                                        }
                                    }
                                }
                            }
                            if ($account->name == 'Cash Account' || $account->name == 'Bank Account' || ($account->name == 'Baddebts' && $basis != 'cash')) {
                                if ($journalItem->credit > 0) {
                                    if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                        $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                    }

                                    if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                        $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->credit;
                                        $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                        $totalAssets -= $journalItem->credit;
                                    } else {
                                        $balanceSheetAssets[$parentAccount->name]['account'][$account->name] -= $journalItem->credit;
                                        $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                        $totalAssets -= $journalItem->credit;
                                    }
                                }
                            }
                        }

                        if (in_array($account->name, $journalFixedAssets)) {
                            if ($journalItem->debit > 0) {
                                if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                    $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }

                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->debit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                    $totalAssets += $journalItem->debit;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] += $journalItem->debit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                    $totalAssets += $journalItem->debit;
                                }
                            } else {
                                if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                    $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }

                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->credit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                    $totalAssets -= $journalItem->credit;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] -= $journalItem->credit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                    $totalAssets -= $journalItem->credit;
                                }
                            }
                            if ($account->name == 'Less Accumulated depreciation' ) {
                                if ($journalItem->credit > 0) {
                                    if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                        $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                    }

                                    if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                        $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->credit;
                                        $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                        $totalAssets -= $journalItem->credit;
                                    } else {
                                        $balanceSheetAssets[$parentAccount->name]['account'][$account->name] -= $journalItem->credit;
                                        $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                        $totalAssets -= $journalItem->credit;
                                    }
                                }
                            }
                        }

                        if ($account->name == 'Prepaid Expense') {
                            if ($journalItem->debit > 0) {
                                if (!array_key_exists($parentAccount->name, $balanceSheetAssets)) {
                                    $balanceSheetAssets[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }

                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->debit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                    $totalAssets += $journalItem->debit;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] += $journalItem->debit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] += $journalItem->debit;
                                    $totalAssets += $journalItem->debit;
                                }
                            } else {
                                if (!array_key_exists($account->name, $balanceSheetAssets[$parentAccount->name]['account'])) {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] = $journalItem->credit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                    $totalAssets += $journalItem->credit;
                                } else {
                                    $balanceSheetAssets[$parentAccount->name]['account'][$account->name] -= $journalItem->credit;
                                    $balanceSheetAssets[$parentAccount->name]['total'] -= $journalItem->credit;
                                    $totalAssets -= $journalItem->credit;
                                }
                            }
                        }

                        if ($account->name == 'Loans') {
                            if ($journalItem->credit > 0) {
                                if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                                    $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                                }
                                if (!array_key_exists($account->name, $balanceSheetLiabilities['Liabilities']['account'])) {
                                    $balanceSheetLiabilities['Liabilities']['account'][$account->name] = $journalItem->credit;
                                    $balanceSheetLiabilities['Liabilities']['total'] += $journalItem->credit;
                                    $totalLiabilities += $journalItem->credit;
                                } else {
                                    $balanceSheetLiabilities['Liabilities']['account'][$account->name] += $journalItem->credit;
                                    $balanceSheetLiabilities['Liabilities']['total'] += $journalItem->credit;
                                    $totalLiabilities += $journalItem->credit;
                                }
                            } else {
                                if (!array_key_exists('Liabilities', $balanceSheetLiabilities)) {
                                    $balanceSheetLiabilities['Liabilities'] = array('account' => [], 'total' => 0);
                                }
                                if (!array_key_exists($account->name, $balanceSheetLiabilities['Liabilities']['account'])) {
                                    $balanceSheetLiabilities['Liabilities']['account'][$account->name] = $journalItem->debit;
                                    $balanceSheetLiabilities['Liabilities']['total'] -= $journalItem->debit;
                                    $totalLiabilities -= $journalItem->debit;
                                } else {
                                    $balanceSheetLiabilities['Liabilities']['account'][$account->name] -= $journalItem->debit;
                                    $balanceSheetLiabilities['Liabilities']['total'] -= $journalItem->debit;
                                    $totalLiabilities -= $journalItem->debit;
                                }
                            }
                        }

                        if ($account->name == 'Capital & Equity') {
                            if ($journalItem->credit > 0) {
                                if (!array_key_exists($parentAccount->name, $balanceSheetOwners)) {
                                    $balanceSheetOwners[$parentAccount->name] = array('account' => [], 'total' => 0);
                                }
                                if (!array_key_exists($account->name, $balanceSheetOwners[$parentAccount->name]['account'])) {
                                    $balanceSheetOwners[$parentAccount->name]['account'][$account->name] = $journalItem->credit;
                                    $balanceSheetOwners[$parentAccount->name]['total'] += $journalItem->credit;
                                    $totalOwners += $journalItem->credit;
                                } else {
                                    $balanceSheetOwners[$parentAccount->name]['account'][$account->name] += $journalItem->credit;
                                    $balanceSheetOwners[$parentAccount->name]['total'] += $journalItem->credit;
                                    $totalOwners += $journalItem->credit;
                                }
                            }
                        }
                    }
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['balanceSheetAssets' => $balanceSheetAssets, 'balanceSheetLiabilities' => $balanceSheetLiabilities,
            'balanceSheetOwners' => $balanceSheetOwners,
            'date' => $date,
            'totalAssets' => $totalAssets,
            'totalLiabilities' => $totalLiabilities,
            'totalOwners' => $totalOwners
        ];
    }

    /**
     * Journal Report
     *
     * @param $input
     * @return array
     */
    public static function journalReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];
        $journalReport = array();
        $totalDebit = 0;
        $totalCredit = 0;

        // All invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'account', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }

        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            if($invoice->payment_term == 'Paid') {
                $account = Account::find($invoice->account_id);

                $cashAccount = ['id' => $invoice->id,
                    'date' =>  \Carbon\Carbon::parse($invoice->date)->toFormattedDateString(),
                    'type' => 'Payment',
                    'transactionType' => $invoice->type,
                    'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                    'account' => $account->name,
                    'debit' => $invoice->amount,
                    'credit' => '',
                ];

                $receivablesAccount = ['id' => $invoice->id,
                    'date' =>  '',
                    'type' => '',
                    'transactionType' => '',
                    'name' => '',
                    'account' => 'Account Receivables ( Debtors)',
                    'debit' => '',
                    'credit' => $invoice->amount,
                ];

                $totalDebit += $invoice->amount;
                $totalCredit += $invoice->amount;
                array_push($journalReport, $cashAccount);
                array_push($journalReport, $receivablesAccount);
            }

            if($invoice->payment_term != 'Paid') {
                $salesAccount = ['id' => $invoice->id,
                    'date' =>  \Carbon\Carbon::parse($invoice->date)->toFormattedDateString(),
                    'type' => 'Invoice',
                    'transactionType' => $invoice->type,
                    'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                    'account' => 'Sales',
                    'debit' => '',
                    'credit' => $invoice->amount,
                ];

                $receivablesAccount = ['id' => $invoice->id,
                    'date' =>  '',
                    'type' => '',
                    'transactionType' => '',
                    'name' => '',
                    'account' => 'Account Receivables ( Debtors)',
                    'debit' => $invoice->amount,
                    'credit' => '',
                ];

                array_push($journalReport, $salesAccount);
                array_push($journalReport, $receivablesAccount);
            }
        }

        // All expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'account', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }

        $expenses = $expenses->get();

        foreach ($expenses as $expense) {
            if($expense->payment_term == 'Paid') {
                $account = Account::find($expense->account_id);

                $cashAccount = ['id' => $expense->id,
                    'date' =>  '',
                    'type' => '',
                    'transactionType' => '',
                    'name' => '',
                    'account' => $account->name,
                    'debit' => '',
                    'credit' => $expense->amount,
                ];

                $payableAccount = [
                    'id' => $expense->id,
                    'date' =>  \Carbon\Carbon::parse($expense->date)->toFormattedDateString(),
                    'type' => 'Payment',
                    'transactionType' => $expense->type,
                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                    'account' => 'Account payable ( creditors)',
                    'debit' => $expense->amount,
                    'credit' => '',
                ];

                $totalDebit += $expense->amount;
                $totalCredit += $expense->amount;
                array_push($journalReport, $payableAccount);
                array_push($journalReport, $cashAccount);
            }

            if($expense->payemnt_term != 'Paid') {
                $items = $expense->businessExpenseItem;
                if(!empty($items)) {
                    foreach ($items as $item) {
                        $account = Account::find($item->account_id);
                        $cashAccount = ['id' => $expense->id,
                            'date' =>  \Carbon\Carbon::parse($expense->date)->toFormattedDateString(),
                            'type' => 'Expense',
                            'transactionType' => $expense->type,
                            'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                            'account' => $account->name,
                            'debit' => $expense->amount,
                            'credit' => '',
                        ];

                        $payableAccount = ['id' => $expense->id,
                            'date' =>  '',
                            'type' => '',
                            'transactionType' => '',
                            'name' => '',
                            'account' => 'Account payable ( creditors)',
                            'debit' => '',
                            'credit' => $expense->amount,
                        ];
                        array_push($journalReport, $cashAccount);
                        array_push($journalReport, $payableAccount);
                    }
                }
            }
        }

        $journalEntries = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $journalEntries = $journalEntries->whereBetween('created_at', [$from, $to]);
        }

        $journalEntries = $journalEntries->get();

        foreach ($journalEntries as $journalEntry) {
            $journalItems = $journalEntry->businessJournalItem;
            $i = 0;
            if(!empty($journalItems)) {
                foreach ($journalItems as $journalItem) {
                    $account = Account::find($journalItem->account_id);
                    if($i == 0) {
                        $items = ['id' => $journalEntry->id,
                           'date' => \Carbon\Carbon::parse($journalEntry->date)->toFormattedDateString(),
                           'type' => 'Journal',
                           'transactionType' => 'JOURNAL',
                           'name' => '',
                           'account' => $account->name,
                           'debit' => $journalItem->debit,
                           'credit' => $journalItem->credit,
                        ];
                    } else {
                        $items = ['id' => $journalEntry->id,
                           'date' =>  '',
                           'type' => '',
                           'transactionType' => '',
                           'name' => '',
                           'account' => $account->name,
                           'debit' => $journalItem->debit,
                           'credit' => $journalItem->credit,
                        ];
                    }
                    $i++;
                    $totalDebit += $journalItem->debit;
                    $totalCredit += $journalItem->credit;
                    array_push($journalReport, $items);
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['journalReport' => $journalReport, 'date' => $date, 'totalDebit' => $totalDebit, 'totalCredit' => $totalCredit];
    }

    public static function agedReceviablesReport($input)
    {
       /* $from = $input['from'];
        $to = $input['to'];
        $headerArray = ['Date', 'Type', 'Name'];
        $headerArray = Util::getMonthList($from, $to, $headerArray);*/
        $businessProfileId = $input['business_profile_id'];
        $currentMonthFirstDay = \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d');
        $currentMonthLastDay = \Carbon\Carbon::now()->lastOfMonth()->format('Y-m-d');
        $totalReceviables = 0;
        $currentMonthName = date("F",mktime(0,0,0,\Carbon\Carbon::now()->month,1,2011));

        //get all due invoices
        $agedReceviables = array();

        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', '!=', 'Paid')
            ->whereBetween('created_at', [$currentMonthFirstDay, $currentMonthLastDay])
            ->whereNull('deleted_at')->get();


        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            $itemTotal = 0;
            if(!empty($items)) {
                foreach ($items as $item) {
                    $itemTotal += $item['amount'];
                }
            }

            $agedReceviables[] = ['id' => $invoice->id,
                'date' => \Carbon\Carbon::parse($invoice->created_date)->toFormattedDateString(),
                'transactionNo' => $invoice->invoice_no,
                'type' => 'Invoice',
                'customer' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                'total' => $itemTotal
            ];

            $totalReceviables += $itemTotal;
        }

        $journalEntries = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');
        if(!empty($from) && !empty($to)) {
            $journalEntries = $journalEntries->whereBetween('created_at', [$currentMonthFirstDay, $currentMonthLastDay]);
        }

        $journalEntries = $journalEntries->get();

        foreach ($journalEntries as $journalEntry) {
            $journalItems = $journalEntry->businessJournalItem;
            foreach ($journalItems as $journalItem) {
                $account = Account::find($journalItem->account_id);
                if($account->name == 'Account Receivables ( Debtors)' && $journalItem->debit > 0) {
                    $agedReceviables[] = ['id' => $journalEntry->id,
                        'date' => \Carbon\Carbon::parse($journalEntry->date)->toFormattedDateString(),
                        'transactionNo' => '',
                        'customer' => '',
                        'type' => 'Journal',
                        'total' => $journalItem->debit
                    ];
                    $totalReceviables += $journalItem->debit;
                }
            }
        }

        return ['agedReceviables' => $agedReceviables, 'totalReceviables' => $totalReceviables, 'currentMonth' => $currentMonthName];
    }

    public static function agedPayablesReport($input)
    {
        $businessProfileId = $input['business_profile_id'];
        $currentMonthFirstDay = \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d');
        $currentMonthLastDay = \Carbon\Carbon::now()->lastOfMonth()->format('Y-m-d');
        $totalPayables = 0;
        $currentMonthName = date("F",mktime(0,0,0,\Carbon\Carbon::now()->month,1,2011));

        //get all due expenses
        $agedPayables = array();

        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', '!=', 'Paid')
            ->whereBetween('created_at', [$currentMonthFirstDay, $currentMonthLastDay])
            ->whereNull('deleted_at')->get();

        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            $itemTotal = 0;
            $ignore = 0;
            if(!empty($items)) {
                foreach ($items as $item) {
                    $account = Account::find($item['account_id']);
                    if(!empty($account) && $account->name != 'Salary and wages') {
                        $itemTotal += $item['amount'];
                    } else {
                        $ignore = 1;
                    }
                }
            }
            if($ignore == 0) {
                $agedPayables[] = ['id' => $expense->id,
                    'date' => \Carbon\Carbon::parse($expense->created_date)->toFormattedDateString(),
                    'type' => 'Expense',
                    'vendor' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                    'total' => $itemTotal
                ];

                $totalPayables += $itemTotal;
            }
        }


        $journalEntries = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');
        if(!empty($from) && !empty($to)) {
            $journalEntries = $journalEntries->whereBetween('created_at', [$currentMonthFirstDay, $currentMonthLastDay]);
        }

        $journalEntries = $journalEntries->get();

        foreach ($journalEntries as $journalEntry) {
            $journalItems = $journalEntry->businessJournalItem;
            foreach ($journalItems as $journalItem) {
                $account = Account::find($journalItem->account_id);
                if($account->name == 'Account payable ( creditors)' && $journalItem->credit > 0) {
                    $agedPayables[] = ['id' => $journalEntry->id,
                        'date' => \Carbon\Carbon::parse($journalEntry->date)->toFormattedDateString(),
                        'type' => 'Journal',
                        'vendor' => '',
                        'total' => $journalItem->credit
                    ];
                    $totalPayables += $journalItem->credit;
                }
            }
        }

        return ['agedPayables' => $agedPayables, 'totalPayables' => $totalPayables, 'currentMonth' => $currentMonthName];
    }

    /**
     * Trail Balance Report
     *
     * @param $input
     * @return array
     */
    public static function trailBalanceReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];
        $trailBalance = array();
        $totalDebit = 0;
        $totalCredit = 0;

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }
        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }
        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            if(!empty($items)) {
                foreach ($items as $item) {

                    // Revenue Sales Account
                    $account = Account::find($item->account_id);
                    $parentAccount = Account::find($account->parent_id);

                    if (!array_key_exists($parentAccount->name, $trailBalance)) {
                        $trailBalance[$parentAccount->name] = array();
                    }
                    if (!array_key_exists($account->name, $trailBalance[$parentAccount->name])) {
                        $trailBalance[$parentAccount->name][$account->name] = ['name' => $account->name, 'debit' => '', 'credit' => $item->amount];
                    } else {
                        $trailBalance[$parentAccount->name][$account->name]['credit'] += $item->amount ;
                    }

                    // Assests Cash, Bank, and cash Eq Account
                    if($invoice->payment_term == 'Paid') {
                        if(!empty($invoice->account_id) && $invoice->account_id != 0) {
                            $account = Account::find($invoice->account_id);
                            $parentAccount = Account::find($account->parent_id);
                            if (!array_key_exists($parentAccount->name, $trailBalance)) {
                                $trailBalance[$parentAccount->name] = array();
                            }

                            if(!array_key_exists($account->name, $trailBalance[$parentAccount->name])) {
                                $trailBalance[$parentAccount->name][$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                            } else {
                                $trailBalance[$parentAccount->name][$account->name]['debit'] += $item->amount;
                                $trailBalance[$parentAccount->name][$account->name]['credit'] = '';
                            }
                        }
                    }

                    // Assests Account Receivables
                    if($invoice->payment_term != 'Paid') {
                        if (!array_key_exists('Current Assets', $trailBalance)) {
                            $trailBalance['Current Assets'] = array();
                        }
                        if(!array_key_exists('Account Receivables ( Debtors)', $trailBalance['Current Assets'])) {
                            $trailBalance['Current Assets']['Account Receivables ( Debtors)'] = ['name' => 'Account Receivables ( Debtors)', 'debit' => $item->amount, 'credit' => ''];
                        } else {
                            $trailBalance['Current Assets']['Account Receivables ( Debtors)']['debit'] += $item->amount;
                            $trailBalance['Current Assets']['Account Receivables ( Debtors)']['credit'] = '';
                        }
                    }
                }
            }
        }

        //get all Expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }
        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }
        $expenses = $expenses->get();

        $accountPayableList = ['Advertising & Marketing', 'Meals & Rent', 'Travel & Vehicle Expense', 'Insurance', 'Entertainment',
            'Office Expense', 'Legal Expense', 'Telephone & Internet', 'Subscription', 'Reserve', 'Cost of goods sold', 'Purchase',
            'Freight & Delivery'
        ];
        $assestAccount = ['Office Equipment', 'Vehicle', 'Investment'];
        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    if($expense->payment_term == 'Paid') {
                        // Assest Cash, bank and cash eq. account
                        if(!empty($expense->account_id) && $expense->account_id != 0) {
                            $account = Account::find($expense->account_id);
                            $parentAccount = Account::find($account->parent_id);
                            if (!array_key_exists($parentAccount->name, $trailBalance)) {
                                $trailBalance[$parentAccount->name] = array();
                            }

                            if(!array_key_exists($account->name, $trailBalance[$parentAccount->name])) {
                                $trailBalance[$parentAccount->name][$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                            } else {
                                $trailBalance[$parentAccount->name][$account->name]['debit'] -= $item->amount;
                                $trailBalance[$parentAccount->name][$account->name]['credit'] = '';
                            }
                        }
                        // Expense Account
                        $account = Account::find($item->account_id);
                        $parentAccount = Account::find($account->parent_id);

                        if (!array_key_exists($parentAccount->name, $trailBalance)) {
                            $trailBalance[$parentAccount->name] = array();
                        }

                        if (!array_key_exists($account->name, $trailBalance[$parentAccount->name])) {
                            $trailBalance[$parentAccount->name][$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                        } else {
                            $trailBalance[$parentAccount->name][$account->name]['debit'] += $item->amount ;
                        }
                    }

                    //
                    if($expense->payment_term != 'Paid') {
                        $account = Account::find($item->account_id);
                        $parentAccount = Account::find($account->parent_id);
                        if (!array_key_exists($parentAccount->name, $trailBalance)) {
                            $trailBalance[$parentAccount->name] = array();
                        }
                        if(!array_key_exists($account->name, $trailBalance[$parentAccount->name])) {
                            $trailBalance[$parentAccount->name][$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                        } else {
                            $trailBalance[$parentAccount->name][$account->name]['debit'] -= $item->amount;
                            $trailBalance[$parentAccount->name][$account->name]['credit'] = '';
                        }

                        if ($account->name == 'Salary and wages') {
                            if (!array_key_exists('Liabilities', $trailBalance)) {
                                $trailBalance['Liabilities'] = array();
                            }
                            if(!array_key_exists('Salary Payable', $trailBalance['Liabilities'])) {
                                $trailBalance['Liabilities']['Salary Payable'] = ['name' => 'Salary Payable', 'debit' => '', 'credit' => $item->amount];
                            } else {
                                $trailBalance['Liabilities']['Salary Payable']['debit'] = '';
                                $trailBalance['Liabilities']['Salary Payable']['credit'] = $item->amount;
                            }
                        }

                        if(in_array($account->name, $accountPayableList)) {
                            if (!array_key_exists('Liabilities', $trailBalance)) {
                                $trailBalance['Liabilities'] = array();
                            }
                            if(!array_key_exists('Account payable ( creditors)', $trailBalance['Liabilities'])) {
                                $trailBalance['Liabilities']['Account payable ( creditors)'] = ['name' => 'Account payable ( creditors)', 'debit' => '', 'credit' => $item->amount];
                            } else {
                                $trailBalance['Liabilities']['Account payable ( creditors)']['debit'] = '';
                                $trailBalance['Liabilities']['Account payable ( creditors)']['credit'] += $item->amount;
                            }
                        }

                        if(in_array($account->name, $assestAccount)) {
                            if(!array_key_exists($account->name, $trailBalance)) {
                                $trailBalance[$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                            } else {
                                $trailBalance[$account->name]['debit'] -= $item->amount;
                                $trailBalance[$account->name]['credit'] = '';
                            }
                        }
                    }
                }
            }
        }

        $trailBalanceData = array();
        foreach ($trailBalance as $key=>$value) {
            if(!array_key_exists($key, $trailBalanceData)) {
                $trailBalanceData[$key] = array();
            }
            foreach ($value as $v) {
                $trailBalanceData[$key][] = $v;
                $totalDebit += !empty($v['debit']) ? $v['debit'] : 0;
                $totalCredit += !empty($v['credit']) ? $v['credit'] : 0;
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['trailBalanceData' => $trailBalanceData, 'totalDebit' => $totalDebit, 'totalCredit' => $totalCredit, 'date' => $date];
    }

    /**
     * Accounting Transaction Report
     *
     * @param $input
     * @return array
     */
    public static function accountingTransactionReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];
        $accountingTransactions = array();

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }

        $invoices = $invoices->get();
        $totalAmount = 0;
        $totalGrossAmount = 0;

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            $tax = TaxRate::find($invoice->tax_rate_id);

            if(!empty($items)) {
                foreach ($items as $item) {
                    $account = Account::find($item->account_id);
                    $Item = BusinessItem::find($item->business_item_id);

                    $accountingTransactions[] = ['id' => $invoice->id,
                        'date' =>  \Carbon\Carbon::parse($invoice->created_at)->toFormattedDateString(),
                        'account' => $account->name,
                        'type' => 'Invoice',
                        'transaction' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name . ' - ' . $Item['name'],
                        'reference' => $invoice->reference,
                        'net_amount' => $invoice->amount,
                        'tax_amount' => $invoice->tax_amount,
                        'gross' => $invoice->amount + $invoice->tax_amount,
                        'tax_rate' => !empty($tax) ? $tax->tax_rates : null,
                        'tax_name' => !empty($tax) ? $tax->name : null,
                    ];

                    $totalAmount += $invoice->amount;
                    $grossAmount = $invoice->amount + $invoice->tax_amount;
                    $totalGrossAmount += $grossAmount;
                }
            }
        }


        //get all Expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }
        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }
        $expenses = $expenses->get();

        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            $tax = TaxRate::find($expense->tax_rate_id);

            if(!empty($items)) {
                foreach ($items as $item) {
                    $account = Account::find($item->account_id);
                    $Item = BusinessItem::find($item->business_item_id);

                    $accountingTransactions[] = ['id' => $expense->id,
                        'date' =>  \Carbon\Carbon::parse($expense->created_at)->toFormattedDateString(),
                        'account' => $account->name,
                        'type' => 'Expense',
                        'transaction' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name . ' - ' . $Item['name'],
                        'reference' => $expense->reference,
                        'net_amount' => $expense->amount,
                        'tax_amount' => $expense->tax_amount,
                        'gross' => $expense->amount,
                        'tax_rate' => !empty($tax) ? $tax->tax_rates : null,
                        'tax_name' => !empty($tax) ? $tax->name : null,
                    ];

                    $totalAmount += $expense->amount;
                    $grossAmount = $expense->amount + $expense->tax_amount;
                    $totalGrossAmount += $grossAmount;
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['accountingTransactions' => $accountingTransactions, 'date' => $date, 'totalAmount' => $totalAmount, 'totalGrossAmount' => $totalGrossAmount];
    }

    public static function invoiceReceivablesReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $businessProfileId = $input['business_profile_id'];
        $invoiceReceivables = array();
        $totalAmount = 0;

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', 'Paid')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }
        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            $tax = TaxRate::find($invoice->tax_rate_id);

            if(!empty($items)) {
                foreach ($items as $item) {
                    $account = Account::find($item->account_id);
                    $Item = BusinessItem::find($item->business_item_id);

                    $invoiceReceivables[] = ['id' => $invoice->id,
                        'date' =>  \Carbon\Carbon::parse($invoice->created_at)->toFormattedDateString(),
                        'account' => $account->name,
                        'invoice_no' => $invoice->invoice_no,
                        'transaction' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name . ' - ' . $Item['name'],
                        'reference' => $invoice->reference,
                        'quantity' => $item->quantity,
                        'unit_price' => $item->price,
                        'net_amount' => $invoice->amount,
                        'tax_amount' => $invoice->tax_amount,
                        'discount' => $invoice->discount,
                        'tax_rate' => !empty($tax) ? $tax->tax_rates : null,
                        'tax_name' => !empty($tax) ? $tax->name : null,
                    ];
                    $totalAmount += $invoice->amount;
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['invoiceReceivables' => $invoiceReceivables, 'date' => $date, 'totalAmount' => $totalAmount];
    }

    public static function taxReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];
        $taxReport = array();

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }

        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }
        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            $tax = TaxRate::find($invoice->tax_rate_id);

            if(!empty($tax)) {
                if(!array_key_exists($tax->name, $taxReport)) {
                    $taxReport[$tax->name] = [];
                }
                foreach ($items as $item) {
                    if($invoice->payment_term == 'Paid') {
                        $account = Account::find($invoice->account_id);
                    } else {
                        $account = Account::find($item->account_id);
                    }
                    $Item = BusinessItem::find($item->business_item_id);
                    $taxReport[$tax->name][] = ['id' => $invoice->id,
                        'date' =>  \Carbon\Carbon::parse($invoice->created_at)->toFormattedDateString(),
                        'type' => 'Invoice',
                        'account' => !empty($account) ? $account->name : '',
                        'transaction' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name . ' - ' . $Item->name,
                        'net_amount' => $item->amount,
                        'tax' => $tax->tax_rates,
                        'gross' => $invoice->amount
                    ];
                }
            }
        }

        //get all Expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }
        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }
        $expenses = $expenses->get();

        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            $tax = TaxRate::find($expense->tax_rate_id);

            if(!empty($tax)) {
                if(!array_key_exists($tax->name, $taxReport)) {
                    $taxReport[$tax->name] = [];
                }
                if (!empty($items)) {
                    foreach ($items as $item) {
                        if($expense->payment_term == 'Paid') {
                            $account = Account::find($expense->account_id);
                        } else {
                            $account = Account::find($item->account_id);
                        }
                        $Item = BusinessItem::find($item->business_item_id);
                        $taxReport[$tax->name][] = ['id' => $expense->id,
                            'date' =>  \Carbon\Carbon::parse($expense->created_at)->toFormattedDateString(),
                            'type' => 'Expense',
                            'account' => !empty($account) ? $account->name : '',
                            'transaction' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name . ' - ' . $Item->name,
                            'net_amount' => $item->amount,
                            'tax' => $tax->tax_rates,
                            'gross' => $expense->amount
                        ];
                    }
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['taxReport' => $taxReport, 'date' => $date];
    }

    public static function cashSummaryReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $businessProfileId = $input['business_profile_id'];
        $incomeCashSummary = array();
        $expenseCashSummary = array();
        $totalIncome = 0;
        $totalExpense = 0;

        $invoiceList = array();
        $expenseList = array();

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', 'Paid')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }
        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    // Revenue Sales Account
                    $account = Account::find($item->account_id);
                    $parentAccount = Account::find($account->parent_id);

                    if(!empty($parentAccount)) {
                        if (!array_key_exists($parentAccount->name, $incomeCashSummary)) {
                            $incomeCashSummary[$parentAccount->name] = array();
                        }
                        if (!array_key_exists($account->name, $incomeCashSummary[$parentAccount->name])) {
                            $incomeCashSummary[$parentAccount->name][$account->name] = $item->amount;
                            $totalIncome += $item->amount;
                        } else {
                            $incomeCashSummary[$parentAccount->name][$account->name] += $item->amount;
                            $totalIncome += $item->amount;
                        }

                        array_push($invoiceList, ['id' => $invoice->id,
                            'date' => \Carbon\Carbon::parse($invoice->created_at)->format('Y-m-d'),
                            'transactionType' => 'Invoice',
                            'no' => $invoice->invoice_no,
                            'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                            'account' => $account->name,
                            'amount' => $item->amount,
                        ]);
                    }
                }
            }
        }

        //get all Expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', 'Paid')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }
        $expenses = $expenses->get();

        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    // Expense Account
                    $account = Account::find($item->account_id);
                    $parentAccount = Account::find($account->parent_id);

                    if(!empty($parentAccount)) {
                        if (!array_key_exists($parentAccount->name, $expenseCashSummary)) {
                            $expenseCashSummary[$parentAccount->name] = array();
                        }

                        if (!array_key_exists($account->name, $expenseCashSummary[$parentAccount->name])) {
                            $expenseCashSummary[$parentAccount->name][$account->name] = $item->amount;
                            $totalExpense += $item->amount;
                        } else {
                            $expenseCashSummary[$parentAccount->name][$account->name] += $item->amount;
                            $totalExpense += $item->amount;
                        }

                        array_push($expenseList, ['id' => $expense->id,
                            'date' => \Carbon\Carbon::parse($expense->created_at)->format('Y-m-d'),
                            'transactionType' => 'Expense',
                            'no' => '',
                            'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                            'account' => $account->name,
                            'amount' => $item->amount,
                        ]);
                    }
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['incomeCashSummary' => $incomeCashSummary,
            'expenseCashSummary' => $expenseCashSummary,
            'totalIncome' => $totalIncome,
            'totalExpense' => $totalExpense,
            'invoiceList' => $invoiceList,
            'expenseList' => $expenseList,
            'date' => $date];
    }

    public static function cashAccountReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $businessProfileId = $input['business_profile_id'];
        $cashAccount = array();
        $totalDebit = 0;
        $totalCredit = 0;

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', 'Paid')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }
        $invoices = $invoices->get();

        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    $cashAccount[] = ['id' => $invoice->id,
                        'date' => \Carbon\Carbon::parse($invoice->created_date)->toFormattedDateString(),
                        'type' => 'Invoice',
                        'transaction' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                        'debit' => $item->amount,
                        'credit' => ''
                    ];
                    $totalDebit += $item->amount;
                }
            }
        }

        //get all Expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->where('payment_term', 'Paid')
            ->whereNull('deleted_at');

        if(!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }
        $expenses = $expenses->get();

        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    $cashAccount[] = ['id' => $expense->id,
                        'date' => \Carbon\Carbon::parse($expense->created_date)->toFormattedDateString(),
                        'type' => 'Expense',
                        'transaction' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                        'debit' => '',
                        'credit' => $item->amount
                    ];
                    $totalCredit += $item->amount;
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['cashAccount' => $cashAccount, 'totalDebit' => $totalDebit, 'totalCredit' => $totalCredit, 'date' => $date];
    }

    public static function generalLedgerReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];
        $generalLedger = array();
        $totalDebit = 0;
        $totalCredit = 0;

        $cashBankEquiAccount = ['Cash Account', 'Bank Account', 'Cash & Cash Equivalents'];

        //get all invoices
        $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'INVOICE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if (!empty($from) && !empty($to)) {
            $invoices = $invoices->whereBetween('created_at', [$from, $to]);
        }
        if(!empty($basis) && $basis == 'cash') {
            $invoices = $invoices->where('payment_term', 'Paid');
        }
        $invoices = $invoices->get();

        $generalLedgerDetails = array();
        foreach ($invoices as $invoice) {
            $items = $invoice->businessInvoiceItem;
            if (!empty($items)) {
                foreach ($items as $item) {

                    // Revenue Sales Account
                    $account = Account::find($item->account_id);
                    if (!array_key_exists($account->name, $generalLedger)) {
                        $generalLedger[$account->name] = ['name' => $account->name, 'debit' => '', 'credit' => $item->amount];
                        $generalLedgerDetails[] = ['id' => $invoice->id,
                            'date' => $invoice->date,
                            'type' => 'INVOICE',
                            'no' => $invoice->invoice_no,
                            'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                            'account' => $account->name,
                            'debit' => 0,
                            'credit' => $item->amount
                        ];
                    } else {
                        $generalLedger[$account->name]['credit'] += $item->amount;
                        $generalLedgerDetails[] = ['id' => $invoice->id,
                            'date' => $invoice->date,
                            'type' => 'INVOICE',
                            'no' => $invoice->invoice_no,
                            'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                            'account' => $account->name,
                            'debit' => 0,
                            'credit' => $item->amount
                        ];
                    }

                    // Assests Cash, Bank, and cash Eq Account
                    if ($invoice->payment_term == 'Paid') {
                        if (!empty($invoice->account_id) && $invoice->account_id != 0) {
                            $account = Account::find($invoice->account_id);
                            if (!array_key_exists($account->name, $generalLedger)) {
                                $generalLedger[$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                                $generalLedgerDetails[] = ['id' => $invoice->id,
                                    'date' => $invoice->date,
                                    'type' => 'INVOICE',
                                    'no' => $invoice->invoice_no,
                                    'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                                    'account' => $account->name,
                                    'debit' => 0,
                                    'credit' => $item->amount
                                ];
                            } else {
                                $generalLedger[$account->name]['debit'] += $item->amount;
                                $generalLedger[$account->name]['credit'] = '';
                                $generalLedgerDetails[] = ['id' => $invoice->id,
                                    'date' => $invoice->date,
                                    'type' => 'INVOICE',
                                    'no' => $invoice->invoice_no,
                                    'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                                    'account' => $account->name,
                                    'debit' => 0,
                                    'credit' => $item->amount
                                ];
                            }
                        }
                    }

                    // Assests Account Receivables
                    if ($invoice->payment_term != 'Paid') {
                        if (!array_key_exists('Account Receivables ( Debtors)', $generalLedger)) {
                            $generalLedger['Account Receivables ( Debtors)'] = ['name' => 'Account Receivables ( Debtors)', 'debit' => $item->amount, 'credit' => ''];
                            $generalLedgerDetails[] = ['id' => $invoice->id,
                                'date' => $invoice->date,
                                'type' => 'INVOICE',
                                'no' => $invoice->invoice_no,
                                'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                                'account' => 'Account Receivables ( Debtors)',
                                'debit' => 0,
                                'credit' => $item->amount
                            ];
                        } else {
                            $generalLedger['Account Receivables ( Debtors)']['debit'] += $item->amount;
                            $generalLedger['Account Receivables ( Debtors)']['credit'] = '';
                            $generalLedgerDetails[] = ['id' => $invoice->id,
                                'date' => $invoice->date,
                                'type' => 'INVOICE',
                                'no' => $invoice->invoice_no,
                                'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                                'account' => 'Account Receivables ( Debtors)',
                                'debit' => 0,
                                'credit' => $item->amount
                            ];
                        }
                    }
                }
            }
        }

        //get all Expenses
        $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
            ->where('business_profile_id', $businessProfileId)
            ->where('type', 'EXPENSE')
            ->where('status', '!=', 'DRAFT')
            ->whereNull('deleted_at');

        if (!empty($from) && !empty($to)) {
            $expenses = $expenses->whereBetween('created_at', [$from, $to]);
        }
        if(!empty($basis) && $basis == 'cash') {
            $expenses = $expenses->where('payment_term', 'Paid');
        }
        $expenses = $expenses->get();

        $accountPayableList = ['Advertising & Marketing', 'Meals & Rent', 'Travel & Vehicle Expense', 'Insurance', 'Entertainment',
            'Office Expense', 'Legal Expense', 'Telephone & Internet', 'Subscription', 'Reserve', 'Cost of goods sold', 'Purchase',
            'Freight & Delivery'
        ];
        $assestAccount = ['Office Equipment', 'Vehicle', 'Investment', 'Buildings'];
        foreach ($expenses as $expense) {
            $items = $expense->businessExpenseItem;
            if(!empty($items)) {
                foreach ($items as $item) {
                    if($expense->payment_term == 'Paid') {
                        // Assest Cash, bank and cash eq. account
                        if(!empty($expense->account_id) && $expense->account_id != 0) {
                            $account = Account::find($expense->account_id);

                            if(!array_key_exists($account->name, $generalLedger)) {
                                $generalLedger[$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => $account->name,
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            } else {
                                $generalLedger[$account->name]['debit'] -= $item->amount;
                                $generalLedger[$account->name]['credit'] = '';
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => $account->name,
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            }
                        }
                        // Expense Account
                        $account = Account::find($item->account_id);
                        if (!array_key_exists($account->name, $generalLedger)) {
                            $generalLedger[$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                            $generalLedgerDetails[] = ['id' => $expense->id,
                                'date' => $expense->date,
                                'type' => 'EXPENSE',
                                'no' => '',
                                'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                'account' => $account->name,
                                'debit' => $item->amount,
                                'credit' => 0,
                            ];
                        } else {
                            $generalLedger[$account->name]['debit'] += $item->amount ;
                            $generalLedgerDetails[] = ['id' => $expense->id,
                                'date' => $expense->date,
                                'type' => 'EXPENSE',
                                'no' => '',
                                'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                'account' => $account->name,
                                'debit' => $item->amount,
                                'credit' => 0,
                            ];
                        }
                    }

                    //
                    if($expense->payment_term != 'Paid') {
                        $account = Account::find($item->account_id);
                        if(!array_key_exists($account->name, $generalLedger)) {
                            $generalLedger[$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                            $generalLedgerDetails[] = ['id' => $expense->id,
                                'date' => $expense->date,
                                'type' => 'EXPENSE',
                                'no' => '',
                                'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                'account' => $account->name,
                                'debit' => $item->amount,
                                'credit' => 0,
                            ];
                        } else {
                            $generalLedger[$account->name]['debit'] -= $item->amount;
                            $generalLedger[$account->name]['credit'] = '';
                            $generalLedgerDetails[] = ['id' => $expense->id,
                                'date' => $expense->date,
                                'type' => 'EXPENSE',
                                'no' => '',
                                'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                'account' => $account->name,
                                'debit' => $item->amount,
                                'credit' => 0,
                            ];
                        }

                        if ($account->name == 'Salary and wages') {
                            if(!array_key_exists('Salary Payable', $generalLedger)) {
                                $generalLedger['Salary Payable'] = ['name' => 'Salary Payable', 'debit' => '', 'credit' => $item->amount];
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => 'Salary Payable',
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            } else {
                                $generalLedger['Salary Payable']['debit'] = '';
                                $generalLedger['Salary Payable']['credit'] = $item->amount;
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => 'Salary Payable',
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            }
                        }

                        if(in_array($account->name, $accountPayableList)) {
                            if(!array_key_exists('Account payable ( creditors)', $generalLedger)) {
                                $generalLedger['Account payable ( creditors)'] = ['name' => 'Account payable ( creditors)', 'debit' => '', 'credit' => $item->amount];
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => 'Account payable ( creditors)',
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            } else {
                                $generalLedger['Account payable ( creditors)']['debit'] = '';
                                $generalLedger['Account payable ( creditors)']['credit'] += $item->amount;
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => 'Account payable ( creditors)',
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            }
                        }

                        if(in_array($account->name, $assestAccount)) {
                            if(!array_key_exists($account->name, $generalLedger)) {
                                $generalLedger[$account->name] = ['name' => $account->name, 'debit' => $item->amount, 'credit' => ''];
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => $account->name,
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            } else {
                                $generalLedger[$account->name]['debit'] -= $item->amount;
                                $generalLedger[$account->name]['credit'] = '';
                                $generalLedgerDetails[] = ['id' => $expense->id,
                                    'date' => $expense->date,
                                    'type' => 'EXPENSE',
                                    'no' => '',
                                    'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                    'account' => $account->name,
                                    'debit' => $item->amount,
                                    'credit' => 0,
                                ];
                            }
                        }
                    }
                }
            }
        }


        $journalEntries = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');
        if(!empty($from) && !empty($to)) {
            $journalEntries = $journalEntries->whereBetween('created_at', [$from, $to]);
        }

        $journalEntries = $journalEntries->get();
        foreach ($journalEntries as $journalEntry) {
            $journalItems = $journalEntry->businessJournalItem;
            if(!empty($journalItems)) {
                foreach ($journalItems as $journalItem) {
                    $account = Account::find($journalItem->account_id);
                    if(!empty($account)) {
                        if($journalItem->debit > 0) {
                            if (!array_key_exists($account->name, $generalLedger)) {
                                $generalLedger[$account->name] = ['name' => $account->name, 'debit' => $journalItem->debit, 'credit' => ''];
                                $generalLedgerDetails[] = ['id' => $journalEntry->id,
                                    'date' => $journalEntry->date,
                                    'type' => 'JOURNAL',
                                    'no' => '',
                                    'name' => '',
                                    'account' => $account->name,
                                    'debit' => $journalItem->debit,
                                    'credit' => 0,
                                ];
                            } else {
                                $generalLedger[$account->name]['debit'] += $journalItem->debit;
//                                $generalLedger[$account->name]['credit'] = '';
                                $generalLedgerDetails[] = ['id' => $journalEntry->id,
                                    'date' => $journalEntry->date,
                                    'type' => 'JOURNAL',
                                    'no' => '',
                                    'name' => '',
                                    'account' => $account->name,
                                    'debit' => $journalItem->debit,
                                    'credit' => 0
                                ];
                            }
                        } else {
                            if (!array_key_exists($account->name, $generalLedger)) {
                                $generalLedger[$account->name] = ['name' => $account->name, 'debit' => 0, 'credit' => $journalItem->credit];
                                $generalLedgerDetails[] = ['id' => $journalEntry->id,
                                    'date' => $journalEntry->date,
                                    'type' => 'JOURNAL',
                                    'no' => '',
                                    'name' => '',
                                    'account' => $account->name,
                                    'debit' => 0,
                                    'credit' => $journalItem->credit,
                                ];
                            } else {
//                                $generalLedger[$account->name]['debit'] = '';
                                $generalLedger[$account->name]['credit'] += $journalItem->credit;
                                $generalLedgerDetails[] = ['id' => $journalEntry->id,
                                    'date' => $journalEntry->date,
                                    'type' => 'JOURNAL',
                                    'no' => '',
                                    'name' => '',
                                    'account' => $account->name,
                                    'debit' => 0,
                                    'credit' => $journalItem->credit
                                ];
                            }
                        }
                    }
                }
            }
        }

        $generalLedgerData = array();
        foreach ($generalLedger as $key=>$value) {
            $generalLedgerData[] = $value;
            $totalDebit += $value['debit'];
            $totalCredit += $value['credit'];
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['generalLedgerData' => $generalLedgerData,
            'totalDebit' => $totalDebit,
            'totalCredit' => $totalCredit,
            'date' => $date,
            'generalLedgerDetails' => $generalLedgerDetails];
    }

    public static function cashFlowReport($input)
    {
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        $businessProfileId = $input['business_profile_id'];

        //get Profit and loss report Data
        $profitandLoss = self::profitandLossReport($input);
        $totalIncome = $profitandLoss['totalIncome'];
        $totalExpense = $profitandLoss['totalExpense'];

        // get balance sheet report data
        $balanceSheet = self::balanceSheetReport($input);
        $balanceSheetAssets = $balanceSheet['balanceSheetAssets'];
        $balanceSheetLiabilities = $balanceSheet['balanceSheetLiabilities'];
        $balanceSheetOwners = $balanceSheet['balanceSheetOwners'];

        //find net income from profit and loss account
        $netIncome = $totalIncome - $totalExpense;

        // find non cash data form balance sheet
        $adjustmentNonCash = array();
        $investingActivites = array();
        $financingActivites = array();
        $totalAdjustment = 0;
        $totalInvesting = 0;
        $totalFinancial = 0;

        $adjustmentAccountsAdd = ['Account payable ( creditors)'];
        $adjustmentAccountsLess = ['Account Receivables ( Debtors)', 'Prepaid Expense'];
        $investingAccounts = ['Buildings', 'Office Equipment', 'Vehicle'];
        $financialAccounts = ['Capital & Equity'];

        if(!empty($profitandLoss['profitandLoss'])) {
            foreach ($profitandLoss['profitandLoss'] as $items) {
                foreach ($items as $item) {
                    foreach ($item as $account=>$amount) {
                        if ($account == 'Depreciation Expense' || $account == 'Less Accumulated depreciation') {
                            $adjustmentNonCash['Add'][$account] = $amount;
                            $totalAdjustment += $amount;
                        }
                    }
                }
            }
        }
        if(!empty($balanceSheetAssets)) {
            foreach ($balanceSheetAssets as $key => $value) {
                foreach ($value['account'] as $account => $amount) {
                    if (in_array($account, $adjustmentAccountsAdd)) {
                        $adjustmentNonCash['Add'][$account] = $amount;
                        $totalAdjustment += $amount;
                    }
                    if (in_array($account, $adjustmentAccountsLess)) {
                        $adjustmentNonCash['Less'][$account] = $amount;
                        $totalAdjustment -= $amount;
                    }
                }
            }
        }
        if(!empty($balanceSheetLiabilities)) {
            foreach ($balanceSheetLiabilities as $key => $value) {
                foreach ($value['account'] as $account => $amount) {
                    if (in_array($account, $adjustmentAccountsAdd)) {
                        $adjustmentNonCash['Add'][$account] = $amount;
                        $totalAdjustment += $amount;
                    }
                    if (in_array($account, $adjustmentAccountsLess)) {
                        $adjustmentNonCash['Less'][$account] = $amount;
                        $totalAdjustment -= $amount;
                    }
                }
            }
        }
        if(!empty($balanceSheetOwners)) {
            foreach ($balanceSheetOwners as $key=>$value) {
                foreach ($value['account'] as $account => $amount) {
                    if (in_array($account, $financialAccounts)) {
                        $financingActivites[$account] = $amount;
                        $totalFinancial += $amount;
                    }
                }
            }
        }

        $accounts = Account::whereIn('name', ['Cash Account', 'Bank Account', 'Cash & Cash Equivalents'])->get();

        foreach ($accounts as $account) {
            $accountBalance = AccountBalance::where('business_profile_id', $businessProfileId)->where('account_id', $account->id)->first();
            if(!empty($accountBalance)) {
                $financingActivites[$account->name] = $accountBalance['balance'];
                $totalFinancial += $accountBalance['balance'];
            }
        }

        //get All Journal Entries
        $journalEntries = BusinessJournal::with('businessJournalItem', 'businessProfile')->where('business_profile_id', $businessProfileId)->whereNull('deleted_at');
        if(!empty($from) && !empty($to)) {
            $journalEntries = $journalEntries->whereBetween('created_at', [$from, $to]);
        }

        $journalEntries = $journalEntries->get();
        foreach ($journalEntries as $journalEntry) {
            $journalItems = $journalEntry->businessJournalItem;
            foreach ($journalItems as $journalItem) {
                $account = Account::find($journalItem->account_id);
                if(!empty($account) && in_array($account->name, $investingAccounts) ) {
                    if($journalItem->debit > 0) {
                        $investingActivites[$account->name] = $journalItem->debit;
                        $totalInvesting += $journalItem->debit;
                    }
                }
            }
        }

        $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

        return ['adjustmentNonCash' => $adjustmentNonCash,
            'investingActivites' => $investingActivites,
            'financingActivites' => $financingActivites,
            'totalAdjustment' => $totalAdjustment,
            'totalInvesting' => $totalInvesting,
            'totalFinancial' => $totalFinancial,
            'netIncome' => $netIncome,
            'date' => $date,
        ];
    }
}