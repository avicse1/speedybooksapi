<?php  namespace App\Libraries;

use App\Account;
use App\BusinessInvoice;
use App\BusinessItem;
use App\BusinessExpense;
use App\BusinessProfile;
use App\TaxRate;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\Guard;

class PDF
{
    /**
     * customer Invoice Report
     *
     * @param $input
     * @return string
     */
    public static function invoiceReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $invoiceReport = Report::invoiceReport($input);
            $invoices = $invoiceReport['invoices'];
            $total = $invoiceReport['total'];
            $date = $invoiceReport['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'invoiceReport' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'invoiceReport' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.invoiceReport', compact('invoices', 'total', 'date', 'businessProfile'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Customer Invoice report " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Sales Item Report
     * @param $input
     * @return string
     */

    public static function salesByItemsReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $salesItemReport = Report::salesByItemsReport($input);

            $items = $salesItemReport['items'];
            $totalAmount = $salesItemReport['totalAmount'];
            $totalQuantity = $salesItemReport['totalQuantity'];
            $totalAverage = $totalAmount / $totalQuantity;
            $date = $salesItemReport['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'salesItems' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'salesItems' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.salesItemReport', compact('items', 'totalAmount', 'totalQuantity', 'totalAverage', 'date', 'businessProfile'))->save($pathToFile);

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Sales by items " . $e->getTraceAsString());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Profit and Loss Report
     *
     * @param $input
     * @return string
     */
    public static function profitandLossReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $profitandLossReport = Report::profitandLossReport($input);

            $profitandLoss = $profitandLossReport['profitandLoss'];
            $date = $profitandLossReport['date'];
            $totalIncome = $profitandLossReport['totalIncome'];
            $totalExpense = $profitandLossReport['totalExpense'];
            $invoiceList = $profitandLossReport['invoiceList'];
            $expenseList = $profitandLossReport['expenseList'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'profitandLoss' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'profitandLoss' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.profitandLoss', compact('businessProfile', 'profitandLoss', 'date', 'totalIncome', 'totalExpense', 'invoiceList', 'expenseList'))->save($pathToFile);

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Profit and loss reports " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Balance Sheet Report
     *
     * @param $input
     * @return string
     */
    public static function balanceSheetReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $profitandLoss = Report::profitandLossReport($input);
            $balanceSheetReport = Report::balanceSheetReport($input);

            $netProfit = $profitandLoss['totalIncome'] - $profitandLoss['totalExpense'];
            $balanceSheetAssets = $balanceSheetReport['balanceSheetAssets'];
            $balanceSheetLiabilities = $balanceSheetReport['balanceSheetLiabilities'];
            $balanceSheetOwners = $balanceSheetReport['balanceSheetOwners'];
            $date = $balanceSheetReport['date'];
            $totalAssets = $balanceSheetReport['totalAssets'];
            $totalLiabilities = $balanceSheetReport['totalLiabilities'];
            $totalOwners = $balanceSheetReport['totalOwners'] + $netProfit;

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'balanceSheet' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'balanceSheet' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.balance-sheet', compact('businessProfile', 'balanceSheetAssets', 'balanceSheetLiabilities', 'balanceSheetOwners', 'date', 'totalAssets', 'totalLiabilities', 'totalOwners', 'netProfit'))->save($pathToFile);

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Balance reports pdf" . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Journal Report
     *
     * @param $input
     * @return string
     */
    public static function journalReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $journalReportDetails = Report::journalReport($input);
            $journalReport = $journalReportDetails['journalReport'];
            $date = $journalReportDetails['date'];
            $totalDebit = $journalReportDetails['totalDebit'];
            $totalCredit = $journalReportDetails['totalCredit'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'journalReport' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'journalReport' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.journalReport', compact('journalReport', 'date', 'businessProfile', 'totalDebit', 'totalCredit'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Journal reports details" . $e->getMessage());
            return response()->error($e->getMessage());
        }
    }

    /**
     * Aged Receivables Report
     *
     * @param $input
     * @return string
     */
    public static function agedReceviablesReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $agedReceviablesReport = Report::agedReceviablesReport($input);
            $agedReceviables = $agedReceviablesReport['agedReceviables'];
            $totalReceviables = $agedReceviablesReport['totalReceviables'];
            $currentMonth = $agedReceviablesReport['currentMonth'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'agedReceivables' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'agedReceivables' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.agedReceivables', compact('agedReceviables', 'businessProfile', 'totalReceviables', 'currentMonth'))->save($pathToFile);

            return $filePath;

        } catch(\Exception $e) {
            \Log::error("Aged Receviables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Aged payables Report
     *
     * @param $input
     * @return string
     */
    public static function agedPayablesReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $agedPayablesReport = Report::agedPayablesReport($input);

            $agedPayables = $agedPayablesReport['agedPayables'];
            $totalPayables = $agedPayablesReport['totalPayables'];
            $currentMonth = $agedPayablesReport['currentMonth'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'agedPayables' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'agedPayables' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.agedPayables', compact('agedPayables', 'businessProfile', 'totalPayables', 'currentMonth'))->save($pathToFile);

            return $filePath;

        } catch(\Exception $e) {
            \Log::error("Aged Payables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Trail Balance Report
     *
     * @param $input
     * @return string
     */
    public static function trailBalanceReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $trailBalanceReport = Report::trailBalanceReport($input);
            $trailBalanceData = $trailBalanceReport['trailBalanceData'];
            $totalDebit = $trailBalanceReport['totalDebit'];
            $totalCredit = $trailBalanceReport['totalCredit'];
            $date = $trailBalanceReport['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'trailBalance' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'trailBalance' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.trailBalance', compact('trailBalanceData', 'totalDebit', 'totalCredit', 'date', 'businessProfile'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Trail Balance reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Accounting Transaction Report
     *
     * @param $input
     * @return string
     */
    public static function accountingTransactionReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $accountingTransactionReport = Report::accountingTransactionReport($input);

            $accountingTransactions = $accountingTransactionReport['accountingTransactions'];
            $date = $accountingTransactionReport['date'];
            $totalAmount = $accountingTransactionReport['totalAmount'];
            $totalGrossAmount = $accountingTransactionReport['totalGrossAmount'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'accountingTransaction' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'accountingTransaction' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.accountingTransaction', compact('accountingTransactions', 'date', 'businessProfile', 'totalAmount', 'totalGrossAmount'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Accounting Transaction reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Invoice Receivables Report
     *
     * @param $input
     * @return string
     */
    public static function invoiceReceivablesReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $invoiceReceivablesReport = Report::invoiceReceivablesReport($input);
            $invoiceReceivables = $invoiceReceivablesReport['invoiceReceivables'];
            $date = $invoiceReceivablesReport['date'];
            $totalAmount = $invoiceReceivablesReport['totalAmount'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'invoiceReceivables' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'invoiceReceivables' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.invoiceReceivables', compact('invoiceReceivables', 'date', 'businessProfile', 'totalAmount'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Invoice Receivables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Tax Report
     *
     * @param $input
     * @return string
     */
    public static function taxReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $taxReportDetails = Report::taxReport($input);
            $taxReport = $taxReportDetails['taxReport'];
            $date = $taxReportDetails['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'taxReport' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'taxReport' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.taxReport', compact('taxReport', 'date', 'businessProfile'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Tax reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Cash Summary Report
     *
     * @param $input
     * @return string
     */
    public static function cashSummaryReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $cashSummaryReport = Report::cashSummaryReport($input);
            $incomeCashSummary = $cashSummaryReport['incomeCashSummary'];
            $expenseCashSummary = $cashSummaryReport['expenseCashSummary'];
            $totalIncome = $cashSummaryReport['totalIncome'];
            $totalExpense = $cashSummaryReport['totalExpense'];
            $invoiceList = $cashSummaryReport['invoiceList'];
            $expenseList = $cashSummaryReport['expenseList'];
            $date = $cashSummaryReport['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'cashSummary' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'cashSummary' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.cashSummary', compact('incomeCashSummary',
                'expenseCashSummary',
                'totalIncome',
                'totalExpense',
                'invoiceList',
                'expenseList',
                'date',
                'businessProfile'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Cash Summary reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Cash Account Report
     *
     * @param $input
     * @return string
     */
    public static function cashAccountReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $cashAccountReport = Report::cashAccountReport($input);
            $cashAccount = $cashAccountReport['cashAccount'];
            $totalDebit = $cashAccountReport['totalDebit'];
            $totalCredit = $cashAccountReport['totalCredit'];
            $date = $cashAccountReport['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'cashAccount' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'cashAccount' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.cashAccount', compact('cashAccount', 'totalDebit', 'totalCredit', 'date', 'businessProfile'))->save($pathToFile);

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Cash Account reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * General Ledger Report
     *
     * @param $input
     * @return string
     */
    public static function generalLedgerReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $generalLedgerReport = Report::generalLedgerReport($input);

            $generalLedgerData = $generalLedgerReport['generalLedgerData'];
            $totalDebit = $generalLedgerReport['totalDebit'];
            $totalCredit = $generalLedgerReport['totalCredit'];
            $date = $generalLedgerReport['date'];

            $pathToFile = public_path() . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'generalLedger' . '.pdf';
            $appUrl = env('APP_URL');
            $filePath = $appUrl . '/pdf/' . str_replace(' ', '', $businessProfile->name). '-' . 'generalLedger' . '.pdf';

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('report.generalLedger', compact('generalLedgerData', 'totalDebit', 'totalCredit', 'date', 'businessProfile'))->save($pathToFile);

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("General Ledger reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }
}