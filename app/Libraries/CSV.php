<?php  namespace App\Libraries;

use App\Account;
use App\BusinessInvoice;
use App\BusinessItem;
use App\BusinessExpense;
use App\BusinessProfile;
use App\TaxRate;
use Illuminate\Support\Str;
use Illuminate\Contracts\Auth\Guard;
use Maatwebsite\Excel\Facades\Excel;

class CSV
{
    /**
     * customer Invoice Report
     *
     * @param $input
     * @return string
     */
    public static function invoiceReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $invoiceReport = Report::invoiceReport($input);
            $invoices = $invoiceReport['invoices'];
            $total = $invoiceReport['total'];
            $date = $invoiceReport['date'];

            $sheetName = $businessProfile->name . '-' . 'invoiceReport';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($invoices, $total, $date, $businessProfile) {
                $excel->sheet('InvoiceReport', function($sheet) use ($invoices, $total, $date, $businessProfile){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Customer Invoice Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));
                    $sheet->row(7, array(
                        'Invoice No', 'Reference', 'Type', 'To', 'Date', 'Due Date', 'Status', 'Invoice Total'
                    ));

                    foreach ($invoices as $invoice) {
                        $sheet->appendRow(array(
                            $invoice->invoice_no, $invoice->reference, $invoice->type,
                            $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                            $invoice->date,
                            $invoice->date,
                            $invoice->status,
                            $invoice->amount,
                        ));
                    }

                    $sheet->appendRow(array(
                        'Total',' ', ' ', ' ', ' ', ' ', ' ', $total
                    ));

                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Customer Invoice report CSV" . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Sales Item Report
     * @param $input
     * @return string
     */

    public static function salesByItemsReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $salesItemReport = Report::salesByItemsReport($input);
            $items = $salesItemReport['items'];
            $totalAmount = $salesItemReport['totalAmount'];
            $totalQuantity = $salesItemReport['totalQuantity'];
            $totalAverage = $totalAmount / $totalQuantity;
            $date = $salesItemReport['date'];

            $sheetName = $businessProfile->name . '-' . 'salesItemsReport';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($items, $totalAmount, $totalQuantity, $totalAverage, $date, $businessProfile) {
                $excel->sheet('Sales Items report', function($sheet) use ($items, $totalAmount, $totalQuantity, $totalAverage, $date, $businessProfile){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Sales By Items Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));
                    $sheet->row(7, array(
                        'Items', 'Current Unit Price', 'Quantity Sold', 'Total', 'Average',
                    ));
                    foreach ($items as $item) {
                        $sheet->appendRow(array(
                            $item['name_code'], $item['current_price'], $item['quantity'], $item['total'], round($item['total'] / $item['quantity'], 2)
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total', ' ', ' ', $totalAmount, round($totalAverage, 2)
                    ));

                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Sales by items csv " . $e->getTraceAsString());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Profit and Loss Report
     *
     * @param $input
     * @return string
     */
    public static function profitandLossReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $profitandLossReport = Report::profitandLossReport($input);

            $profitandLoss = $profitandLossReport['profitandLoss'];
            $date = $profitandLossReport['date'];
            $totalIncome = $profitandLossReport['totalIncome'];
            $totalExpense = $profitandLossReport['totalExpense'];
            $invoiceList = $profitandLossReport['invoiceList'];
            $expenseList = $profitandLossReport['expenseList'];

            $sheetName = $businessProfile->name . '-' . 'profitandLossReport';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($profitandLoss, $date, $totalIncome, $totalExpense, $invoiceList, $expenseList, $businessProfile) {
                $excel->sheet('Profit and Loss report', function($sheet) use ($profitandLoss, $date, $totalIncome, $totalExpense, $invoiceList, $expenseList, $businessProfile){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Profit and Loss Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));

                    $sheet->row(7, array(
                        'Account', ' ', ' ', ' ', 'Total',
                    ));
                    foreach ($profitandLoss['income'] as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value as $account=>$amount) {
                            $sheet->appendRow(array(
                                ' ', $account, ' ', ' ', $amount,
                            ));
                        }
                    }
                    $sheet->appendRow(array(
                        'Total Income',' ', ' ', ' ', $totalIncome,
                    ));
                    $sheet->appendRow(array(
                        'Gross profit',' ', ' ', ' ', $totalIncome,
                    ));


                    foreach ($profitandLoss['expense'] as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value as $account=>$amount) {
                            $sheet->appendRow(array(
                                ' ', $account, ' ', ' ', $amount,
                            ));
                        }
                    }
                    $sheet->appendRow(array(
                        'Total Expense',' ', ' ', ' ', $totalExpense,
                    ));
                    if($totalIncome > $totalExpense) {
                        $sheet->appendRow(array(
                            'Net profit', ' ', ' ', ' ', $totalIncome - $totalExpense,
                        ));
                    } else {
                        $sheet->appendRow(array(
                            'Net Loss', ' ', ' ', ' ', $totalIncome - $totalExpense,
                        ));
                    }

                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;


        } catch (\Exception $e) {
            \Log::error("Profit and loss reports Csv " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Balance Sheet Report
     *
     * @param $input
     * @return string
     */
    public static function balanceSheetReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $profitandLoss = Report::profitandLossReport($input);
            $balanceSheetReport = Report::balanceSheetReport($input);

            $netProfit = $profitandLoss['totalIncome'] - $profitandLoss['totalExpense'];
            $balanceSheetAssets = $balanceSheetReport['balanceSheetAssets'];
            $balanceSheetLiabilities = $balanceSheetReport['balanceSheetLiabilities'];
            $balanceSheetOwners = $balanceSheetReport['balanceSheetOwners'];
            $date = $balanceSheetReport['date'];
            $totalAssets = $balanceSheetReport['totalAssets'];
            $totalLiabilities = $balanceSheetReport['totalLiabilities'];
            $totalOwners = $balanceSheetReport['totalOwners'] + $netProfit;

            $sheetName = $businessProfile->name . '-' . 'balanceSheetReport';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($netProfit, $balanceSheetAssets, $balanceSheetLiabilities, $balanceSheetOwners, $totalAssets, $totalLiabilities, $totalOwners, $businessProfile, $date) {
                $excel->sheet('Balance Sheet report', function($sheet) use ($netProfit, $balanceSheetAssets, $balanceSheetLiabilities, $balanceSheetOwners, $totalAssets, $totalLiabilities, $totalOwners, $businessProfile, $date){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Balance Sheet Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));

                    $sheet->row(7, array(
                        'Account', ' ', ' ', ' ', 'Total',
                    ));
                    foreach ($balanceSheetAssets as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value['account'] as $account=>$amount) {
                            $sheet->appendRow(array(
                                ' ', $account, ' ', ' ', $amount,
                            ));
                        }
                        $sheet->appendRow(array(
                            '', 'Total' . $key, ' ', ' ', $value['total'],
                        ));
                    }

                    $sheet->appendRow(array(
                        'Total Assets', '', ' ', ' ', $totalAssets,
                    ));

                    foreach ($balanceSheetLiabilities as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value['account'] as $account=>$amount) {
                            $sheet->appendRow(array(
                                ' ', $account, ' ', ' ', $amount,
                            ));
                        }
                        $sheet->appendRow(array(
                            '', 'Total' . $key, ' ', ' ', $value['total'],
                        ));
                    }

                    $sheet->appendRow(array(
                        'Total Liabilities', '', ' ', ' ', $totalLiabilities,
                    ));

                    foreach ($balanceSheetOwners as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value['account'] as $account=>$amount) {
                            $sheet->appendRow(array(
                                ' ', $account, ' ', ' ', $amount,
                            ));
                        }
                        if($netProfit > 0) {
                            $sheet->appendRow(array(
                                '', 'Net Profit', ' ', ' ', $netProfit,
                            ));
                        } else {
                            $sheet->appendRow(array(
                                '', 'Net Loss', ' ', ' ', $netProfit,
                            ));
                        }
                        $sheet->appendRow(array(
                            '', 'Total' . $key, ' ', ' ', $value['total'] + $netProfit,
                        ));
                    }

                    $sheet->appendRow(array(
                        'Total Owners', '', ' ', ' ', $totalOwners,
                    ));

                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;


        } catch (\Exception $e) {
            \Log::error("Balance reports pdf" . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Journal Report
     *
     * @param $input
     * @return string
     */
    public static function journalReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $journalReportDetails = Report::journalReport($input);
            $journalReport = $journalReportDetails['journalReport'];
            $date = $journalReportDetails['date'];
            $totalDebit = $journalReportDetails['totalDebit'];
            $totalCredit = $journalReportDetails['totalCredit'];

            $sheetName = $businessProfile->name . '-' . 'journalReport';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($date, $journalReport, $totalCredit, $totalDebit, $businessProfile, $date) {
                $excel->sheet('Journal Report', function($sheet) use ($date, $journalReport, $totalCredit, $totalDebit, $businessProfile, $date){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Journal Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));

                    $sheet->row(7, array(
                        'Date', 'Type', 'Name', 'Account', 'Debit', 'Cedit',
                    ));
                    foreach ($journalReport as $data) {
                        $sheet->appendRow(array(
                            $data['date'], $data['type'], $data['name'], $data['account'], $data['debit'], $data['credit']
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total', ' ', ' ', '', $totalDebit, $totalCredit
                    ));

                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Journal reports details" . $e->getMessage());
            return response()->error($e->getMessage());
        }
    }

    /**
     * Aged Receivables Report
     *
     * @param $input
     * @return string
     */
    public static function agedReceviablesReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $agedReceviablesReport = Report::agedReceviablesReport($input);
            $agedReceviables = $agedReceviablesReport['agedReceviables'];
            $totalReceviables = $agedReceviablesReport['totalReceviables'];
            $currentMonth = $agedReceviablesReport['currentMonth'];

            $sheetName = $businessProfile->name . '-' . 'agedReceivables';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($agedReceviables, $totalReceviables, $currentMonth, $businessProfile) {
                $excel->sheet('Aged Receivables', function($sheet) use ($agedReceviables, $totalReceviables, $currentMonth, $businessProfile){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Aged Receivables Report'
                    ));

                    $sheet->row(7, array(
                        'Date', 'Type', 'Name', $currentMonth,
                    ));
                    foreach ($agedReceviables as $data) {
                        $sheet->appendRow(array(
                            $data['date'], $data['transactionNo'], $data['customer'], $data['total']
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total Receivables', ' ', ' ', $totalReceviables
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch(\Exception $e) {
            \Log::error("Aged Receviables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Aged payables Report
     *
     * @param $input
     * @return string
     */
    public static function agedPayablesReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $agedPayablesReport = Report::agedPayablesReport($input);

            $agedPayables = $agedPayablesReport['agedPayables'];
            $totalPayables = $agedPayablesReport['totalPayables'];
            $currentMonth = $agedPayablesReport['currentMonth'];

            $sheetName = $businessProfile->name . '-' . 'agedPayables';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($agedPayables, $totalPayables, $currentMonth, $businessProfile) {
                $excel->sheet('Aged payables', function($sheet) use ($agedPayables, $totalPayables, $currentMonth, $businessProfile){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Aged Payables Report'
                    ));
                    $sheet->row(7, array(
                        'Date', 'Name', $currentMonth,
                    ));
                    foreach ($agedPayables as $data) {
                        $sheet->appendRow(array(
                            $data['date'], $data['vendor'], $data['total']
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total payables', ' ', $totalPayables
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch(\Exception $e) {
            \Log::error("Aged Payables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Trail Balance Report
     *
     * @param $input
     * @return string
     */
    public static function trailBalanceReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $trailBalanceReport = Report::trailBalanceReport($input);
            $trailBalanceData = $trailBalanceReport['trailBalanceData'];
            $totalDebit = $trailBalanceReport['totalDebit'];
            $totalCredit = $trailBalanceReport['totalCredit'];
            $date = $trailBalanceReport['date'];

            $sheetName = $businessProfile->name . '-' . 'trailBalance';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($trailBalanceData, $totalDebit, $totalCredit, $businessProfile, $date) {
                $excel->sheet('Trail Balance', function($sheet) use ($trailBalanceData, $totalDebit, $totalCredit, $businessProfile, $date){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Trail Balance Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));

                    $sheet->row(7, array(
                        'Account', ' ', ' ', 'Debit', 'Credit',
                    ));
                    foreach ($trailBalanceData as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value as $k=>$v) {
                            $sheet->appendRow(array(
                                '', $v['name'], ' ', $v['debit'], $v['credit']
                            ));
                        }
                    }
                    $sheet->appendRow(array(
                        'Total', ' ', ' ', $totalDebit, $totalCredit
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Trail Balance reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Accounting Transaction Report
     *
     * @param $input
     * @return string
     */
    public static function accountingTransactionReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $accountingTransactionReport = Report::accountingTransactionReport($input);

            $accountingTransactions = $accountingTransactionReport['accountingTransactions'];
            $date = $accountingTransactionReport['date'];
            $totalAmount = $accountingTransactionReport['totalAmount'];
            $totalGrossAmount = $accountingTransactionReport['totalGrossAmount'];

            $sheetName = $businessProfile->name . '-' . 'accountingTransaction';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($accountingTransactions, $totalAmount, $totalGrossAmount, $businessProfile, $date) {
                $excel->sheet('Accounting Transaction', function($sheet) use ($accountingTransactions, $totalAmount, $totalGrossAmount, $businessProfile, $date){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Accounting Transaction Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));

                    $sheet->row(7, array(
                        'Date', 'Account', 'Type', 'Transaction', 'Reference', 'Net Amount', 'Tax Amount', 'Gross', 'Tax Rate', 'Tax name'
                    ));
                    foreach ($accountingTransactions as $data) {
                        $sheet->appendRow(array(
                            $data['date'], $data['account'], $data['type'], $data['transaction'], $data['reference'], $data['net_amount'],
                            $data['tax_amount'], $data['gross'], $data['tax_rate'], $data['tax_name']
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total', ' ', ' ', ' ', ' ', $totalAmount, ' ', $totalGrossAmount, ' ', ' '
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;


        } catch (\Exception $e) {
            \Log::error("Accounting Transaction reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Invoice Receivables Report
     *
     * @param $input
     * @return string
     */
    public static function invoiceReceivablesReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $invoiceReceivablesReport = Report::invoiceReceivablesReport($input);
            $invoiceReceivables = $invoiceReceivablesReport['invoiceReceivables'];
            $date = $invoiceReceivablesReport['date'];
            $totalAmount = $invoiceReceivablesReport['totalAmount'];

            $sheetName = $businessProfile->name . '-' . 'invoiceReceivables';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($invoiceReceivables, $totalAmount, $businessProfile, $date) {
                $excel->sheet('Invoice Receivables', function($sheet) use ($invoiceReceivables, $totalAmount, $businessProfile, $date){
                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Trail Balance Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . ' - ' . $date['to']
                    ));
                    $sheet->row(7, array(
                        'Date', 'Account', 'Type', 'Transaction', 'Reference', 'Quantity', 'Price', 'Net Amount', 'Tax Amount', 'Discount', 'Tax Rate', 'Tax name'
                    ));
                    foreach ($invoiceReceivables as $data) {
                        $sheet->appendRow(array(
                            $data['date'], $data['account'], $data['invoice_no'], $data['transaction'], $data['reference'], $data['quantity'], $data['unit_price'],
                            $data['net_amount'], $data['tax_amount'], $data['discount'], $data['tax_rate'], $data['tax_name']
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total', ' ', ' ', ' ', ' ', ' ', $totalAmount, ' ', ' ', ' ', ' '
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Invoice Receivables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Tax Report
     *
     * @param $input
     * @return string
     */
    public static function taxReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $taxReportDetails = Report::taxReport($input);
            $taxReport = $taxReportDetails['taxReport'];
            $date = $taxReportDetails['date'];

            $sheetName = $businessProfile->name . '-' . 'taxReport';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($taxReport, $businessProfile, $date) {
                $excel->sheet('Tax Report', function($sheet) use ($taxReport, $businessProfile, $date) {

                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Tax Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . '-' . $date['to']
                    ));
                    $sheet->row(7, array(
                        ' ','Date', 'Account', 'Transaction', 'Net Amount', 'Tax', 'Gross'
                    ));
                    foreach ($taxReport as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value as $k=>$v) {
                            $sheet->appendRow(array(
                                ' ', $v['date'], $v['account'], $v['transaction'], $v['net_amount'], $v['tax'], $v['gross']
                            ));
                        }
                    }
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;
        } catch (\Exception $e) {
            \Log::error("Tax reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Cash Summary Report
     *
     * @param $input
     * @return string
     */
    public static function cashSummaryReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $cashSummaryReport = Report::cashSummaryReport($input);

            $incomeCashSummary = $cashSummaryReport['incomeCashSummary'];
            $expenseCashSummary = $cashSummaryReport['expenseCashSummary'];
            $totalIncome = $cashSummaryReport['totalIncome'];
            $totalExpense = $cashSummaryReport['totalExpense'];
            $invoiceList = $cashSummaryReport['invoiceList'];
            $expenseList = $cashSummaryReport['expenseList'];
            $date = $cashSummaryReport['date'];


            $sheetName = $businessProfile->name . '-' . 'cashSummary';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($incomeCashSummary, $expenseCashSummary, $totalIncome, $totalExpense, $businessProfile, $date) {
                $excel->sheet('Cash Summary Report', function($sheet) use ($incomeCashSummary, $expenseCashSummary, $totalIncome, $totalExpense, $businessProfile, $date) {

                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Cash Summary Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . '-' . $date['to']
                    ));

                    $sheet->row(7, array(
                        ' ', ' ', ' ', 'Amount'
                    ));
                    foreach ($incomeCashSummary as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value as $k=>$v) {
                            $sheet->appendRow(array(
                                ' ', $k, ' ', $v
                            ));
                        }
                    }

                    $sheet->appendRow(array(
                       'Total Income', ' ', ' ', $totalIncome
                    ));

                    foreach ($expenseCashSummary as $key=>$value) {
                        $sheet->appendRow(array(
                            $key
                        ));
                        foreach ($value as $k=>$v) {
                            $sheet->appendRow(array(
                                ' ', $k, ' ', $v
                            ));
                        }
                    }

                    $sheet->appendRow(array(
                        'Total Expense', ' ', ' ', $totalExpense
                    ));

                    $sheet->appendRow(array(
                        'Net Cash', ' ', ' ', $totalIncome - $totalExpense
                    ));

                    $sheet->appendRow(array(
                        'Cash Account', ' ', ' ', $totalIncome - $totalExpense
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Cash Summary reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    /**
     * Cash Account Report
     *
     * @param $input
     * @return string
     */
    public static function cashAccountReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $cashAccountReport = Report::cashAccountReport($input);
            $cashAccount = $cashAccountReport['cashAccount'];
            $totalDebit = $cashAccountReport['totalDebit'];
            $totalCredit = $cashAccountReport['totalCredit'];
            $date = $cashAccountReport['date'];

            $sheetName = $businessProfile->name . '-' . 'cashAccount';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($cashAccount, $totalCredit, $totalDebit, $businessProfile, $date) {
                $excel->sheet('Cash Summary Report', function($sheet) use ($cashAccount, $totalCredit, $totalDebit, $businessProfile, $date) {

                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'Cash Account Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . '-' . $date['to']
                    ));

                    $sheet->appendRow(7, array(
                        'Date', 'Type', 'Transaction', 'Debit', 'Credit'
                    ));
                    foreach ($cashAccount as $data) {
                        $sheet->appendRow(array(
                            $data['date'], $data['type'], $data['transaction'], $data['debit'], $data['credit']
                        ));
                    }

                    $sheet->appendRow(array(
                        'Total Balance', ' ', ' ', $totalDebit, $totalCredit
                    ));
                    $sheet->appendRow(array(
                        'Closing Balance', ' ', ' ', $totalDebit - $totalCredit, ''
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("Cash Account reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }


    /**
     * General Ledger Report
     *
     * @param $input
     * @return string
     */
    public static function generalLedgerReport($input)
    {
        try {
            $businessProfile = BusinessProfile::find($input['business_profile_id']);
            $generalLedgerReport = Report::generalLedgerReport($input);

            $generalLedgerData = $generalLedgerReport['generalLedgerData'];
            $totalDebit = $generalLedgerReport['totalDebit'];
            $totalCredit = $generalLedgerReport['totalCredit'];
            $date = $generalLedgerReport['date'];

            $sheetName = $businessProfile->name . '-' . 'generalLedger';
            $pathToFile = public_path() . '/csv/';
            $appUrl = env('APP_URL');

            Excel::create($sheetName, function($excel) use($generalLedgerData, $totalCredit, $totalDebit, $businessProfile, $date) {
                $excel->sheet('Tax Report', function($sheet) use ($generalLedgerData, $totalCredit, $totalDebit, $businessProfile, $date) {

                    $sheet->row(1, array(
                        '', '', '', $businessProfile->name
                    ));
                    $sheet->row(2, array(
                        '', '', '', 'General Ledger Report'
                    ));
                    $sheet->row(3, array(
                        '', '', '', $date['from'] . '-' . $date['to']
                    ));
                    $sheet->row(7, array(
                        'Account', 'Debit', 'Credit'
                    ));
                    foreach ($generalLedgerData as $data) {
                        $sheet->appendRow(array(
                            $data['name'], $data['debit'], $data['credit']
                        ));
                    }
                    $sheet->appendRow(array(
                        'Total', $totalDebit, $totalCredit
                    ));
                });

            })->store('xls', $pathToFile);

            $filePath = $appUrl . '/csv/' . $sheetName . '.xls';

            return $filePath;

        } catch (\Exception $e) {
            \Log::error("General Ledger reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }
}