<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model {

    protected $table = 'vendors';

    protected $fillable = array('business_profile_id', 'title', 'first_name', 'last_name','email', 'phone', 'mobile',
                                'company_name', 'website', 'notes', 'attachment', 'suppliers',
                                'billing_street', 'billing_zip', 'billing_city', 'billing_state', 'billing_country',
                                'shipping_street', 'shipping_zip', 'shipping_city', 'shipping_state', 'shipping_country',
                                'status', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'first_name' => 'required',
        'email' => 'required',
    ];

    public function businessVendor()
    {
        return $this->hasMany('\App\BusinessVendor');
    }

    public function businessProfile()
    {
        return $this->belongsToMany('\App\BusinessProfile', 'business_vendors', 'vendor_id', 'business_profile_id');
    }

    public function businessExpense()
    {
        return $this->belongsToMany('\App\BusinessExpense', 'business_expense_vendors', 'vendor_id', 'business_expense_id');
    }

    public function businessExpenseVendor()
    {
        return $this->hasMany('\App\BusinessExpenseVendor');
    }

    public function businessItem()
    {
        return $this->hasMany('\App\BusinessItem');
    }

    public function businessPurchaseOrder()
    {
        return $this->hasMany('\App\BusinessPurchaseOrder');
    }

    public function businessCheque()
    {
        return $this->hasMany('\App\BusinessCheque');
    }
}