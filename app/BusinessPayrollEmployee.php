<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPayrollEmployee extends Model {

    protected $table = 'business_payroll_employees';

    protected $fillable = array('business_payroll_id', 'employee_id');

    public $timestamps = false;

    public function businessPayroll()
    {
        return $this->belongsTo('\App\BusinessPayroll', 'business_payroll_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('\App\Employee', 'employee_id', 'id');
    }
}