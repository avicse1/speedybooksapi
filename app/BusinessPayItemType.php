<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPayItemType extends Model {

    protected $table = 'business_pay_item_types';

    protected $fillable = array('name', 'description');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required',
        'description' => 'required',
    ];

    public function businessPayItem()
    {
        return $this->hasMany('\App\BusinessPayItem');
    }
}