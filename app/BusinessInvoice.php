<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessInvoice extends Model {

    protected $table = 'business_invoices';

    protected $fillable = array('business_profile_id', 'account_id', 'tax_rate_id', 'reconcile_id', 'invoice_no', 'date', 'reference', 'due_date', 'status', 'description', 'payment_term', 'amount', 'discount', 'type', 'tax_amount', 'attachment', 'banking_type', 'payment_method', 'payment_method_reference', 'credit_card_no', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'business_profile_id' => 'required',
        'invoice_no' => 'required',
        'date' => 'required',
        'due_date' => 'required',
    ];

    public function businessInvoiceItem()
    {
        return $this->hasMany('\App\BusinessInvoiceItem');
    }

    public function customer()
    {
        return $this->belongsToMany('\App\Customer', 'business_invoice_customers', 'business_invoice_id', 'customer_id');
    }

    public function businessInvoiceCustomer()
    {
        return $this->hasMany('\App\BusinessInvoiceCustomer');
    }

    public function notes()
    {
        return $this->morphToMany('\App\Note', 'taggable');
    }

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function businessInvoiceConfiguration()
    {
        return $this->hasOne('\App\BusinessInvoiceConfiguration');
    }

    public function createdBy()
    {
        return $this->belongsTo('\App\User', 'created_by', 'id');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function taxRate()
    {
        return $this->belongsTo('\App\TaxRate');
    }

    public function businessReconcile()
    {
        return $this->belongsTo('\App\BusinessReconcile');
    }
}