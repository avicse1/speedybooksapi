<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessReconcile extends Model {

    protected $table = 'business_reconciles';

    protected $fillable = array('business_profile_id', 'account_id', 'ending_date', 'beginning_balance', 'reconcile_on', 'ending_balance', 'cheque_payments', 'deposite_payments', 'auto_adjustment', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'account_id' => 'required',
        'ending_balance' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function businessExpense()
    {
        return $this->hasMany('\App\BusinessExpense');
    }

    public function businessInvoice()
    {
        return $this->hasMany('\App\BusinessInvoice');
    }

    public function businessCheque()
    {
        return $this->hasMany('\App\BusinessCheque');
    }

    public function businessJournalItem()
    {
        return $this->hasMany('\App\BusinessJournalItem');
    }
}