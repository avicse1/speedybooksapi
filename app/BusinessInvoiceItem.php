<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessInvoiceItem extends Model {

    protected $table = 'business_invoice_items';

    protected $fillable = array('business_invoice_id', 'business_item_id', 'account_id', 'name', 'description', 'quantity', 'price', 'discount', 'amount', 'item_code');

    public $timestamps = false;

    public static $rules = [
        'business_invoice_id' => 'required',
        'business_item_id' => 'required',
        'account_id' => 'required',
        'name' => 'required',
        'description' => 'required',
        'quantity' => 'required',
        'price' => 'required',
        'amount' => 'required',

    ];

    public function businessInvoice()
    {
        return $this->belongsTo('\App\BusinessInvoice');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function businessItem()
    {
        return $this->belongsTo('\App\BusinessItem');
    }
}