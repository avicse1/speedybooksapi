<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessCheque extends Model {

    protected $table = 'business_cheques';

    protected $fillable = array('business_profile_id', 'vendor_id', 'account_id', 'reconcile_id','cheque_no', 'payment_term', 'status', 'date', 'description', 'amount', 'created_by', 'deleted_by', 'updated_by');

    public static $rules = [
        'vendor_id' => 'required',
        'account_id' => 'required',
        'cheque_no' => 'required',
        'date' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function vendor()
    {
        return $this->belongsTo('\App\Vendor');
    }

    public function businessReconcile()
    {
        return $this->belongsTo('\App\BusinessReconcile');
    }

    public function businessChequeItem()
    {
        return $this->hasOne('\App\BusinessChequeItem');
    }
}