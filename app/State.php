<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model {

    protected $table = 'states';

    protected $fillable = array('country_id', 'name');

    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('\App\User');
    }

    public function country()
    {
        return $this->belongsTo('\App\Country');
    }

    public function city()
    {
        return $this->hasMany('\App\City');
    }

    public function businessProfile()
    {
        return $this->hasMany('\App\BusinessProfile');
    }
}