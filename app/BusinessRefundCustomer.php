<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessRefundCustomer extends Model {

    protected $table = 'business_refund_customers';

    protected $fillable = array('business_refund_id', 'customer_id');

    public $timestamps = false;

    public function businessRefund()
    {
        return $this->belongsTo('\App\BusinessRefund', 'business_refund_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer', 'customer_id', 'id');
    }
}