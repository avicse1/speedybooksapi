<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessVendor extends Model {

    protected $table = 'business_vendors';

    protected $fillable = array('business_profile_id', 'vendor_id');

    public $timestamps = false;

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile', 'business_profile_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo('\App\Vendor', 'vendor_id', 'id');
    }
}