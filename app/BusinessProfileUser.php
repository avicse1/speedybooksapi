<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessProfileUser extends Model
{

    protected $table = 'business_profile_users';

    protected $fillable = array('business_profile_id', 'user_id');

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }
}