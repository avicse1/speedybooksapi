<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Taggable extends Model {

    protected $table = 'taggables';

    protected $fillable = array('note_id', 'taggable_id', 'taggable_type');

    public $timestamps = false;

    public function businessInvoice()
    {
        return $this->morphedByMany('\App\BusinessInvoice', 'taggable');
    }

    public function businessExpense()
    {
        return $this->morphedByMany('\App\BusinessExpense', 'taggable');
    }

    public function businessRefund()
    {
        return $this->morphedByMany('\App\BusinessRefund', 'taggable');
    }

    public function businessPurchaseOrder()
    {
        return $this->morphedByMany('\App\BusinessPurchaseOrder', 'taggable');
    }

    public function businessReimbursement()
    {
        return $this->morphedByMany('\App\BusinessReimbursement', 'taggable');
    }

    public function businessPayroll()
    {
        return $this->morphedByMany('\App\BusinessPayroll', 'taggable');
    }

    public function taggable() {
        return $this->morphTo();
    }
}