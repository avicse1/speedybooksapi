<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPurchaseOrderItem extends Model {

    protected $table = 'business_purchase_order_items';

    protected $fillable = array('business_purchase_order_id', 'business_item_id', 'account_id', 'item_code', 'quantity', 'discount', 'price', 'amount', 'description');

    public $timestamps = false;

    public static $rules = [
        'business_purchase_order_id' => 'required',
        'business_item_id' => 'required',
        'account_id' => 'required',
        'item_code' => 'required',
        'quantity' => 'required',
        'discount' => 'required',
        'price' => 'required',
        'amount' => 'required',
        'description' => 'required',
    ];

    public function businessPurchaseOrder()
    {
        return $this->belongsTo('\App\BusinessPurchaseOrder');
    }

    public function businessItem()
    {
        return $this->belongsTo('\App\BusinessItem');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }
}