<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model {

    protected $table = 'accounts';

    protected $fillable = array('parent_id', 'code', 'name', 'description', 'detail_type', 'user_id');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required',
        'description' => 'required',
    ];

    public function businessInvoiceItem()
    {
        return $this->hasMany('\App\BusinessInvoiceItem');
    }

    public function businessExpenseItem()
    {
        return $this->hasMany('\App\BusinessExpenseItem');
    }

    public function businessItem()
    {
        return $this->hasMany('\App\BusinessItem');
    }

    public function businessPurchaseOrderItem()
    {
        return $this->hasMany('\App\BusinessPurchaseOrderItem');
    }

    public function businessReimbursementItem()
    {
        return $this->hasMany('\App\BusinessReimbursementItem');
    }

    public function businessPayItem()
    {
        return $this->hasMany('\App\BusinessPayItem');
    }

    public function businessInvoice()
    {
        return $this->hasMany('\App\BusinessInvoice');
    }

    public function businessExpense()
    {
        return $this->hasMany('\App\BusinessExpense');
    }

    public function bankAccount()
    {
        return $this->hasMany('\App\BankAccount');
    }

    public function businessJournalItem()
    {
        return $this->hasMany('\App\BusinessJournalItem');
    }

    public function businessCheque()
    {
        return $this->hasMany('\App\BusinessCheque');
    }

    public function businessChequeItem()
    {
        return $this->hasMany('\App\BusinessChequeItem');
    }

    public function businessReconcile()
    {
        return $this->hasMany('\App\BusinessReconcile');
    }

    public function accountBalance()
    {
        return $this->hasMany('\App\AccountBalance');
    }
}