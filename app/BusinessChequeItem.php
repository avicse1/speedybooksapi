<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessChequeItem extends Model {

    protected $table = 'business_cheque_items';

    protected $fillable = array('business_cheque_id', 'account_id', 'description', 'amount');

    public $timestamps = false;

    public function businessCheque()
    {
        return $this->belongsTo('\App\BusinessCheque');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }
}