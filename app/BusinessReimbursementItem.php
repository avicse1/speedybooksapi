<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessReimbursementItem extends Model {

    protected $table = 'business_reimbursement_items';

    protected $fillable = array('business_reimbursement_id', 'business_item_id', 'account_id', 'item_code', 'quantity', 'discount', 'price', 'amount', 'description');

    public $timestamps = false;

    public static $rules = [
        'business_reimbursement_id' => 'required',
        'business_item_id' => 'required',
        'account_id' => 'required',
        'item_code' => 'required',
        'quantity' => 'required',
        'discount' => 'required',
        'price' => 'required',
        'amount' => 'required',
        'description' => 'required',
    ];

    public function businessReimbursement()
    {
        return $this->belongsTo('\App\BusinessReimbursement');
    }

    public function businessItem()
    {
        return $this->belongsTo('\App\BusinessItem');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }
}