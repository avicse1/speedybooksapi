<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    protected $table = 'cities';

    protected $fillable = array('state_id', 'name');

    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('\App\User');
    }

    public function state()
    {
        return $this->hasMany('\App\State');
    }
}