<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPayrollItem extends Model {

    protected $table = 'business_payroll_items';

    protected $fillable = array('business_payroll_id', 'business_pay_item_id', 'item', 'description', 'quantity', 'price', 'amount');

    public $timestamps = false;

    public static $rules = [
        'business_invoice_id' => 'required',
        'description' => 'required',
        'quantity' => 'required',
        'price' => 'required',
        'amount' => 'required',
    ];

    public function businessPayroll()
    {
        return $this->belongsTo('\App\BusinessPayroll');
    }

    public function businessPayItem()
    {
        return $this->belongsTo('\App\BusinessPayItem');
    }
}