<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessJournal extends Model {

    protected $table = 'business_journals';

    protected $fillable = array('business_profile_id', 'name', 'date', 'status', 'total_debit', 'total_credit', 'banking_type', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'name' => 'required',
        'date' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function businessJournalItem()
    {
        return $this->hasMany('\App\BusinessJournalItem');
    }
}