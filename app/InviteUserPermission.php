<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class InviteUserPermission extends Model {

    protected $table = 'invite_user_permissions';

    protected $fillable = array('business_profile_id', 'user_id' , 'first_name', 'last_name', 'email', 'invoice', 'expense', 'setting', 'reports', 'payroll', 'manage_users', 'banking', 'invite_by', 'status', 'expiry_date', 'expired', 'cpa_permission', 'invited_by_cpa');

    public $timestamps = false;

    public static $rules = [
        'invite_by' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo('\App\User');
    }
}