<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessInvoiceCustomer extends Model {

    protected $table = 'business_invoice_customers';

    protected $fillable = array('business_invoice_id', 'customer_id');

    public $timestamps = false;

    public function businessInvoice()
    {
        return $this->belongsTo('\App\BusinessInvoice', 'business_invoice_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer', 'customer_id', 'id');
    }
}