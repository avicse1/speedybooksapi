<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessItem extends Model {

    protected $table = 'business_items';

    protected $fillable = array('business_profile_id', 'account_id', 'vendor_id', 'name', 'amount', 'item_code', 'description', 'unit', 'selling_price');

    public $timestamps = false;

    public static $rules = [
        'business_profile_id' => 'required',
        'account_id' => 'required',
        'name' => 'required',
        'amount' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function businessInvoiceItem()
    {
        return $this->hasMany('\App\BusinessInvoiceItem');
    }

    public function businessExpenseItem()
    {
        return $this->hasMany('\App\BusinessExpenseItem');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function vendor()
    {
        return $this->belongsTo('\App\Vendor');
    }

    public function businessPurchaseOrderItem()
    {
        return $this->hasMany('\App\BusinessPurchaseOrderItem');
    }

    public function businessReimbursementItem()
    {
        return $this->hasMany('\App\BusinessReimbursementItem');
    }
}