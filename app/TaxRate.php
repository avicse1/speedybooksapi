<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxRate extends Model {

    protected $table = 'tax_rates';

    protected $fillable = array('name', 'component', 'tax_rates');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required',
        'component' => 'required',
        'tax_rates' => 'required'
    ];

    public function businessInvoice() {
        return $this->hasMany('\App\BusinessInvoice');
    }

    public function businessExpense()
    {
        return $this->hasMany('\App\BusinessExpense');
    }

    public function businessPurchaseOrder()
    {
        return $this->hasMany('\App\BusinessPurchaseOrder');
    }

    public function businessReimbursement()
    {
        return $this->hasMany('\App\BusinessReimbursement');
    }

    public function businessPayroll()
    {
        return $this->hasMany('\App\BusinessPayroll');
    }
}