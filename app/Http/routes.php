<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::get('invoices/view/{id}', 'BusinessInvoiceController@invoiceView');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

/*
|--------------------------------------------------------------------------
| Application API Routes
|--------------------------------------------------------------------------
|
| This route group applies the "api" middleware group to every route
| it contains. The "api" middleware group is defined in your HTTP
| kernel and includes JWT Auth.
|
*/


$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => ['cors']], function ($api) {
    // This route requires authentication.
    $api->group(['middleware' => 'api.auth'], function ($api) {

        $api->group(['prefix' => 'business'], function ($api) {
            $api->resource('/profiles', 'App\Http\Controllers\BusinessProfileController');

            $api->get('/invoices/generate', 'App\Http\Controllers\BusinessInvoiceController@generateInvoiceNo');
            $api->get('/invoices/due', 'App\Http\Controllers\BusinessInvoiceController@getDueInvoices');
            $api->get('/invoices/type/{type}', 'App\Http\Controllers\BusinessInvoiceController@getInvoiceType');
            $api->get('/invoices/payment/type/{type}', 'App\Http\Controllers\BusinessInvoiceController@getInvoicePaymentType');
            $api->get('/invoices/upcoming/payment/list', 'App\Http\Controllers\BusinessInvoiceController@upcomingPaymentList');
            $api->post('/invoices/send/{id}', 'App\Http\Controllers\BusinessInvoiceController@sendInvoice');
            $api->post('/invoices/receive/payment/{id}', 'App\Http\Controllers\BusinessInvoiceController@receivePayment');
            $api->resource('/invoices', 'App\Http\Controllers\BusinessInvoiceController');

            $api->get('/expenses/due', 'App\Http\Controllers\BusinessExpenseController@getDueExpenses');
            $api->post('/expenses/make/payment/{id}', 'App\Http\Controllers\BusinessExpenseController@makePayment');
            $api->get('/expenses/type/{type}', 'App\Http\Controllers\BusinessExpenseController@getExpensesType');
            $api->resource('/expenses', 'App\Http\Controllers\BusinessExpenseController');
            $api->resource('/items', 'App\Http\Controllers\BusinessItemController');
            $api->resource('/refunds', 'App\Http\Controllers\BusinessRefundController');

            $api->post('/purchaseOrder/send/{id}', 'App\Http\Controllers\BusinessPurchaseOrderController@sendPurchaseOrder');
            $api->get('/purchaseOrder/generate', 'App\Http\Controllers\BusinessPurchaseOrderController@generatePurchaseOrderNo');
            $api->resource('/purchaseOrder', 'App\Http\Controllers\BusinessPurchaseOrderController');

            $api->get('/reimbursement/generate', 'App\Http\Controllers\BusinessReimbursementController@generateReimbursementNo');
            $api->resource('/reimbursement', 'App\Http\Controllers\BusinessReimbursementController');

            $api->resource('/payItemType', 'App\Http\Controllers\BusinessPayItemTypeController');
            $api->resource('/payItems', 'App\Http\Controllers\BusinessPayItemController');

            $api->post('/payrolls/send/email/{id}', 'App\Http\Controllers\BusinessPayrollController@sendEmail');
            $api->resource('/payrolls', 'App\Http\Controllers\BusinessPayrollController');

            $api->resource('journals', 'App\Http\Controllers\BusinessJournalController');

            $api->resource('cheques', 'App\Http\Controllers\BusinessChequeController');

            $api->post('/reconcile/account/{id}', 'App\Http\Controllers\BusinessReconcileController@reconcileAccount');
            $api->get('/reconcile/latest/{id}', 'App\Http\Controllers\BusinessReconcileController@reconcileLatest');
            $api->resource('/reconcile', 'App\Http\Controllers\BusinessReconcileController');
        });

        $api->group(['prefix' => 'location'], function ($api) {
            $api->get('/', 'App\Http\Controllers\LocationController@index');
            $api->get('/country', 'App\Http\Controllers\LocationController@getCountries');
            $api->get('/country/{id}', 'App\Http\Controllers\LocationController@getStates');
            $api->get('/state/{id}', 'App\Http\Controllers\LocationController@getCities');
        });

        $api->get('user-permissions/inactive/{id}', 'App\Http\Controllers\InviteUserPermissionController@inactiveInviteUser');
        $api->get('user-permissions/resend/{id}', 'App\Http\Controllers\InviteUserPermissionController@resendInvitation');
        $api->resource('user-permissions', 'App\Http\Controllers\InviteUserPermissionController');
        $api->resource('employees', 'App\Http\Controllers\EmployeeController');
        $api->resource('customers', 'App\Http\Controllers\CustomerController');
        $api->resource('vendors', 'App\Http\Controllers\VendorController');
        $api->resource('businessNatures', 'App\Http\Controllers\BusinessNatureController');
        $api->resource('financialYears', 'App\Http\Controllers\FinancialYearController');

        $api->group(['prefix' => 'dashboard'], function ($api) {
            $api->get('/users/count', 'App\Http\Controllers\DashboardController@userCount');
            $api->get('/invoices/upcoming/payment', 'App\Http\Controllers\DashboardController@invoiceUpcomingPayment');
            $api->get('/recent/activity', 'App\Http\Controllers\DashboardController@recentActivity');
            $api->get('/invoices/count', 'App\Http\Controllers\DashboardController@invoiceCount');
            $api->get('/expense/report', 'App\Http\Controllers\DashboardController@expenseReport');
            $api->get('/profit-loss/report', 'App\Http\Controllers\DashboardController@profitLossReport');
        });

        $api->group(['prefix' => 'report'], function ($api) {
            $api->get('/customer/invoice', 'App\Http\Controllers\ReportController@invoiceReport');
            $api->get('/sales-by/items', 'App\Http\Controllers\ReportController@salesByItemsReport');
            $api->get('/profit-and-loss', 'App\Http\Controllers\ReportController@profitandLossReport');
            $api->get('/balance-sheet', 'App\Http\Controllers\ReportController@balanceSheetReport');
            $api->get('/balance-sheet-details/{type}', 'App\Http\Controllers\ReportController@balanceSheetDetailsReport');
            $api->get('/journal-report', 'App\Http\Controllers\ReportController@journalReport');
            $api->get('/aged-payables', 'App\Http\Controllers\ReportController@agedPayablesReport');
            $api->get('/aged-receivables', 'App\Http\Controllers\ReportController@agedReceviablesReport');
            $api->get('/trail-balance', 'App\Http\Controllers\ReportController@trailBalanceReport');
            $api->get('/accounting-transaction', 'App\Http\Controllers\ReportController@accountingTransactionReport');
            $api->get('/invoice-receivables', 'App\Http\Controllers\ReportController@invoiceReceivablesReport');
            $api->get('/tax-report', 'App\Http\Controllers\ReportController@taxReport');
            $api->get('/cash-summary', 'App\Http\Controllers\ReportController@cashSummaryReport');
            $api->get('/cash-account', 'App\Http\Controllers\ReportController@cashAccountReport');
            $api->get('/general-ledger', 'App\Http\Controllers\ReportController@generalLedgerReport');
            $api->get('/cash-flow', 'App\Http\Controllers\ReportController@cashFlowReport');
        });
    });

    $api->get('invoices/details/{id}', 'App\Http\Controllers\BusinessInvoiceController@getInvoiceDetails');

    $api->get('users/cpa', 'App\Http\Controllers\UsersController@getCPAUsers');
    $api->get('user-permissions/signup/{token}', 'App\Http\Controllers\InviteUserPermissionController@getInviteUserDetails');
	$api->post('forgot/password', 'App\Http\Controllers\UsersController@forgotPassword');
	$api->post('users/authenticate', 'App\Http\Controllers\UsersController@authenticate');
    $api->post('users/password', 'App\Http\Controllers\UsersController@userPassword');
    $api->post('users/change/password', 'App\Http\Controllers\UsersController@userChangePassword');
	$api->resource('users', 'App\Http\Controllers\UsersController');
	$api->resource('files', 'App\Http\Controllers\FileController');
    $api->resource('currency', 'App\Http\Controllers\CurrencyController');

    $api->get('accounts/receive/payment/list', 'App\Http\Controllers\AccountController@getReceivePaymentAccountList');
    $api->get('accounts/parent/list', 'App\Http\Controllers\AccountController@getParentAccountList');
    $api->get('accounts/detail/type/list/{id}', 'App\Http\Controllers\AccountController@getDetailTypeList');
    $api->resource('accounts', 'App\Http\Controllers\AccountController');

    $api->resource('taxRates', 'App\Http\Controllers\TaxRateController');

    $api->post('bankAccounts/invoice/receive/payment/{id}', 'App\Http\Controllers\BankAccountController@invoiceReceivePayment');
    $api->post('bankAccounts/expense/make/payment/{id}', 'App\Http\Controllers\BankAccountController@expenseMakePayment');
    $api->post('bankAccounts/upload/{id}', 'App\Http\Controllers\BankAccountController@uploadTransaction');
    $api->post('bankAccounts/transaction/{id}', 'App\Http\Controllers\BankAccountController@saveBankTransaction');
    $api->post('bankAccounts/create/journal/{id}', 'App\Http\Controllers\BankAccountController@createJournal');
    $api->get('bankAccounts/undo/transaction/{id}', 'App\Http\Controllers\BankAccountController@undoTransaction');
    $api->resource('bankAccounts', 'App\Http\Controllers\BankAccountController');

    $api->post('bankAccountTransaction/change/account/{id}', 'App\Http\Controllers\BankAccountTransactionController@changeTransactionAccount');
    $api->resource('bankAccountTransaction', 'App\Http\Controllers\BankAccountTransactionController');

	$api->group(['prefix' => 'location'], function ($api) {
		$api->get('/', 'App\Http\Controllers\LocationController@index');
		$api->get('/country', 'App\Http\Controllers\LocationController@getCountries');
		$api->get('/country/{id}', 'App\Http\Controllers\LocationController@getStates');
		$api->get('/state/{id}', 'App\Http\Controllers\LocationController@getCities');
	});

    $api->group(['prefix' => 'cron'], function ($api) {
		$api->get('/expense/recurring', 'App\Http\Controllers\CronController@expenseRecurring');
		$api->get('/sales/recurring', 'App\Http\Controllers\CronController@salesRecurring');
	});
});

Route::get('test', function () {
   return view('payroll.payroll_pdf_layout');

});
