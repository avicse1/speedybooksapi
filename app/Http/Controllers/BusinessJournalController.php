<?php

namespace App\Http\Controllers;

use App\BusinessJournal;
use App\BusinessJournalItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;

class BusinessJournalController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessJournals = BusinessJournal::with('businessJournalItem')
                ->where('business_profile_id', $this->_business_profile_id)
                ->whereNull('deleted_at')
                ->where('banking_type', 0)
                ->orderBy('created_at', 'desc')->get();

            return response()->success('', compact('businessJournals'));
        } catch (\Exception $e) {
            \Log::error('Business Journals Details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $validation = Validator::make($input, BusinessJournal::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $totalDebit = 0;
                $totalCredit = 0;
                $input['created_by'] = $this->_user_id;
                $businessJournal = BusinessJournal::create($input);

                if (!empty($input['Items'])) {
                    $items = $input['Items'];
                    foreach ($items as $item) {
                        if(!empty($item['account_id'])) {
                            $totalDebit += $item['debit'];
                            $totalCredit += $item['credit'];
                            $item['business_journal_id'] = $businessJournal->id;
                            BusinessJournalItem::create($item);
                        }
                    }
                }
                $businessJournal->total_debit = $totalDebit;
                $businessJournal->total_credit = $totalCredit;
                $businessJournal->save();

                return response()->success('Business Journal has been created successfully!', compact('businessJournal'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Journal creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessJournal = BusinessJournal::with('businessJournalItem')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessJournal->businessJournalItem;
            foreach ($items as $item) {
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
            }
            return response()->success('', compact('businessJournal'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request) {

        $input = $request->all();

        if (!empty($id)) {
            $businessJournal = BusinessJournal::find($id);
            if (!empty($businessJournal)) {
                $businessJournal->update($input);
                $totalDebit = 0;
                $totalCredit = 0;
                if(!empty($input['Items'])) {
                    BusinessJournalItem::where('business_Journal_id', $businessJournal->id)->delete();
                    $items = $input['Items'];

                    foreach ($items as $item) {
                        if(!empty($item['account_id'])) {
                            $totalDebit += $item['debit'];
                            $totalCredit += $item['credit'];
                            $item['business_journal_id'] = $businessJournal->id;
                            BusinessJournalItem::create($item);
                        }
                    }
                }
                $businessJournal->total_debit = $totalDebit;
                $businessJournal->total_credit = $totalCredit;
                $businessJournal->save();

                return response()->success('Business Journal has been updated successfully!', compact('businessJournal'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessJournal = BusinessJournal::find($id);

                $businessJournal->deleted_at = \Carbon\Carbon::now();
                $businessJournal->save();

                return response()->success('Business Journal has been deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
