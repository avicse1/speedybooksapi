<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\BusinessEmployee;
use App\BusinessProfile;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $employees = Employee::with('businessProfile')->where('status', '!=', 'DELETED')->where('is_terminated', 0)->whereNull('deleted_at')->get();

            return response()->success('', compact('employees'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $existEmployee = Employee::where('email', $input['email'])->first();
            $businessEmployee = BusinessEmployee::where('business_profile_id', $this->_business_profile_id)->where('employee_id', $existEmployee['id'])->first();

            if(empty($businessEmployee)) {
                $employee = Employee::create($input);

                $businessEmployee = BusinessEmployee::create([
                    'business_profile_id' => $this->_business_profile_id,
                    'employee_id' => $employee->id
                ]);

                return response()->success('Employee has been created successfully', compact('employee'));
            } else {
                return response()->error('Employee already exists');
            }
        } catch (\Exception $e) {
            \Log::error("Employee creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $employee = Employee::with('businessProfile')->find($id);
            return response()->success('', compact('employee'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $employee = Employee::find($id);
            if (!empty($employee)) {
                if($input['is_terminated'] == 0) {
                    $input['last_working_day'] = null;
                    $input['description'] = null;
                }
                $employee->update($input);
                return response()->success('Your Record has been updated successfully !', compact('employee'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $employee = Employee::find($id)->update(['deleted_at' => \Carbon\Carbon::now(), 'status' => 'DELETED']);
                return response()->success('Employee deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
