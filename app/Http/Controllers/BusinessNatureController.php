<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessNature;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessNatureController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessNatures = BusinessNature::all();

            return response()->success('', compact('businessNatures'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessNature::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $businessNature = BusinessNature::create($input);
                return response()->success('Business Nature has been created successfully', compact('businessNature'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Nature creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessNature = BusinessNature::find($id);
            return response()->success('', compact('businessNature'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $businessNature = BusinessNature::find($id);
            if (!empty($businessNature)) {
                $validation = Validator::make($input, BusinessNature::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $businessNature->update($input);
                    return response()->success('Your Record has been updated successfully !', compact('businessNature'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessNature = BusinessNature::find($id)->delete();

                return response()->success('Business Nature deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
