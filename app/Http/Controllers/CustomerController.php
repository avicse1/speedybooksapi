<?php

namespace App\Http\Controllers;

use App\BusinessCustomer;
use Illuminate\Http\Request;
use App\Customer;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class CustomerController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $customers = Customer::where('business_profile_id', $this->_business_profile_id)->where('status', '!=', 'DELETED')->whereNull('deleted_at')->get();

            return response()->success('', compact('customers'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, Customer::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $existCustomer = Customer::where('email', $input['email'])->first();
                $businessCustomer = BusinessCustomer::where('business_profile_id', $this->_business_profile_id)->where('customer_id', $existCustomer['id'])->first();

                if(empty($businessCustomer)) {
                    $customer = Customer::create($input);
                    $businessCustomer = BusinessCustomer::create([
                        'business_profile_id' => $this->_business_profile_id,
                        'customer_id' => $customer->id
                    ]);

                    return response()->success('Customer has been created successfully!', compact('customer'));
                } else {
                    return response()->error('Customer already exists');
                }
            }
        } catch (\Exception $e) {
            \Log::error("Customer creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $customer = Customer::find($id);
            return response()->success('', compact('customer'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $customer = Customer::find($id);
            if (!empty($customer)) {
                $validation = Validator::make($input, Customer::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $customer->update($input);
                    return response()->success('Your Record has been updated successfully!', compact('customer'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $customer = Customer::find($id)->update(['deleted_at' => \Carbon\Carbon::now(), 'status' => 'DELETED']);
                return response()->success('Customer deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
