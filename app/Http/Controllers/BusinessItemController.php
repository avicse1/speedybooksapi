<?php

namespace App\Http\Controllers;

use App\BusinessInvoice;
use App\BusinessItem;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessItems = BusinessItem::with('businessProfile', 'account', 'vendor')->where('business_profile_id', $this->_business_profile_id)->orderBy('name', 'asc')->get();

            return response()->success('', compact('businessItems'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessItem::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $businessItem = BusinessItem::create($input);

                return response()->success('Business item has been created successfully!', compact('businessItem'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Item creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessItem = BusinessItem::with('businessProfile', 'account', 'vendor')->where('business_profile_id', $this->_business_profile_id)->find($id);
            return response()->success('', compact('businessItem'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $businessItem = BusinessItem::find($id);
            if (!empty($businessItem)) {
                $validation = Validator::make($input, BusinessItem::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $businessItem->update($input);
                    return response()->success('Your item has been updated successfully!', compact('account'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessItem = BusinessItem::find($id)->delete();

                return response()->success('Business Item has been deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
