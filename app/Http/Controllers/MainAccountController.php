<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Account;
use \App\MainAccount;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class MainAccountController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $mainAccounts = MainAccount::with('subAccount')->get();
            return response()->success('', compact('mainAccounts'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, MainAccount::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $mainAccount = MainAccount::create($input);
                return response()->success('Main Account has been created successfully', compact('mainAccount'));
            }
        } catch (\Exception $e) {
            \Log::error("Main Account Type creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $mainAccount = MainAccount::with('subAccount')->find($id);
            return response()->success('', compact('mainAccount'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $mainAccount = MainAccount::find($id);
            if (!empty($mainAccount)) {
                $validation = Validator::make($input, MainAccount::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $mainAccount->update($input);
                    return response()->success('Your Record has been updated successfully !', compact('mainAccount'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $mainAccount = MainAccount::find($id)->delete();

                return response()->success('Main Account Type deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
