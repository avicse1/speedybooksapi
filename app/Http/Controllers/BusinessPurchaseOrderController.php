<?php

namespace App\Http\Controllers;

use App\BusinessProfile;
use App\BusinessPurchaseOrder;
use App\BusinessPurchaseOrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Note;
use App\Taggable;

class BusinessPurchaseOrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessPurchaseOrder = BusinessPurchaseOrder::with('businessPurchaseOrderItem','businessProfile', 'vendor', 'notes', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->orderBy('created_at', 'desc')->whereNull('deleted_at')->get();

            return response()->success('', compact('businessPurchaseOrder'));
        } catch (\Exception $e) {
            \Log::error('Business Purchase Order Details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessPurchaseOrder::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;
                $businessPurchaseOrder = BusinessPurchaseOrder::create($input);

                if (!empty($input['Items'])) {
                    $items = $input['Items'];

                    foreach ($items as $item) {
                        $item['business_purchase_order_id'] = $businessPurchaseOrder->id;
                        BusinessPurchaseOrderItem::create($item);
                    }
                }

                if (!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if (empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessPurchaseOrder->id, 'taggable_type' => 'App\BusinessPurchaseOrder']);
                    }
                }

                $businessPurchaseOrder->businessPurchaseOrderItem;
                $businessPurchaseOrder->businessProfile;
                $businessPurchaseOrder->vendor;

                $appUrl = env('APP_UI_URL');

                if (!empty($input['status']) && $input['status'] == 'SEND EMAIL') {
                    $sendEmailTo = $input['send_email_to'];
                    \Mail::send('emails.businessPurchaseOrder', ['businessPurchaseOrder' => $businessPurchaseOrder, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo) {
                        $message->to($sendEmailTo)->subject('Speedy Books: Business Purchase Order Generated');
                    });
                }

                return response()->success('Business Purchase order has been '. strtolower($input['status']) .' successfully', compact('businessPurchaseOrder'));
            }
        } catch (\Exception $e) {
            \Log::error("Business purchase order creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessPurchaseOrder = BusinessPurchaseOrder::with('businessPurchaseOrderItem', 'notes', 'vendor', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessPurchaseOrder->businessPurchaseOrderItem;
            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessItem) ?  $item->businessItem->name : '';
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
            }
            return response()->success('', compact('businessPurchaseOrder'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request) {

        $input = $request->all();

        if (!empty($id)) {
            $businessPurchaseOrder = BusinessPurchaseOrder::find($id);
            if (!empty($businessPurchaseOrder)) {
                $businessPurchaseOrder->update($input);

                if(!empty($input['Items'])) {
                    BusinessPurchaseOrderItem::where('business_purchase_order_id', $businessPurchaseOrder->id)->delete();
                    $items = $input['Items'];
                    foreach ($items as $item) {
                        if(!empty($item['business_item_id'])) {
                            $item['business_purchase_order_id'] = $businessPurchaseOrder->id;
                            BusinessPurchaseOrderItem::create($item);
                        }
                    }
                }

                if(!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    Taggable::where('taggable_id', $businessPurchaseOrder->id)->where('taggable_type', 'App\BusinessPurchaseOrder')->delete();
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if(empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessPurchaseOrder->id, 'taggable_type' => 'App\BusinessPurchaseOrder']);
                    }
                }

                return response()->success('Business purchase order has been updated successfully', compact('businessPurchaseOrder'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessPurchaseOrderItem = BusinessPurchaseOrderItem::where('business_purchase_order_id', '=', $id)->delete();
                $businessPurchaseOrder = BusinessPurchaseOrder::find($id)->delete();

                return response()->success('Business purchase order deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     * Generate unique invoice no.
     *
     * @return Response
     */
    public function generatePurchaseOrderNo()
    {
        $businessProfileId = $this->_business_profile_id;

        $businessProfile = BusinessProfile::find($businessProfileId);

        $generateNumber = 'PO' . strtoupper(substr($businessProfile->name, 0, 3));

        $generateNumber .= rand(0, 9999999999);

        self::checkPurchaseOrderNumberExists($generateNumber);

        return $generateNumber;
    }

    public function checkPurchaseOrderNumberExists($number)
    {
        $businessPurchaseOrder = BusinessPurchaseOrder::where('purchase_order_no', $number)->first();

        if (!empty($businessPurchaseOrder)) {
            self::generateInvoiceNo();
        } else {
            return $number;
        }
    }

    public function sendPurchaseOrder($id, Request $request)
    {
        $input = $request->all();
        try {
            $businessPurchaseOrder = BusinessPurchaseOrder::find($id);

            $sendEmailTo = $input['send_email_to'];

            \Mail::send('emails.businessPurchaseOrder', ['businessPurchaseOrder' => $businessPurchaseOrder], function ($message) use ($sendEmailTo) {
                $message->to($sendEmailTo)->subject('Speedy Books: Business Purchase Order Generated');
            });


            return response()->success('Business purchase order has been resend successfully', compact('businessInvoice'));
        } catch (\Exception $e) {

        }
    }
}