<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessPayItem;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessPayItemController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessPayItems = BusinessPayItem::with('businessPayItemType', 'account')->get();

            foreach ($businessPayItems as $businessPayItem) {
                $businessPayItem['pay_item_type_name'] = !empty($businessPayItem->businessPayItemType) ? $businessPayItem->businessPayItemType->name : '';
            }

            return response()->success('', compact('businessPayItems'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $validation = Validator::make($input, BusinessPayItem::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $businessPayItem = BusinessPayItem::create($input);

                return response()->success('Business pay item has been created successfully!', compact('businessPayItem'));
            }
        } catch (\Exception $e) {
            \Log::error("Business pay item creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessPayItem = BusinessPayItem::with('businessPayItemType', 'account')->find($id);
            return response()->success('', compact('businessPayItem'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $businessPayItem = BusinessPayItem::find($id);
            if (!empty($businessPayItem)) {
                $businessPayItem->update($input);
                return response()->success('Your Record has been updated successfully!', compact('businessPayItem'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessPayItem = BusinessPayItem::find($id)->delete();
                return response()->success('Business pay item deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
