<?php

namespace App\Http\Controllers;

use App\Account;
use App\AccountBalance;
use App\BusinessExpense;
use App\BusinessItem;
use App\BusinessProfile;
use App\Libraries\CSV;
use App\Libraries\PDF;
use App\TaxRate;
use Illuminate\Http\Request;
use \App\BusinessInvoice;
use App\Http\Requests;
use \App\Libraries\Report;

class ReportController extends BaseController
{
    public function invoiceReport(Request $request)
    {
        $input = $request->all();
        try {

            $invoiceReport = Report::invoiceReport($input);
            $invoices = $invoiceReport['invoices'];
            $total = $invoiceReport['total'];
            $date = $invoiceReport['date'];

            $pdfPath = PDF::invoiceReport($input);
            $csvPath = CSV::invoiceReport($input);

            return response()->success('', compact('invoices', 'total', 'date', 'pdfPath', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Customer Invoice report " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    public function salesByItemsReport(Request $request)
    {
        $input = $request->all();
        try {
            $salesItemReport = Report::salesByItemsReport($input);

            $items = $salesItemReport['items'];
            $totalAmount = $salesItemReport['totalAmount'];
            $totalQuantity = $salesItemReport['totalQuantity'];
            $date = $salesItemReport['date'];

            $pdfPath = PDF::salesByItemsReport($input);
            $csvPath = CSV::salesByItemsReport($input);

            return response()->success('', compact('items', 'totalAmount', 'totalQuantity', 'pdfPath', 'csvPath', 'date'));
        } catch (\Exception $e) {
            \Log::error("Sales by items " . $e->getTraceAsString());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    public function profitandLossReport(Request $request)
    {
        $input = $request->all();
        try {
            $profitandLossReport = Report::profitandLossReport($input);

            $profitandLoss = $profitandLossReport['profitandLoss'];
            $date = $profitandLossReport['date'];
            $totalIncome = $profitandLossReport['totalIncome'];
            $totalExpense = $profitandLossReport['totalExpense'];
            $invoiceList = $profitandLossReport['invoiceList'];
            $expenseList = $profitandLossReport['expenseList'];

            $pdfPath = PDF::profitandLossReport($input);
            $csvPath = CSV::profitandLossReport($input);

            return response()->success('', compact('profitandLoss', 'date', 'totalIncome', 'totalExpense', 'invoiceList', 'expenseList', 'pdfPath', 'csvPath'));
        } catch (\Exception $e) {
            \Log::error("Profit and loss reports " . $e->getTraceAsString());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    public function balanceSheetReport(Request $request)
    {
        $input = $request->all();
        try {
            $balanceSheetReport = Report::balanceSheetReport($input);

            $balanceSheetAssets = $balanceSheetReport['balanceSheetAssets'];
            $balanceSheetLiabilities = $balanceSheetReport['balanceSheetLiabilities'];
            $balanceSheetOwners = $balanceSheetReport['balanceSheetOwners'];
            $date = $balanceSheetReport['date'];
            $totalAssets = $balanceSheetReport['totalAssets'];
            $totalLiabilities = $balanceSheetReport['totalLiabilities'];
            $totalOwners = $balanceSheetReport['totalOwners'];

            $pdfPath = PDF::balanceSheetReport($input);
            $csvPath = CSV::balanceSheetReport($input);

            return response()->success('', compact('balanceSheetAssets', 'balanceSheetLiabilities', 'balanceSheetOwners', 'date', 'totalAssets', 'totalLiabilities', 'totalOwners', 'pdfPath', 'csvPath'));
        } catch (\Exception $e) {
            \Log::error("Balance Sheet reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function balanceSheetDetailsReport($accountType, Request $request)
    {
        $input = $request->all();
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $businessProfileId = $input['business_profile_id'];
        $cashBankEquiAccount = ['Cash Account', 'Bank Account', 'Cash & Cash Equivalents'];
        $creditTotal = 0;
        $debitTotal = 0;
        $assestAccount = ['Office Equipment', 'Vehicle', 'Investment'];

        try {
            // get all invoices
            $invoices = BusinessInvoice::with('businessInvoiceItem', 'account', 'customer')
                ->where('business_profile_id', $businessProfileId)
                ->where('type', 'INVOICE')
                ->where('status', '!=', 'DRAFT')
                ->whereNull('deleted_at');

            if(!empty($from) && !empty($to)) {
                $invoices = $invoices->whereBetween('created_at', [$from, $to]);
            }
            if(!empty($basis) && $basis == 'cash') {
                $invoices = $invoices->where('payment_term', 'Paid');
            }

            // get all Expenses

            $expenses = BusinessExpense::with('businessExpenseItem', 'account', 'vendor')
                ->where('business_profile_id', $businessProfileId)
                ->where('type', 'EXPENSE')
                ->where('status', '!=', 'DRAFT')
                ->whereNull('deleted_at');

            if(!empty($from) && !empty($to)) {
                $expenses = $expenses->whereBetween('created_at', [$from, $to]);
            }

            if(!empty($basis) && $basis == 'cash') {
                $expenses = $expenses->where('payment_term', 'Paid');
            }

            if(in_array($accountType, $cashBankEquiAccount)) {
                $account = Account::where('name', $accountType)->first();
                if(!empty($account)) {
                    $invoices = $invoices->where('account_id', $account->id);
                    $expenses = $expenses->where('account_id', $account->id);
                }
                $invoices = $invoices->get();
                $expenses = $expenses->get();

                foreach ($invoices as $invoice) {
                    $items = $invoice->businessInvoiceItem;
                    foreach ($items as $item) {
                        $creditTotal += $item['amount'];
                        $invoice['credit'] += $item['amount'];
                    }
                }

                foreach ($expenses as $expense) {
                    $items = $expense->businessExpenseItem;
                    foreach ($items as $item) {
                        $debitTotal += $item['amount'];
                        $expense['debit'] += $item['amount'];
                    }
                }
                $balanceSheetDetails = array_merge($invoices->toArray(), $expenses->toArray());
            }

            if($accountType == 'Account Receivables ( Debtors)') {
                $invoices = $invoices->get();

                foreach ($invoices as $invoice) {
                    $items = $invoice->businessInvoiceItem;
                    if($invoice->payment_term == 'Paid') {
                        foreach ($items as $item) {
                            $debitTotal += $item['amount'];
                            $invoice['debit'] += $item['amount'];
                        }
                    }

                    if($invoice->payment_term != 'Paid') {
                        foreach ($items as $item) {
                            $creditTotal += $item['amount'];
                            $invoice['credit'] += $item['amount'];
                        }
                    }
                }

                $balanceSheetDetails = $invoices->toArray();
            }

            if($accountType == 'Drawings') {
                $expenses = $expenses->get();
                $expenseDrawings = array();

                foreach ($expenses as $expense) {
                    $items = $expense->businessExpenseItem;
                    foreach ($items as $item) {
                        if(!empty($item['account_id'])) {
                            $account = Account::find($item['account_id']);
                            if($account->name == 'Drawings') {
                                $debitTotal += $item['amount'];
                                $expense['debit'] += $item['amount'];

                                array_push($expenseDrawings, $expense);
                            }
                        }
                    }
                }

                $balanceSheetDetails = $expenseDrawings;
            }

            if($accountType == 'Account payable ( creditors)') {
                $expenses = $expenses->get();
                $expensePayable = array();

                foreach ($expenses as $expense) {
                    if($expense->payment_term != 'Paid') {
                        $items = $expense->businessExpenseItem;
                        foreach ($items as $item) {
                            $account = Account::find($item->account_id);
                            if($account->name != 'Salary and wages') {
                                $creditTotal += $item['amount'];
                                $expense['credit'] += $item['amount'];
                                array_push($expensePayable, $expense);
                            }
                        }
                        $balanceSheetDetails = $expensePayable;
                    }
                }
            }

            if(in_array($accountType, $assestAccount)) {
                $expenses = $expenses->get();
                $assestExpense = array();

                foreach ($expenses as $expense) {
                    if($expense->payment_term == 'Paid') {
                        $items = $expense->businessExpenseItem;
                        foreach ($items as $item) {
                            if(!empty($item->account_id)) {
                                $account = Account::find($item['account_id']);
                                if($account->name == $accountType) {
                                    $debitTotal += $item['amount'];
                                    $expense['debit'] += $item['amount'];
                                    array_push($assestExpense, $expense);
                                }
                            }
                        }
                        $balanceSheetDetails = $assestExpense;
                    }
                }
            }

            $date = ['from' => \Carbon\Carbon::parse($from)->toFormattedDateString(), 'to' => \Carbon\Carbon::parse($to)->toFormattedDateString()];

            return response()->success('', compact('balanceSheetDetails', 'creditTotal', 'debitTotal', 'date'));

        } catch (\Exception $e) {
            \Log::error("Balance Sheet reports details" . $e->getMessage());
            return response()->error($e->getMessage());
        }
    }

    public function journalReport(Request $request)
    {
        $input = $request->all();
        try {
            $journalReportDetails = Report::journalReport($input);
            $journalReport = $journalReportDetails['journalReport'];
            $date = $journalReportDetails['date'];
            $totalDebit = $journalReportDetails['totalDebit'];
            $totalCredit = $journalReportDetails['totalCredit'];

            $pdfPath = PDF::journalReport($input);
            $csvPath = CSV::journalReport($input);

            return response()->success('', compact('journalReport', 'date', 'pdfPath', 'totalDebit', 'totalCredit', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Journal reports details" . $e->getMessage());
            return response()->error($e->getMessage());
        }
    }

    public function agedReceviablesReport(Request $request)
    {
        $input = $request->all();
        $input['business_profile_id'] = $this->_business_profile_id;
        try {
            $agedReceviablesReport = Report::agedReceviablesReport($input);
            $agedReceviables = $agedReceviablesReport['agedReceviables'];
            $totalReceviables = $agedReceviablesReport['totalReceviables'];
            $currentMonth = $agedReceviablesReport['currentMonth'];

            $pdfPath = PDF::agedReceviablesReport($input);
            $csvPath = CSV::agedReceviablesReport($input);

            return response()->success('', compact('agedReceviables', 'totalReceviables', 'currentMonth', 'pdfPath', 'csvPath'));

        } catch(\Exception $e) {
            \Log::error("Aged Receviables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function agedPayablesReport(Request $request)
    {
        $input = $request->all();
        $input['business_profile_id'] = $this->_business_profile_id;
        try {
            $agedPayablesReport = Report::agedPayablesReport($input);

            $agedPayables = $agedPayablesReport['agedPayables'];
            $totalPayables = $agedPayablesReport['totalPayables'];
            $currentMonth = $agedPayablesReport['currentMonth'];

            $pdfPath = PDF::agedPayablesReport($input);
            $csvPath = CSV::agedPayablesReport($input);

            return response()->success('', compact('agedPayables', 'totalPayables', 'currentMonth', 'pdfPath', 'csvPath'));

        } catch(\Exception $e) {
            \Log::error("Aged Payables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function trailBalanceReport(Request $request)
    {
        $input = $request->all();

        try {
            $trailBalanceReport = Report::trailBalanceReport($input);
            $trailBalanceData = $trailBalanceReport['trailBalanceData'];
            $totalDebit = $trailBalanceReport['totalDebit'];
            $totalCredit = $trailBalanceReport['totalCredit'];
            $date = $trailBalanceReport['date'];

            $pdfPath = PDF::trailBalanceReport($input);
            $csvPath = CSV::trailBalanceReport($input);
            return response()->success('', compact('trailBalanceData', 'totalDebit', 'totalCredit', 'date', 'pdfPath', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Trail Balance reports " . $e->getTraceAsString());
            return response()->error($e->getTraceAsString());
        }
    }

    public  function accountingTransactionReport(Request $request)
    {
        $input = $request->all();

        try {
            $accountingTransactionReport = Report::accountingTransactionReport($input);

            $accountingTransactions = $accountingTransactionReport['accountingTransactions'];
            $date = $accountingTransactionReport['date'];
            $totalAmount = $accountingTransactionReport['totalAmount'];
            $totalGrossAmount = $accountingTransactionReport['totalGrossAmount'];

            $pdfPath = PDF::accountingTransactionReport($input);
            $csvPath = CSV::accountingTransactionReport($input);
            return response()->success('', compact('accountingTransactions', 'date', 'pdfPath', 'totalAmount', 'totalGrossAmount', 'csvPath'));
        } catch (\Exception $e) {
            \Log::error("Accounting Transaction reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function invoiceReceivablesReport(Request $request)
    {
        $input = $request->all();

        try {
            $invoiceReceivablesReport = Report::invoiceReceivablesReport($input);
            $invoiceReceivables = $invoiceReceivablesReport['invoiceReceivables'];
            $date = $invoiceReceivablesReport['date'];
            $totalAmount = $invoiceReceivablesReport['totalAmount'];

            $pdfPath = PDF::invoiceReceivablesReport($input);
            $csvPath = CSV::invoiceReceivablesReport($input);

            return response()->success('', compact('invoiceReceivables', 'date', 'pdfPath', 'totalAmount', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Invoice Receivables reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function taxReport(Request $request)
    {
        $input = $request->all();

        try {
            $taxReportDetails = Report::taxReport($input);
            $taxReport = $taxReportDetails['taxReport'];
            $date = $taxReportDetails['date'];

            $pdfPath = PDF::taxReport($input);
            $csvPath = CSV::taxReport($input);
            return response()->success('', compact('taxReport', 'date', 'pdfPath', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Tax reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function cashSummaryReport(Request $request)
    {
        $input = $request->all();

        try {
            $cashSummaryReport = Report::cashSummaryReport($input);
            $incomeCashSummary = $cashSummaryReport['incomeCashSummary'];
            $expenseCashSummary = $cashSummaryReport['expenseCashSummary'];
            $totalIncome = $cashSummaryReport['totalIncome'];
            $totalExpense = $cashSummaryReport['totalExpense'];
            $invoiceList = $cashSummaryReport['invoiceList'];
            $expenseList = $cashSummaryReport['expenseList'];
            $date = $cashSummaryReport['date'];

            $pdfPath = PDF::cashSummaryReport($input);
            $csvPath = CSV::cashSummaryReport($input);

            return response()->success('', compact('incomeCashSummary', 'expenseCashSummary', 'totalIncome', 'totalExpense', 'invoiceList', 'expenseList', 'date', 'pdfPath', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Cash Summary reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function cashAccountReport(Request $request)
    {
        $input = $request->all();

        try {
            $cashAccountReport = Report::cashAccountReport($input);
            $cashAccount = $cashAccountReport['cashAccount'];
            $totalDebit = $cashAccountReport['totalDebit'];
            $totalCredit = $cashAccountReport['totalCredit'];
            $date = $cashAccountReport['date'];

            $pdfPath = PDF::cashAccountReport($input);
            $csvPath = CSV::cashAccountReport($input);

            return response()->success('', compact('cashAccount', 'totalDebit', 'totalCredit', 'date', 'pdfPath', 'csvPath'));

        } catch (\Exception $e) {
            \Log::error("Cash Account reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function generalLedgerReport(Request $request)
    {
        $input = $request->all();
        $input['business_profile_id'] = $this->_business_profile_id;
        $from = $input['from'];
        $to = $input['to'];
        $type = $input['type'];
        $basis = $input['basis'];
        // $businessProfileId = $input['business_profile_id'];

        try {
            $generalLedgerReport = Report::generalLedgerReport($input);

            $generalLedgerData = $generalLedgerReport['generalLedgerData'];
            $totalDebit = $generalLedgerReport['totalDebit'];
            $totalCredit = $generalLedgerReport['totalCredit'];
            $generalLedgerDetails = $generalLedgerReport['generalLedgerDetails'];
            $date = $generalLedgerReport['date'];

            $pdfPath = PDF::generalLedgerReport($input);
            $csvPath = CSV::generalLedgerReport($input);

            return response()->success('', compact('generalLedgerData', 'totalDebit', 'totalCredit', 'date', 'pdfPath', 'csvPath', 'generalLedgerDetails'));
        } catch (\Exception $e) {
            \Log::error("General Ledger reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }

    public function cashFlowReport(Request $request)
    {
        $input = $request->all();
        try {
            $cashFlowReport = Report::cashFlowReport($input);

            return response()->success('', compact('cashFlowReport'));
        } catch (\Exception $e) {
            \Log::error("Cash flow statement reports " . $e->getMessage());
            return response()->error($e->getTraceAsString());
        }
    }
}
