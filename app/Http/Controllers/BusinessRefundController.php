<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\BusinessRefund;
use \App\BusinessRefundCustomer;
use \App\BusinessRefundItem;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use \App\Note;
use \App\Taggable;

class BusinessRefundController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessRefunds = BusinessRefund::with('businessRefundItem', 'customer', 'notes')->where('business_profile_id', $this->_business_profile_id)->whereNull('deleted_at')->get();

            return response()->success('', compact('businessRefunds'));
        } catch (\Exception $e) {
            \Log::error('Business refunds Details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $input['created_by'] = $this->_user_id;
            $businessRefund = BusinessRefund::create($input);

            if(!empty($input['customer_id'])) {
                $businessRefundCustomer = new BusinessRefundCustomer;
                $businessRefundCustomer->business_refund_id = $businessRefund->id;
                $businessRefundCustomer->customer_id = $input['customer_id'];
                $businessRefundCustomer->save();
            }

            if(!empty($input['Items'])) {
                $items = $input['Items'];

                foreach ($items as $item) {
                    $item['business_refund_id'] = $businessRefund->id;
                    BusinessRefundItem::create($item);
                }
            }

            if(!empty($input['Notes'])) {
                $notes = $input['Notes'];
                foreach ($notes as $note) {
                    $noteObject = Note::where('title', $note)->first();
                    if(empty($noteObject)) {
                        $noteObject = new Note;
                        $noteObject->title = $note;
                        $noteObject->save();
                    }
                    $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessRefund->id, 'taggable_type' => 'App\BusinessRefund']);
                }
            }

            $businessRefund->businessRefundItem;
            $businessRefund->businessProfile;
            $businessRefund->customer;

            $appUrl = env('APP_UI_URL');
            if(!empty($input['status']) && $input['status'] == 'SEND EMAIL') {
                $sendEmailTo = $input['send_email_to'];
                \Mail::send('emails.businessRefund', ['businessRefund' => $businessRefund, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo) {
                    $message->to($sendEmailTo)->subject('Speedy Books: Business Refund Generated');
                });
            }


            return response()->success('Business Refund has been created successfully', compact('businessRefund'));
        } catch (\Exception $e) {
            \Log::error("Business Refund creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessRefund = BusinessRefund::with('businessRefundItem', 'notes', 'customer')->where('business_profile_id', $this->_business_profile_id)->find($id);
            return response()->success('', compact('businessRefund'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessRefundItems = BusinessRefundItem::where('business_refund_id', '=', $id)->delete();
                $businessRefund = BusinessRefund::find($id)->delete();

                return response()->success('Business Refund deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
