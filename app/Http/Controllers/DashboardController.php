<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\BusinessExpense;
use App\Http\Requests;
use \App\Libraries\Util;
use \App\User;
use \App\BusinessInvoice;

class DashboardController extends BaseController
{
    public function userCount()
    {
        try {
            $user = User::where('business_profile_id', $this->_business_profile_id)->count();
            return response()->success('', compact('user'));
        } catch (\Exception $e) {
            \Log::error('Users Count' . $e->getTraceAsString());
            return response()->error('Something went wrong !');
        }
    }

    public function invoiceCount()
    {
        try {
            $currentDate = \Carbon\Carbon::now()->format('Y-m-d');
            $paidInvoices = BusinessInvoice::where('payment_term', 'Paid')->where('business_profile_id', $this->_business_profile_id)->whereIn('type', ['INVOICE', 'RECURRING'])->get([\DB::raw("sum(amount) as amount")]);
            $dueInvoices = BusinessInvoice::where('due_date', '<', $currentDate)->where('payment_term','!=' , 'Paid')->where('business_profile_id', $this->_business_profile_id)->whereIn('type', ['INVOICE', 'RECURRING'])->get([\DB::raw("sum(amount) as amount")]);
            $openInvoices = BusinessInvoice::where('due_date', '>', $currentDate)->where('payment_term','!=' , 'Paid')->where('business_profile_id', $this->_business_profile_id)->whereIn('type', ['INVOICE', 'RECURRING'])->get([\DB::raw("sum(amount) as amount")]);

            $invoices = array('paid' => $paidInvoices, 'due' => $dueInvoices, 'open' => $openInvoices);

            return response()->success('', compact('invoices'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found');
        }
    }

    public function invoiceUpcomingPayment()
    {
        try {
            $currentDate = \Carbon\Carbon::now()->format('Y-m-d');
            $after4Week = \Carbon\Carbon::now()->addWeeks(4)->format('Y-m-d');
            $invoices = BusinessInvoice::with('createdBy')->where('type', 'INVOICE')->where('payment_term', '!=', 'Paid')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$currentDate, $after4Week])->take(3)->get(['id', 'invoice_no', 'amount', 'payment_term','created_by', 'created_at']);

            return response()->success('', compact('invoices'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found');
        }
    }

    public function expenseReport()
    {
        $monthFirstDate = \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d');
        $monthLastDate = \Carbon\Carbon::now()->endOfMonth()->format('Y-m-d');

        $yearFirstDate = \Carbon\Carbon::now()->startOfYear()->format('Y-m-d');
        $yearLastDate = \Carbon\Carbon::now()->lastOfYear()->format('Y-m-d');

        $weekfirstDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
        $weekLastDate = \Carbon\Carbon::parse($weekfirstDate)->addDays(6)->format('Y-m-d');

        $expenseYearlyReports = BusinessExpense::where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$yearFirstDate, $yearLastDate])->groupBy(\DB::raw("MONTH(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);
        $expenseMonthlyReports = BusinessExpense::where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$monthFirstDate, $monthLastDate])->groupBy(\DB::raw("DAY(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);
        $expenseWeeklyReports = BusinessExpense::where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$weekfirstDate, $weekLastDate])->groupBy(\DB::raw("DAY(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);

        $yearlyReport = Util::getYearChart($expenseYearlyReports);
        $monthlyReport = Util::getMonthChart($monthFirstDate, $monthLastDate, $expenseMonthlyReports);
        $weeklyReport = Util::getWeekChart($weekfirstDate, $weekLastDate, $expenseWeeklyReports);

        $report = array('year' => array('key' => array_keys($yearlyReport), 'value' => array_values($yearlyReport)),
            'month' => array('key' => array_keys($monthlyReport), 'value' => array_values($monthlyReport)),
            'week' => array('key' => ['Mon', 'Tue', 'Wed', 'Thru', 'Fri', 'Sat', 'Sun'], 'value' => array_values($weeklyReport))
        );

        return response()->success('', compact('report'));
    }

    public function profitLossReport()
    {
        $monthFirstDate = \Carbon\Carbon::now()->startOfMonth()->format('Y-m-d');
        $monthLastDate = \Carbon\Carbon::now()->endOfMonth()->format('Y-m-d');

        $yearFirstDate = \Carbon\Carbon::now()->startOfYear()->format('Y-m-d');
        $yearLastDate = \Carbon\Carbon::now()->lastOfYear()->format('Y-m-d');

        $weekfirstDate = \Carbon\Carbon::now()->startOfWeek()->format('Y-m-d');
        $weekLastDate = \Carbon\Carbon::parse($weekfirstDate)->addDays(6)->format('Y-m-d');

        $expenseYearlyReports = BusinessExpense::where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$yearFirstDate, $yearLastDate])->groupBy(\DB::raw("MONTH(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);
        $expenseMonthlyReports = BusinessExpense::where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$monthFirstDate, $monthLastDate])->groupBy(\DB::raw("DAY(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);
        $expenseWeeklyReports = BusinessExpense::where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$weekfirstDate, $weekLastDate])->groupBy(\DB::raw("DAY(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);

        $ExpensemonthlyReport = Util::getMonthChart($monthFirstDate, $monthLastDate, $expenseMonthlyReports);
        $ExpenseyearlyReport = Util::getYearChart($expenseYearlyReports);
        $ExpenseweeklyReport = Util::getWeekChart($weekfirstDate, $weekLastDate, $expenseWeeklyReports);

        $invoiceYearlyReports = BusinessInvoice::where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$yearFirstDate, $yearLastDate])->groupBy(\DB::raw("MONTH(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);
        $invoiceMonthlyReports = BusinessInvoice::where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$monthFirstDate, $monthLastDate])->groupBy(\DB::raw("DAY(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);
        $invoiceWeeklyReports = BusinessInvoice::where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$weekfirstDate, $weekLastDate])->groupBy(\DB::raw("DAY(due_date)"))->get([\DB::raw("sum(amount) as amount"), 'due_date']);

        $InvoicemonthlyReport = Util::getMonthChart($monthFirstDate, $monthLastDate, $invoiceMonthlyReports);
        $InvoiceyearlyReport = Util::getYearChart($invoiceMonthlyReports);
        $InvoiceWeeklyReport = Util::getWeekChart($weekfirstDate, $weekLastDate, $invoiceWeeklyReports);


        $report = ['year' => ['key' => ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                              'value' => [
                                    ['name' => 'Expense', 'data' => array_values($ExpenseyearlyReport)],
                                    ['name' => 'Invoice', 'data' => array_values($InvoiceyearlyReport)]
                              ]],
                   'month' => ['key' => array_keys($ExpensemonthlyReport),
                        'value' => [
                            ['name' => 'Expense', 'data' => array_values($ExpensemonthlyReport)],
                            ['name' => 'Invoice', 'data' => array_values($InvoicemonthlyReport)]
                   ]],
                   'week' => ['key' => ['Mon', 'Tue', 'Wed', 'Thru', 'Fri', 'Sat', 'Sun'],
                        'value' => [
                            ['name' => 'Expense', 'data' => array_values($ExpenseweeklyReport)],
                            ['name' => 'Invoice', 'data' => array_values($InvoiceWeeklyReport)]
                   ]],

        ];

        return response()->success('', compact('report'));
    }

    public function recentActivity(Request $request)
    {
        $input = $request->all();
        $now = \Carbon\Carbon::now();
        $last24Hours = \Carbon\Carbon::now()->subHours(24);
        try {
            $invoices = BusinessInvoice::with('createdBy')->where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id);
            $estimates = BusinessInvoice::with('createdBy')->where('type', 'ESTIMATES')->where('business_profile_id', $this->_business_profile_id);
            $expenses = BusinessExpense::with('createdBy')->where('type', 'EXPENSE')->where('business_profile_id', $this->_business_profile_id);

            if(empty($input['type'])) {
                $invoices = $invoices->whereNull('deleted_at');
                $estimates = $estimates->whereNull('deleted_at');
                $expenses = $expenses->whereNull('deleted_at');
            }

            $invoices = $invoices->orderBy('updated_at', 'desc')->whereBetween('created_at', [$last24Hours, $now])->get(['id', 'invoice_no', 'amount', 'payment_term','created_by', 'created_at']);
            $estimates = $estimates->orderBy('updated_at', 'desc')->whereBetween('created_at', [$last24Hours, $now])->get(['id', 'invoice_no', 'amount', 'payment_term','created_by', 'created_at']);
            $expenses = $expenses->orderBy('updated_at', 'desc')->whereBetween('created_at', [$last24Hours, $now])->get(['id', 'amount', 'payment_term','created_by', 'created_at']);

            return response()->success('', compact('invoices', 'estimates', 'expenses'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found');
        }
    }
}
