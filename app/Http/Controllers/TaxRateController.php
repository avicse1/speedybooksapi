<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\TaxRate;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;

class TaxRateController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $taxRates = TaxRate::orderBy('id', 'desc')->get();

            return response()->success('', compact('taxRates'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, TaxRate::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $taxRate = TaxRate::create($input);

                return response()->success('Tax rate has been created successfully', compact('taxRate'));
            }
        } catch (\Exception $e) {
            \Log::error("Customer creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $taxRate = TaxRate::find($id);
            return response()->success('', compact('taxRate'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $taxRate = TaxRate::find($id);
            if (!empty($taxRate)) {
                $validation = Validator::make($input, TaxRate::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $taxRate->update($input);
                    return response()->success('Tax rate has been updated successfully!', compact('taxRate'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                TaxRate::find($id)->delete();
                return response()->success('Tax rate deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
