<?php

namespace App\Http\Controllers;

use App\Account;
use App\BusinessCheque;
use App\BusinessExpense;
use App\BusinessInvoice;
use App\BusinessJournal;
use App\BusinessJournalItem;
use App\BusinessReconcile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;

class BusinessReconcileController extends BaseController
{

    public function index(Request $request)
    {
        $input = $request->all();
        try {
            $businessReconciles = BusinessReconcile::where('account_id', $input['account_id'])->where('business_profile_id', $this->_business_profile_id)->get();

            return response()->success('', compact('businessReconciles'));
        } catch (\Exception $e) {
            \Log::error('get Reconcile details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessReconcile::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;
                $input['reconcile_on'] = \Carbon\Carbon::now()->format('Y-m-d');
                $businessReconcile = BusinessReconcile::create($input);

                $checkPaymentData = $input['checkPaymentData'];
                $depositsData= $input['depostisData'];

                if(!empty($checkPaymentData)) {
                    foreach ($checkPaymentData as $data) {
                        if($data['type'] == 'Cheque') {
                            $businessCheque = BusinessCheque::find($data['id']);
                            $businessCheque->reconcile_type = 1;
                            $businessCheque->business_reconcile_id = $businessReconcile->id;
                            $businessCheque->save();
                        }

                        if($data['type'] == 'Journal') {
                            $businessJournal = BusinessJournal::find($data['id']);
                            $items = $businessJournal->businessJournalItem;
                            foreach ($items as $item) {
                                if($item->account_id == $input['account_id'] && $item->id == $data['item_id'])
                                {
                                    $item->reconcile_type = 1;
                                    $item->business_reconcile_id = $businessReconcile->id;
                                    $item->save();
                                }
                            }
                        }
                        if($data['type'] == 'Expense') {
                            $businessExpense = BusinessExpense::find($data['id']);
                            $businessExpense->reconcile_type = 1;
                            $businessExpense->business_reconcile_id = $businessReconcile->id;
                            $businessExpense->save();
                        }
                    }
                }

                if(!empty($depositsData)) {
                    foreach ($depositsData as $data) {
                        if($data['type'] == 'Journal') {
                            $businessJournal = BusinessJournal::find($data['id']);
                            $items = $businessJournal->businessJournalItem;
                            foreach ($items as $item) {
                                if($item->account_id == $input['account_id'] && $item->id == $data['item_id']) {
                                    $item->reconcile_type = 1;
                                    $item->business_reconcile_id = $businessReconcile->id;
                                    $item->save();
                                }
                            }
                        }

                        if($data['type'] == 'Payment') {
                            $businessInvoice = BusinessInvoice::find($data['id']);
                            $businessInvoice->reconcile_type = 1;
                            $businessInvoice->business_reconcile_id = $businessReconcile->id;
                            $businessInvoice->save();
                        }
                    }
                }

                if(!empty($input['auto_adjustment']) && $input['auto_adjustment'] > 0) {
                    $journalAdjustment = ['business_profile_id' => $this->_business_profile_id,
                        'name' => 'Reconcile Auto Adjustment for Income',
                        'date' => \Carbon\Carbon::now()->format('Y-m-d'),
                        'status' => 'APPROVED',
                        'total_debit' => $input['auto_adjustment'],
                        'total_credit' => $input['auto_adjustment'],
                    ];
                    $businessJournal = BusinessJournal::create($journalAdjustment);

                    $journalItems[] = ['business_journal_id' => $businessJournal->id,
                        'account_id' => 11,
                        'description'=> 'Reconcile Auto Adjustment',
                        'debit' => 0,
                        'credit' => $input['auto_adjustment']
                    ];

                    $journalItems[] = ['business_journal_id' => $businessJournal->id,
                        'account_id' => 32,
                        'description'=> 'Reconcile Auto Adjustment',
                        'debit' => $input['auto_adjustment'],
                        'credit' => 0
                    ];
                    foreach ($journalItems as $journalItem) {
                        BusinessJournalItem::create($journalItem);
                    }
                } else {
                    $journalAdjustment = ['business_profile_id' => $this->_business_profile_id,
                        'name' => 'Reconcile Auto Adjustment for Expense',
                        'date' => \Carbon\Carbon::now()->format('Y-m-d'),
                        'status' => 'APPROVED',
                        'total_debit' => $input['auto_adjustment'],
                        'total_credit' => $input['auto_adjustment'],
                    ];
                    $businessJournal = BusinessJournal::create($journalAdjustment);

                    $journalItems[] = ['business_journal_id' => $businessJournal->id,
                        'account_id' => 11,
                        'description'=> 'Reconcile Auto Adjustment',
                        'debit' => $input['auto_adjustment'],
                        'credit' => 0
                    ];

                    $journalItems[] = ['business_journal_id' => $businessJournal->id,
                        'account_id' => 32,
                        'description'=> 'Reconcile Auto Adjustment',
                        'debit' => 0,
                        'credit' => $input['auto_adjustment']
                    ];
                    foreach ($journalItems as $journalItem) {
                        BusinessJournalItem::create($journalItem);
                    }
                }
                return response()->success('Business reconcile has been created successfully', compact('businessReconcile'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Reconcile creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    public function show($id)
    {
        try {
            $businessReconcile = BusinessReconcile::with('businessProfile', 'account', 'businessExpense', 'businessInvoice', 'businessCheque', 'businessJournalItem')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $businessExpenses = !empty($businessReconcile->businessExpense) ? $businessReconcile->businessExpense : array();
            $businessInvoices = !empty($businessReconcile->businessInvoice) ? $businessReconcile->businessInvoice : array();
            $businessCheques = !empty($businessReconcile->businessCheque) ? $businessReconcile->businessCheque : array();
            $businessJournalItems = !empty($businessReconcile->businessJournalItem) ? $businessReconcile->businessJournalItem : array();


            $chequePaymentsdata = array();
            $depositePaymentData = array();

            if(!empty($businessExpenses)) {
                foreach ($businessExpenses as $expense) {
                    $expenseItems = $expense->businessExpenseItem;
                    if(!empty($expenseItems)) {
                        foreach ($expenseItems as $item) {
                            $chequePaymentsdata[] = [
                                'id' => $expense->id,
                                'date' => $expense->date,
                                'type' => 'Payment',
                                'no' => $expense->expense_no,
                                'name' => $expense->vendor[0]->first_name . ' ' . $expense->vendor[0]->last_name,
                                'amount' => $item->amount,
                            ];
                        }
                    }
                }
            }

            if(!empty($businessInvoices)) {
                foreach ($businessInvoices as $invoice) {
                    $invoiceItems = $invoice->businessInvoiceItem;
                    if(!empty($invoiceItems)) {
                        foreach ($invoiceItems as $item) {
                            $depositePaymentData[] = [
                                'id' => $invoice->id,
                                'date' => $invoice->date,
                                'type' => 'Income',
                                'no' => $invoice->invoice_no,
                                'name' => $invoice->customer[0]->first_name . ' ' . $invoice->customer[0]->last_name,
                                'amount' => $item->amount,
                            ];
                        }
                    }
                }
            }

            if(!empty($businessCheques)) {
                foreach ($businessCheques as $cheque) {
                    $chequePaymentsdata[] = [
                        'id' => $cheque->id,
                        'date' => $cheque->date,
                        'type' => 'Cheque',
                        'no' => '',
                        'name' => $cheque->vendor->first_name .' '. $cheque->vendor->last_name,
                        'amount' => $cheque->amount,
                    ];
                }
            }

            if(!empty($businessJournalItems)) {
                foreach ($businessJournalItems as $journalItem) {
                    $journal = $journalItem->businessJournal;

                    if (!empty($journalItem->credit) && $journalItem->credit > 0) {
                        $chequePaymentsdata[] = [
                            'id' => $journal->id,
                            'date' => $journal->date,
                            'type' => 'Journal',
                            'no' => '',
                            'name' => '',
                            'item_id' => $journalItem->id,
                            'amount' => $journalItem->credit
                        ];
                    } else {
                        $depositePaymentData[] = ['id' => $journal->id,
                            'date' => $journal->date,
                            'type' => 'Journal',
                            'no' => '',
                            'name' => '',
                            'item_id' => $journalItem->id,
                            'amount' => $journalItem->debit
                        ];
                    }
                }
            }
            
            $businessReconcile['cheque_payment_data'] = $chequePaymentsdata;
            $businessReconcile['deposite_payment_data'] = $depositePaymentData;

            return response()->success('', compact('businessReconcile'));
        } catch (\Exception $e) {
            \Log::error('get Reconcile details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    public function reconcileAccount($id, Request $request)
    {
        $input = $request->all();
        try {
            $reconcilePayments = array();
            $reconcileDeposits = array();

            $account = Account::find($id);

            $expenses = BusinessExpense::with('businessExpenseItem', 'vendor', 'businessProfile')
                        ->where('business_profile_id', $this->_business_profile_id)
                        ->where('reconcile_type', 0)
                        ->whereNull('deleted_at')->get();

            foreach ($expenses as $expense) {
                $items = $expense->businessExpenseItem;
                foreach ($items as $item) {
                    if($expense->payment_term == 'Paid') {
                        if($expense->account_id == $id) {
                            $reconcilePayments[] = ['id' => $expense->id,
                                'date' => $expense->date,
                                'type' => 'Expense',
                                'name' => !empty($expense->vendor) ? $expense->vendor[0]->first_name .' '. $expense->vendor[0]->last_name : '',
                                'no' => $expense->expense_no,
                                'item_id' => $item->id,
                                'amount' => $item->amount
                            ];
                        }
                    }
                }
            }

            $journals = BusinessJournal::with('businessJournalItem', 'businessProfile')
                        ->where('business_profile_id', $this->_business_profile_id)
                        ->where('status', 'APPROVED')
                        ->whereNull('deleted_at')->get();

            foreach ($journals as $journal) {
                $items = $journal->businessJournalItem;
                foreach ($items as $item) {
                    if($item->account_id == $id && $item->reconcile_type == 0) {
                        if (!empty($item->credit) && $item->credit > 0) {
                            $reconcilePayments[] = ['id' => $journal->id,
                                'date' => $journal->date,
                                'type' => 'Journal',
                                'no' => '',
                                'name' => '',
                                'item_id' => $item->id,
                                'amount' => $item->credit
                            ];
                        } else {
                            $reconcileDeposits[] = ['id' => $journal->id,
                                'date' => $journal->date,
                                'type' => 'Journal',
                                'no' => '',
                                'name' => '',
                                'item_id' => $item->id,
                                'amount' => $item->debit
                            ];
                        }
                    }
                }
            }

            $cheques = BusinessCheque::with('businessProfile', 'account')
                ->where('business_profile_id', $this->_business_profile_id)
                ->where('reconcile_type', 0)
                ->whereNull('deleted_at')->get();

            foreach ($cheques as $cheque) {
                if($cheque->account_id == $id) {
                    $reconcilePayments[] = ['id' => $cheque->id,
                        'date' => $cheque->date,
                        'type' => 'Cheque',
                        'name' => !empty($cheque->vendor) ? $cheque->vendor->first_name .' '. $cheque->vendor->last_name : '',
                        'no' => $cheque->cheque_no,
                        'item_id' => '',
                        'amount' => $cheque->amount
                    ];
                }
            }

            $invoices = BusinessInvoice::with('businessInvoiceItem', 'customer', 'businessProfile')
                ->where('business_profile_id', $this->_business_profile_id)
                ->where('reconcile_type', 0)
                ->whereNull('deleted_at')->get();

            foreach ($invoices as $invoice) {
                $items = $invoice->businessInvoiceItem;
                foreach ($items as $item) {
                    if($invoice->payment_term == 'Paid') {
                        if($invoice->account_id == $id) {
                            $reconcileDeposits[] = ['id' => $invoice->id,
                                'date' => $invoice->date,
                                'type' => 'Payment',
                                'name' => !empty($invoice->customer) ? $invoice->customer[0]->first_name .' '. $invoice->customer[0]->last_name : '',
                                'no' => $invoice->invoice_no,
                                'item_id' => $item->id,
                                'amount' => $item->amount
                            ];
                        }
                    }
                }
            }

            $date = ['ending_date' => \Carbon\Carbon::parse($input['ending_date'])->toFormattedDateString()];

            return response()->success('', compact('reconcilePayments', 'reconcileDeposits', 'date', 'account'));

        } catch (\Exception $e) {
            \Log::error('Reconcile account details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    public function reconcileLatest($id)
    {
        try {
            $businessReconcile = BusinessReconcile::where('account_id', $id)->where('business_profile_id', $this->_business_profile_id)->orderBy('created_at', 'desc')->first();

            return response()->success('', compact('businessReconcile'));
        } catch (\Exception $e) {
            \Log::error('get Reconcile details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }
}
