<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessPayItemType;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessPayItemTypeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessPayItemTypes = BusinessPayItemType::with('businessPayItem')->get();

            return response()->success('', compact('businessPayItemTypes'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $validation = Validator::make($input, BusinessPayItemType::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $businessPayItemType = BusinessPayItemType::create($input);

                return response()->success('Business pay item type has been created successfully!', compact('businessPayItemType'));
            }
        } catch (\Exception $e) {
            \Log::error("Business pay item type creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessPayItemType = BusinessPayItemType::with('businessPayItem')->find($id);
            return response()->success('', compact('businessPayItemType'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $businessPayItemType = BusinessPayItemType::find($id);
            if (!empty($businessPayItemType)) {
                $businessPayItemType->update($input);
                return response()->success('Your Record has been updated successfully!', compact('businessPayItemType'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessPayItemType = BusinessPayItemType::find($id)->delete();
                return response()->success('Business pay item type deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
