<?php

namespace App\Http\Controllers;

use App\BusinessInvoiceItem;
use Illuminate\Http\Request;
use \App\BusinessExpense;
use \App\BusinessInvoice;
use App\Http\Requests;
use App\Taggable;

class CronController extends BaseController
{
    public function expenseRecurring()
    {
        try {
            $expensesRecurrings = BusinessExpense::with('businessExpenseConfiguration', 'businessExpenseItem', 'vendor', 'notes')->where('type', 'RECURRING')->get();
            foreach ($expensesRecurrings as $expensesRecurring) {
                $startDate = $expensesRecurring->businessExpenseConfiguration->start_on;
                $schedule = $expensesRecurring->businessExpenseConfiguration->schedule;
                $neverExpire = $expensesRecurring->businessExpenseConfiguration->is_never_expires;

                if($schedule == 1) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addWeek();
                }
                if($schedule == 2) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addWeeks(2);
                }
                if($schedule == 3) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addWeeks(4);
                }
                if($schedule == 4) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addMonth();
                }
                if($schedule == 5) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addMonths(2);
                }
                /*if($schedule == 6) {
                    $scheduleDate = ;
                }*/

                $expense = new BusinessExpense;
                $expense->business_profile_id = $expensesRecurring->business_profile_id;
                $expense->payment_term = $expensesRecurring->payment_term;
                $expense->reference = $expensesRecurring->reference;
                $expense->date = $scheduleDate;
                $expense->due_date = $expensesRecurring->due_date;
                $expense->discount = $expensesRecurring->discount;
                $expense->type = 'EXPENSE';
                $expense->tax = $expensesRecurring->tax;
                $expense->amount = $expensesRecurring->amount;
                $expense->status = $expensesRecurring->status;
                $expense->created_by = $expensesRecurring->created_by;
                $expense->save();

                $expenseItems = $expensesRecurring->businessExpenseItem;
                $expenseNotes = $expensesRecurring->notes;
                $expenseVendor = $expensesRecurring->vendor;

                if(!empty($expenseItems)) {
                    foreach ($expenseItems as $expenseItem) {
                        $expenseItem = new BusinessExpenseItem;
                        $expenseItem->business_item_id = $expenseItem->business_item_id;
                        $expenseItem->business_expense_id = $expense->id;
                        $expenseItem->account_id = $expenseItem->account_id;
                        $expenseItem->description = $expenseItem->description;
                        $expenseItem->quantity = $expenseItem->quantity;
                        $expenseItem->price = $expenseItem->price;
                        $expenseItem->discount = $expenseItem->discount;
                        $expenseItem->amount = $expenseItem->amount;
                        $expenseItem->save();
                    }
                }

                if(!empty($expenseNotes)) {
                    foreach ($expenseNotes as $expenseNote) {
                        $taggable = Taggable::firstorCreate(['note_id' => $expenseNote->id, 'taggable_id' => $expense->id, 'taggable_type' => 'App\BusinessExpense']);
                    }
                }

                if(!empty($expenseVendor)) {
                    $businessExpenseVendor = new BusinessExpenseVendor;
                    $businessExpenseVendor->business_expense_id = $expense->id;
                    $businessExpenseVendor->vendor_id = $expenseVendor->id;
                    $businessExpenseVendor->save();
                }

            }
        } catch (\Exception $e) {
            \Log::error('Cron Expense Recurring ', $e->getTraceAsString());
            return response()->error('No records found');
        }
    }

    public function salesRecurring()
    {
        try {
            $salesRecurrings = BusinessInvoice::with('businessInvoiceConfiguration', 'businessInvoiceItem', 'notes', 'customer')->where('type', 'RECURRING')->get();
            foreach ($salesRecurrings as $salesRecurring) {
                $startDate = $salesRecurring->businessInvoiceConfiguration->start_on;
                $schedule = $salesRecurring->businessInvoiceConfiguration->schedule;
                $neverExpire = $salesRecurring->businessInvoiceConfiguration->is_never_expires;

                if($schedule == 1) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addWeek();
                }
                if($schedule == 2) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addWeeks(2);
                }
                if($schedule == 3) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addWeeks(4);
                }
                if($schedule == 4) {
                    $scheduleDate = \Carbon\Carbon::parse($startDate)->addMonth();
                }

                /*if($schedule == 5) {
                    $scheduleDate = ;
                }*/

                $invoice = new BusinessInvoice;
                $invoice->business_profile_id = $salesRecurring->business_profile_id;
                $invoice->invoice_no = 'asfdsadfas';
                $invoice->date = $scheduleDate;
                $invoice->reference = $salesRecurring->reference;
                $invoice->due_date = $salesRecurring->due_date;
                $invoice->status = 'DRAFT';
                $invoice->description = $salesRecurring->description;
                $invoice->payment_term = $salesRecurring->payment_term;
                $invoice->amount = $salesRecurring->amount;
                $invoice->discount = $salesRecurring->discount;
                $invoice->type = 'INVOICE';
                $invoice->tax = $salesRecurring->tax;
                $invoice->created_by = $salesRecurring->created_by;
                $invoice->save();

                $salesItems = $salesRecurring->businessInvoiceItem;
                $salesNotes = $salesRecurring->notes;
                $salesCustomer = $salesRecurring->customer;

                if(!empty($salesItems)) {
                    foreach ($salesItems as $salesItem) {
                        $invoiceItem = new BusinessInvoiceItem;
                        $invoiceItem->business_item_id = $salesItem->business_item_id;
                        $invoiceItem->business_invoice_id = $invoice->id;
                        $invoiceItem->account_id = $salesItem->account_id;
                        $invoiceItem->item_code = $salesItem->item_code;
                        $invoiceItem->name = $salesItem->name;
                        $invoiceItem->description = $salesItem->description;
                        $invoiceItem->quantity = $salesItem->quantity;
                        $invoiceItem->price = $salesItem->price;
                        $invoiceItem->discount = $salesItem->discount;
                        $invoiceItem->amount = $salesItem->amount;
                        $invoiceItem->save();
                    }
                }

                if(!empty($salesNotes)) {
                    foreach ($salesNotes as $salesNote) {
                        $taggable = Taggable::firstorCreate(['note_id' => $salesNote->id, 'taggable_id' => $invoice->id, 'taggable_type' => 'App\BusinessInvoice']);
                    }
                }

                if(!empty($salesCustomer)) {
                    $businessInvoiceCustomer = new BusinessInvoiceCustomer;
                    $businessInvoiceCustomer->business_invoice_id = $invoice->id;
                    $businessInvoiceCustomer->customer_id = $salesCustomer->id;
                    $businessInvoiceCustomer->save();
                }

            }
        } catch (\Exception $e) {
            \Log::error('Cron Sales Recurring ', $e->getTraceAsString());
            return response()->error('No records found');
        }
    }
}
