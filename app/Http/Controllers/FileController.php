<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Libraries\Util;
use App\Http\Requests;

class FileController extends BaseController
{
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $attachment = "";

            $file = $request->file('file');

            if (!empty($file)) {
                // define input file name.
                $filename = Util::rand_filename($file->getClientOriginalExtension());

                $attachment = Util::upload_file($file, $filename);
            }

            return \Response::json(["attachment" => $attachment]);
        }catch (Exception $e) {
            return \Response::json(['error' => 'Event creation unsuccessful. Please try again.'], HttpResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
