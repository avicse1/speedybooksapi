<?php

namespace App\Http\Controllers;

use App\Account;
use App\BusinessInvoiceConfiguration;
use App\BusinessInvoiceCustomer;
use App\BusinessProfile;
use App\Note;
use App\Taggable;
use Illuminate\Http\Request;
use App\BusinessInvoice;
use App\BusinessInvoiceItem;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessInvoiceController extends BaseController
{
    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['getInvoiceDetails']]);
        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessInvoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->orderBy('created_at', 'desc')->whereNull('deleted_at')->get();

            foreach ($businessInvoices as $businessInvoice) {
                $items = [];
                foreach ($businessInvoice->businessInvoiceItem as $item) {
                    $name = !empty($item->businessItem) ? $item->businessItem->name : '';
                    array_push($items, $name);
                }
                $businessInvoice['item_name'] = implode(', ', $items);
            }

            return response()->success('', compact('businessInvoices'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessInvoice::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;

                $businessInvoice = BusinessInvoice::create($input);

                if(!empty($input['customer_id'])) {
                    $businessInvoiceCustomer = new BusinessInvoiceCustomer;
                    $businessInvoiceCustomer->business_invoice_id = $businessInvoice->id;
                    $businessInvoiceCustomer->customer_id = $input['customer_id'];
                    $businessInvoiceCustomer->save();
                }

                if(!empty($input['Items'])) {
                    $items = $input['Items'];
                    foreach ($items as $item) {
                        $item['business_invoice_id'] = $businessInvoice->id;
                        BusinessInvoiceItem::create($item);
                    }
                }

                if(!empty($input['type']) && $input['type'] == 'RECURRING') {
                    $businessInvoiceConfiguration = new BusinessInvoiceConfiguration;
                    $businessInvoiceConfiguration->business_invoice_id = $businessInvoice->id;
                    $businessInvoiceConfiguration->schedule = $input['schedule'];
                    $businessInvoiceConfiguration->custom_days = $input['custom_days'];
                    $businessInvoiceConfiguration->custom_dates = $input['custom_dates'];
                    $businessInvoiceConfiguration->start_on = $input['start_on'];
                    $businessInvoiceConfiguration->end_on = empty($input['is_never_expires']) ? $input['end_on'] : null;
                    $businessInvoiceConfiguration->is_never_expires = !empty($input['is_never_expires']) ? $input['is_never_expires'] : 0;
                    $businessInvoiceConfiguration->save();
                }

                if(!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if(empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessInvoice->id, 'taggable_type' => 'App\BusinessInvoice']);
                    }
                }

                $businessInvoice->businessInvoiceItem;
                $businessInvoice->businessProfile;
                $businessInvoice->customer;
                $companyName = $businessInvoice->businessProfile->name;

                $appUrl = env('APP_UI_URL');
                $appViewURL = env('APP_UI_VIEW_URL');

                if(!empty($input['status']) && $input['status'] == 'SEND EMAIL') {
                    $sendEmailTo = $input['send_email_to'];
                    $pathToFile = public_path() . '/pdf/' . $businessInvoice->customer[0]->first_name . ' ' . $businessInvoice->customer[0]->last_name . '-' . $businessInvoice->invoice_no . '.pdf';
                    BusinessInvoiceController::invoiceView($businessInvoice->id, $pathToFile);

                    if($input['type'] == 'INVOICE') {
                        $appViewURL = $appViewURL . 'invoice.html#/?id=' . $businessInvoice->id;
                        \Mail::send('emails.businessInvoice', ['businessInvoice' => $businessInvoice, 'appUrl' => $appViewURL, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $pathToFile, $companyName) {
                            $message->to($sendEmailTo)->subject($companyName." : Business Invoice Generated");
                            $message->attach($pathToFile);
                        });
                    }
                    if($input['type'] == 'RECURRING') {
                        \Mail::send('emails.businessInvoiceRecurring', ['businessInvoice' => $businessInvoice, 'appUrl' => $appUrl, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $pathToFile, $companyName) {
                            $message->to($sendEmailTo)->subject($companyName." : Business Invoice Recurring Generated");
                            $message->attach($pathToFile);
                        });
                    }
                    if($input['type'] == 'ESTIMATES') {
                        $appViewURL = $appViewURL . 'estimate.html#/?id=' . $businessInvoice->id;
                        \Mail::send('emails.businessInvoiceEstimates', ['businessInvoice' => $businessInvoice, 'appUrl' => $appViewURL, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $pathToFile, $companyName) {
                            $message->to($sendEmailTo)->subject($companyName." : Business Invoice Estimates Generated");
                            $message->attach($pathToFile);
                        });
                    }
                }
                if($input['status'] == 'SEND EMAIL')
                {
                    $status = 'Sent';
                }
                if($input['status'] == 'APPROVED')
                {
                    $status = 'Saved';
                }
                if($input['status'] == 'DRAFT')
                {
                    $status = 'Drafted';
                }
                return response()->success('Business ' . strtolower($input['type']) . ' has been '. strtolower($status) .' successfully', compact('businessInvoice'));

            }
        } catch (\Exception $e) {
            \Log::error("Business Invoice creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $currentDate = \Carbon\Carbon::now()->format('Y-m-d');
            $businessInvoice = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer', 'businessInvoiceConfiguration', 'account', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessInvoice->businessInvoiceItem;
            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessItem) ?  $item->businessItem->name : '';
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
            }

            if($businessInvoice->type == 'INVOICE' || $businessInvoice->type == 'RECURRING') {
                if($businessInvoice->payment_term == 'Paid') {
                    $businessInvoice['payment_status'] = 'Paid';
                } else {
                    if($businessInvoice->due_date < $currentDate) {
                        $businessInvoice['payment_status'] = 'Over Due';
                    }
                    if($businessInvoice->due_date > $currentDate || $businessInvoice->du_date == $currentDate) {
                        $businessInvoice['payment_status'] = 'Open';
                    }
                }
            }

            return response()->success('', compact('businessInvoice'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $businessInvoice = BusinessInvoice::find($id);
            if (!empty($businessInvoice)) {
                $businessInvoice->update($input);

                if(!empty($input['customer_id'])) {
                    BusinessInvoiceCustomer::where('business_invoice_id', $businessInvoice->id)->delete();

                    $businessInvoiceCustomer = new BusinessInvoiceCustomer;
                    $businessInvoiceCustomer->business_invoice_id = $businessInvoice->id;
                    $businessInvoiceCustomer->customer_id = $input['customer_id'];
                    $businessInvoiceCustomer->save();
                }

                if(!empty($input['Items'])) {
                    BusinessInvoiceItem::where('business_invoice_id', $businessInvoice->id)->delete();
                    $items = $input['Items'];
                    foreach ($items as $item) {
                        if(!empty($item['business_item_id'])) {
                            $item['business_invoice_id'] = $businessInvoice->id;
                            BusinessInvoiceItem::create($item);
                        }
                    }
                }

                if(!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    Taggable::where('taggable_id', $businessInvoice->id)->where('taggable_type', 'App\BusinessInvoice')->delete();
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if(empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessInvoice->id, 'taggable_type' => 'App\BusinessInvoice']);
                    }
                }

                $appUrl = env('APP_UI_URL');
                if(!empty($input['status']) && $input['status'] == 'SEND EMAIL' && $input['type'] == "REFUND") {
                    $sendEmailTo = $input['send_email_to'];
                    $pathToFile = public_path() . '/pdf/' . $businessInvoice->customer[0]->first_name . ' ' . $businessInvoice->customer[0]->last_name . '-' . $businessInvoice->invoice_no . '.pdf';
                    BusinessInvoiceController::invoiceView($businessInvoice->id, $pathToFile);
                    \Mail::send('emails.businessRefund', ['businessRefund' => $businessInvoice, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo, $pathToFile) {
                        $message->to($sendEmailTo)->subject('Speedy Books: Business Refund Generated');
                        $message->attach($pathToFile);
                    });
                }

                return response()->success('Business ' . strtolower($input['type']) . ' has been '. strtolower($input['status']) .' successfully', compact('businessInvoice'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessInvoice = BusinessInvoice::find($id);
                $type = $businessInvoice->type;
                $businessInvoice->deleted_at = \Carbon\Carbon::now();
                $businessInvoice->save();

                return response()->success('Business ' . strtolower($type) . 'has been deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     * Generate unique invoice no.
     *
     * @return Response
     */
    public function generateInvoiceNo()
    {
        $businessProfileId = $this->_business_profile_id;

        $businessProfile = BusinessProfile::find($businessProfileId);

        $generateNumber = strtoupper(substr($businessProfile->name, 0, 3));

        $generateNumber .= rand(0,9999999999);

        self::checkInvoiceNumberExists($generateNumber);

        return $generateNumber;
    }

    public function checkInvoiceNumberExists($number)
    {
        $invoice = BusinessInvoice::where('invoice_no', $number)->first();

        if (!empty($invoice)) {
            self::generateInvoiceNo();
        } else {
            return $number;
        }
    }

    public function getInvoiceType($type)
    {
        try {
            $businessInvoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->where('type', $type)->orderBy('created_at', 'desc')->whereNull('deleted_at')->get();
            $currentDate = \Carbon\Carbon::now()->format('Y-m-d');

            foreach ($businessInvoices as $businessInvoice) {
                if($businessInvoice->type == 'INVOICE' || $businessInvoice->type == 'RECURRING') {
                    if($businessInvoice->payment_term == 'Paid') {
                        $businessInvoice['payment_status'] = 'Paid';
                    } else {
                        if($businessInvoice->due_date < $currentDate) {
                            $businessInvoice['payment_status'] = 'Over Due';
                        }
                        if($businessInvoice->due_date > $currentDate || $businessInvoice->due_date == $currentDate) {
                            $businessInvoice['payment_status'] = 'Open';
                        }
                    }
                }
                $items = [];
                foreach ($businessInvoice->businessInvoiceItem as $item) {
                    $name = !empty($item->businessItem) ? $item->businessItem->name : '';
                    array_push($items, $name);
                }
                $businessInvoice['item_name'] = implode(', ', $items);
            }

            return response()->success('', compact('businessInvoices'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function getInvoicePaymentType($type)
    {
        try {
            $currentDate = \Carbon\Carbon::now()->format('Y-m-d');

            if($type == 'paid') {
                $businessInvoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')->where('payment_term', 'Paid')->where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id)->get();
            }

            if($type == 'due') {
                $businessInvoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')->where('due_date', '<', $currentDate)->where('payment_term','!=' , 'Paid')->where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id)->get();
            }

            if($type == 'open') {
                $businessInvoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')->where('due_date', '>', $currentDate)->where('payment_term','!=' ,'Paid')->where('type', 'INVOICE')->where('business_profile_id', $this->_business_profile_id)->get();
            }

            return response()->success('', compact('businessInvoices'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found');
        }
    }

    /**
     * Upcoming Payment List
     *
     *
     * @return object
     */
    public function upcomingPaymentList()
    {
        try {
            $currentDate = \Carbon\Carbon::now()->format('Y-m-d');
            $after4Week = \Carbon\Carbon::now()->addWeeks(4)->format('Y-m-d');
            $invoices = BusinessInvoice::with('createdBy', 'businessInvoiceItem', 'notes', 'customer', 'taxRate')->where('type', 'INVOICE')->where('payment_term', '!=', 'Paid')->where('business_profile_id', $this->_business_profile_id)->whereBetween('due_date', [$currentDate, $after4Week])->take(3)->get();

            foreach ($invoices as $invoice) {
                $invoice->createdAt = \Carbon\Carbon::parse($invoice->created_at)->format('Y-m-d');
            }

            return response()->success('', compact('invoices'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found');
        }
    }

    public function sendInvoice($id, Request $request)
    {
        $input = $request->all();
        try {
            $businessInvoice = BusinessInvoice::find($id);

            $companyName = $businessInvoice->businessProfile->name;

            // $appUrl = env('APP_UI_URL') . 'invoice_details/' . $businessInvoice->id;
            $appUrl = env('APP_UI_URL');
            $appViewURL = env('APP_UI_VIEW_URL');

            $sendEmailTo = $input['send_email_to'];
            $pathToFile = public_path() . '/pdf/' . $businessInvoice->customer[0]->first_name . ' ' . $businessInvoice->customer[0]->last_name . '-' . $businessInvoice->invoice_no . '.pdf';
            BusinessInvoiceController::invoiceView($businessInvoice->id, $pathToFile);

            if($businessInvoice['type'] == 'INVOICE') {
                $appViewURL = $appViewURL . 'invoice.html#/?id=' . $businessInvoice->id;
                \Mail::send('emails.businessInvoice', ['businessInvoice' => $businessInvoice, 'appUrl' => $appViewURL, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $companyName, $pathToFile) {
                    $message->to($sendEmailTo)->subject($companyName.' : Business Invoice Generated');
                    $message->attach($pathToFile);
                });
            }
            if($businessInvoice['type'] == 'RECURRING') {
                \Mail::send('emails.businessInvoiceRecurring', ['businessInvoice' => $businessInvoice, 'appUrl' => $appUrl, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $companyName, $pathToFile) {
                    $message->to($sendEmailTo)->subject($companyName.' : Business Invoice Recurring Generated');
                    $message->attach($pathToFile);
                });
            }
            if($businessInvoice['type'] == 'ESTIMATES') {
                $appViewURL = $appViewURL . 'estimate.html#/?id=' . $businessInvoice->id;
                \Mail::send('emails.businessInvoiceEstimates', ['businessInvoice' => $businessInvoice, 'appUrl' => $appViewURL, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $companyName, $pathToFile) {
                    $message->to($sendEmailTo)->subject($companyName.' : Business Invoice Estimates Generated');
                    $message->attach($pathToFile);

                });
            }

            if($businessInvoice['type'] == 'REFUND') {
                \Mail::send('emails.businessRefund', ['businessRefund' => $businessInvoice, 'appUrl' => $appUrl, 'companyName' => $companyName], function ($message) use ($sendEmailTo, $companyName, $pathToFile) {
                    $message->to($sendEmailTo)->subject($companyName.' : Business Refund Generated');
                    $message->attach($pathToFile);

                });
            }

            return response()->success('Business invoice has been resent successfully!', compact('businessInvoice'));
        } catch (\Exception $e) {

        }
    }

    public function receivePayment($id, Request $request)
    {
        $input = $request->all();
        try {
            $businessInvoice =BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')->find($id);
            $items = $businessInvoice->businessInvoiceItem;
            $businessInvoice->update($input);

            return response()->success('Business invoice received payment successfully', compact('businessInvoice'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    public function invoiceView($id, $pathToFile)
    {
        try {
            $businessInvoice = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer', 'businessInvoiceConfiguration', 'account', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessInvoice->businessInvoiceItem;
            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessItem) ?  $item->businessItem->name : '';
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
            }
            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('invoice.view', compact('businessInvoice'))->save($pathToFile);

            return view('invoice.view', compact('businessInvoice'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    public function getDueInvoices()
    {
        try {
            $businessDueInvoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer', 'account', 'taxRate')
                            ->where('business_profile_id', $this->_business_profile_id)
                            ->where('payment_term', '!=', 'Paid')
                            ->where('type', 'INVOICE')
                            ->where('status', '!=', 'DRAFT')->get(['id', 'invoice_no', 'date', 'reference']);

            return response()->success('Get Due Business Invoices', compact('businessDueInvoices'));

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    public function getInvoiceDetails($id)
    {
        try {
            $businessInvoice = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer', 'businessInvoiceConfiguration', 'businessProfile', 'account', 'taxRate')->find($id);
            $items = $businessInvoice->businessInvoiceItem;
            $subTotal = 0;
            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessItem) ?  $item->businessItem->name : '';
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
                $subTotal += $item['amount'];
            }
            $businessInvoice['date'] = \Carbon\Carbon::parse($businessInvoice->date)->toFormattedDateString();
            $businessInvoice['due_date'] = \Carbon\Carbon::parse($businessInvoice->due_date)->toFormattedDateString();
            $businessInvoice['subtotal'] = $subTotal;

            return response()->success('', compact('businessInvoice'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }
}