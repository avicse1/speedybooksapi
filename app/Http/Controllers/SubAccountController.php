<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\SubAccount;
use \App\MainAccount;
use \App\Account;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class SubAccountController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $subAccounts = SubAccount::with('mainAccount', 'account')->get();

            foreach ($subAccounts as $subAccount) {
                $subAccount['main_account_name'] = $subAccount->mainAccount->name;
            }

            return response()->success('', compact('subAccounts'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, SubAccount::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $subAccount = SubAccount::create($input);
                return response()->success('Sub Account has been created successfully', compact('subAccount'));
            }
        } catch (\Exception $e) {
            \Log::error("Sub Account Type creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $subAccount = SubAccount::with('account', 'mainAccount')->find($id);
            return response()->success('', compact('subAccount'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $subAccount = SubAccount::find($id);
            if (!empty($subAccount)) {
                $validation = Validator::make($input, SubAccount::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $subAccount->update($input);
                    return response()->success('Your Record has been updated successfully !', compact('subAccount'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $subAccount = SubAccount::find($id)->delete();
                return response()->success('Sub Account Type deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
