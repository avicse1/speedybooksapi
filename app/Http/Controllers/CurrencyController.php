<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $currencies = Currency::all();

            return response()->success('', compact('currencies'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, Currency::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $currency = Currency::create($input);
                return response()->success('Currency has been created successfully', compact('currency'));
            }
        } catch (\Exception $e) {
            \Log::error("Currency creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $currency = Currency::with('businessProfile')->find($id);
            return response()->success('', compact('currency'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $currency = Currency::find($id);
            if (!empty($currency)) {
                $validation = Validator::make($input, Currency::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $currency->update($input);
                    return response()->success('Your Record has been updated successfully !', compact('currency'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $currency = Currency::find($id)->delete();

                return response()->success('Currency deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
