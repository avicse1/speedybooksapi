<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\State;
use Illuminate\Http\Request;

use App\Http\Requests;

class LocationController extends BaseController
{
    public function index()
    {
        $countries = Country::all();
        $states = State::all();
        $cities = City::all();

        $locations = array('countries' => $countries, 'states' => $states, 'cities' => $cities);
        return response()->success('', compact('locations'));
    }

    public function getCountries()
    {
        try {
            $countries = Country::all();
            return response()->success('', compact('countries'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function getStates($country_id)
    {
        try {
            $states = State::where('country_id', '=', $country_id)->get();
            return response()->success('', compact('states'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function getCities($state_id)
    {
        try {
            $cities = City::where('state_id', '=', $state_id)->get();
            return response()->success('', compact('cities'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }
}
