<?php

namespace App\Http\Controllers;

use Dingo\Api\Routing\Helpers;
use Illuminate\Support\Facades\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response as HttpResponse;

class BaseController extends Controller
{
    use Helpers;
    
    protected $_user = null;
    protected $_user_id = null;
    protected $_business_profile_id = null;

    public function __construct () {
        
        $token = JWTAuth::getToken();
        if (!empty($token)) {
            try {
                $this->_user = JWTAuth::parseToken()->toUser();
                $this->_user_id = $this->_user->id;
                $this->_business_profile_id = $this->_user->business_profile_id;
                $this->_user_role = $this->_user->roles;

            } catch (Exception $e) {
                return $this->response->errorUnauthorized($e->getMessage());
            }
        }
    }
}