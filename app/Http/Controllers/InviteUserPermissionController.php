<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\InviteUserPermission;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;

class InviteUserPermissionController extends BaseController
{
    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['getInviteUserDetails']]);
        parent::__construct();
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
       try {
            $inviteUsers = InviteUserPermission::where('business_profile_id', $this->_business_profile_id)->get();
            return response()->success('', compact('inviteUsers'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->get('inviteUser');
            $input['invite_by'] = $this->_user_id;
            $input['business_profile_id'] = $this->_business_profile_id;

            $validation = Validator::make($input, InviteUserPermission::$rules);

            if ($validation->fails())
            {
                return $validation->messages()->toArray();
            }
            else {
                $existInviteUser = InviteUserPermission::where('email', $input['email'])->count();

                $appUrl = env('APP_UI_URL');
                $apiDomain = env('API_DOMAIN');
                $token = base64_encode($input['email']);

                if(!$existInviteUser) {
                    $input['expiry_date'] = \Carbon\Carbon::now()->addHours(24);
                    if(!empty($input['cpa_permission'])) {
                        $input['cpa_permission'] = 1;
                        $input['invoice'] = null;
                        $input['expense'] = null;
                        $input['setting'] = null;
                        $input['reports'] = null;
                        $input['payroll'] = null;
                        $input['manage_users'] = null;
                        $input['banking'] = null;

                        $signupUrl = $appUrl . 'signup_accountant?token=' . $token;
                    } else {
                        $input['cpa_permission'] = 0;
                        if ($input['permissions'] == 'Basic') {
                            $input['invoice'] = $input['basic_invoice'];
                            $input['expense'] = $input['basic_expense'];
                            $input['reports'] = $input['basic_reports'];
                        }
                        if ($input['permissions'] == 'Intermediate') {
                            $input['invoice'] = $input['intermediate_invoice'];
                            $input['expense'] = $input['intermediate_expense'];
                            $input['reports'] = $input['intermediate_reports'];
                            $input['payroll'] = $input['intermediate_payroll'];
                        }
                        if ($input['permissions'] == 'Advance') {
                            $input['invoice'] = $input['advance_invoice'];
                            $input['expense'] = $input['advance_expense'];
                            $input['reports'] = $input['advance_reports'];
                            $input['payroll'] = $input['advance_payroll'];
                            $input['setting'] = $input['advance_setting'];
                            $input['manage_users'] = $input['advance_manageUsers'];
                            $input['banking'] = $input['advance_banking'];
                        }
                        $signupUrl = $appUrl . 'signup?token=' . $token;
                    }

                    if($this->_user_role == 'CPA') {
                        $input['invited_by_cpa'] = 1;
                    }
                    $inviteUserPermission = InviteUserPermission::create($input);
                    $inviteUserPermission->save();

                    $user = User::with('businessProfile')->where('id', $this->_user_id)->first();

                    \Mail::send('emails.users.inviteUser', ['inviteUser' => $inviteUserPermission, 'businessProfile' => $user->businessProfile, 'signupUrl' => $signupUrl, 'input' => $input, 'apiDomain' => $apiDomain], function ($message) use ($user, $input) {
                        $message->to($input['email'])->subject('Invitation to join ' . $user->businessProfile->name);
                    });

                    return response()->success('You have invited a user successfully!', compact('inviteUserPermission'));
                } else {
                    return response()->error('You have already sent invitation');
                }
            }
        } catch (\Exception $e) {
            \Log::error("User " . $e->getTraceAsString());
            return response()->error('Something went wrong ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $inviteUser = InviteUserPermission::find($id);
            return response()->success('', compact('inviteUser'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->get('inviteUser');

        if (!empty($id)) {
            $inviteUser = InviteUserPermission::find($id);
            if (!empty($inviteUser)) {
                if($input['cpa_permission'] == true) {
                    $inviteUser['cpa_permission'] = 1;
                    $inviteUser['invoice'] = null;
                    $inviteUser['expense'] = null;
                    $inviteUser['setting'] = null;
                    $inviteUser['reports'] = null;
                    $inviteUser['payroll'] = null;
                    $inviteUser['manage_users'] = null;
                    $inviteUser['banking'] = null;
                } else {
                    $inviteUser->permissions = $input['permissions'];
                    $inviteUser['cpa_permission'] = 0;
                    if ($input['permissions'] == 'Basic') {
                        $inviteUser->invoice = $input['basic_invoice'];
                        $inviteUser->expense = $input['basic_expense'];
                        $inviteUser->reports = $input['basic_reports'];
                    }
                    if ($input['permissions'] == 'Intermediate') {
                        $inviteUser->invoice = $input['intermediate_invoice'];
                        $inviteUser->expense = $input['intermediate_expense'];
                        $inviteUser->reports = $input['intermediate_reports'];
                        $inviteUser->payroll = $input['intermediate_payroll'];
                    }
                    if ($input['permissions'] == 'Advance') {
                        $inviteUser->invoice = $input['advance_invoice'];
                        $inviteUser->expense = $input['advance_expense'];
                        $inviteUser->reports = $input['advance_reports'];
                        $inviteUser->payroll = $input['advance_payroll'];
                        $inviteUser->setting = $input['advance_setting'];
                        $inviteUser->manage_users = $input['advance_manageUsers'];
                        $inviteUser->banking = $input['advance_banking'];
                    }
                }

                $inviteUser->save();
                return response()->success('Your Record has been updated successfully !', compact('inviteUser'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    public function getInviteUserDetails($token)
    {
        try {
            $email = base64_decode($token);

            $inviteUser = InviteUserPermission::where('email', $email)->where('status', 'PENDING')->where('expiry_date', '>' , \Carbon\Carbon::now())->where('expired', 0)->first();

            if(!empty($inviteUser)) {
                return response()->success('', compact('inviteUser'));
            } else {
                return response()->error('No invited user available');
            }
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function inactiveInviteUser($id)
    {
        try {
            $inviteUser = InviteUserPermission::where('status', 'ACTIVE')->where('id', $id)->first();

            if(!empty($inviteUser)) {
                $inviteUser->status = 'INACTIVE';
                $inviteUser->save();

                $user = User::find($inviteUser->user_id);
                $user->status = 'INACTIVE';
                $user->save();

                return response()->success('User has been deactivated successfully!', compact('inviteUser'));
            } else {
                return response()->success('User is not active already!', compact('inviteUser'));
            }
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function resendInvitation($id)
    {
        try {
            $inviteUser = InviteUserPermission::find($id);

            $appUrl = env('APP_UI_URL');
            $apiDomain = env('API_DOMAIN');
            $token = base64_encode($inviteUser['email']);


            if(!empty($inviteUser)) {
                if(!empty($inviteUser['cpa_permission']) && $inviteUser['cpa_permission'] == 1) {
                    $signupUrl = $appUrl . 'signup_accountant?token=' . $token;
                } else {
                    $signupUrl = $appUrl . 'signup?token=' . $token;
                }
                $user = User::with('businessProfile')->where('id', $this->_user_id)->first();

                \Mail::send('emails.users.inviteUser', ['inviteUser' => $inviteUser, 'businessProfile' => $user->businessProfile, 'signupUrl' => $signupUrl, 'input' => array(), 'apiDomain' => $apiDomain], function ($message) use ($user, $inviteUser) {
                    $message->to($inviteUser['email'])->subject('Invitation to join ' . $user->businessProfile->name);
                });

                return response()->success('Your invitation has been resend successfully!', compact('inviteUser'));
            } else {
                return response()->error('Something went wrong');
            }
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }
}
