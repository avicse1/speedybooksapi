<?php

namespace App\Http\Controllers;

use App\BusinessCheque;
use App\BusinessChequeItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;

class BusinessChequeController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessCheques = BusinessCheque::with('businessProfile', 'account', 'vendor', 'businessChequeItem')->where('business_profile_id', $this->_business_profile_id)
                ->whereNull('deleted_at')
                ->orderBy('created_at', 'desc')->get();

            return response()->success('', compact('businessCheques'));
        } catch (\Exception $e) {
            \Log::error('Business Cheques Details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessCheque::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;
                $businessCheque = BusinessCheque::create($input);

                $businessChequeItem = BusinessChequeItem::create([
                    'business_cheque_id' => $businessCheque->id,
                    'account_id' => $input['item_account_id'],
                    'description' => $input['description'],
                    'amount' => $input['amount']
                ]);

                return response()->success('Business check has been created successfully!', compact('businessCheque'));
            }
        } catch (\Exception $e) {
            \Log::error("Business cheque creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessCheque = BusinessCheque::with('businessProfile', 'account', 'vendor', 'businessChequeItem')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $businessCheque['item_account_name'] = !empty($businessCheque->businessChequeItem) ? $businessCheque->businessChequeItem->account->name : '';

            return response()->success('', compact('businessCheque'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request) {

        $input = $request->all();

        if (!empty($id)) {
            $businessCheque = BusinessCheque::find($id);
            if (!empty($businessCheque)) {
                $businessCheque->update($input);

                return response()->success('Business Check has been updated successfully!', compact('businessCheque'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessCheque = BusinessCheque::find($id);
                $businessCheque->deleted_at = \Carbon\Carbon::now();
                $businessCheque->save();

                return response()->success('Business check has been deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
