<?php

namespace App\Http\Controllers;

use App\BusinessPayrollItem;
use Illuminate\Http\Request;
use App\BusinessPayItemType;
use App\BusinessProfile;
use App\BusinessPayroll;
use App\BusinessEmployee;
use App\BusinessPayrollEmployee;
use App\Note;
use App\Taggable;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessPayrollController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessPayrolls = BusinessPayroll::with('businessPayrollItem', 'notes', 'employee', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->whereNull('deleted_at')->get();

            foreach ($businessPayrolls as $businessPayroll) {
                $businessPayroll['pay_period_from'] = \Carbon\Carbon::parse($businessPayroll->pay_period_from)->toFormattedDateString();
                $businessPayroll['pay_period_to'] = \Carbon\Carbon::parse($businessPayroll->pay_period_to)->toFormattedDateString();
            }
            return response()->success('', compact('businessPayrolls'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessPayroll::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;
                $businessPayroll = BusinessPayroll::create($input);

                if(!empty($input['employee_id'])) {
                    $businessPayrollEmployee = new BusinessPayrollEmployee;
                    $businessPayrollEmployee->business_payroll_id = $businessPayroll->id;
                    $businessPayrollEmployee->employee_id = $input['employee_id'];
                    $businessPayrollEmployee->save();
                }

                if(!empty($input['Items'])) {
                    $items = $input['Items'];

                    foreach ($items as $item) {
                        $item['business_payroll_id'] = $businessPayroll->id;
                        BusinessPayrollItem::create($item);
                    }
                }

                if(!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if(empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessPayroll->id, 'taggable_type' => 'App\BusinessPayroll']);
                    }
                }

                $businessPayroll->businessPayrollItem;
                $businessPayroll->businessProfile;
                $businessPayroll->employee;

                return response()->success('Business Payroll  has been '. strtolower($input['status']) .' successfully', compact('businessPayroll'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Payroll creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessPayroll = BusinessPayroll::with('businessPayrollItem', 'notes', 'employee', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessPayroll->businessPayrollItem;
            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessPayItem) ? $item->businessPayItem->name : '';
            }
            return response()->success('', compact('businessPayroll'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessPayrollItems = BusinessPayrollItem::where('business_payroll_id', '=', $id)->delete();
                $businessPayrollEmployee = BusinessPayrollEmployee::where('business_payroll_id', '=', $id)->delete();
                $businessPayroll = BusinessPayroll::find($id)->delete();

                return response()->success('Business payroll deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    public function sendEmail($id, Request $request)
    {
        $input = $request->all();
        try {
            $businessPayroll = BusinessPayroll::with('businessPayrollItem', 'notes', 'employee', 'taxRate', 'businessProfile')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $appUrl = env('APP_UI_URL');
            if(!empty($input['status']) && $input['status'] == 'SEND EMAIL') {
                $sendEmailTo = $input['send_email_to'];
                $businessPayroll->status = 'SEND EMAIL';
                $businessPayroll->save();

                $companyName = $businessPayroll->businessProfile->name;
                $pathToFile = public_path() . '/pdf/' . $businessPayroll->employee[0]->name . '- payroll' . '.pdf';
                self::attachPdf($businessPayroll->id, $pathToFile);

                \Mail::send('emails.businessPayroll', ['businessPayroll' => $businessPayroll, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo, $pathToFile, $companyName) {
                    $message->to($sendEmailTo)->subject($companyName . " : Business payroll Generated");
                    $message->attach($pathToFile);
                });
            }

            return response()->success('Payroll has been sent successfully', compact('businessPayroll'));

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    public function attachPdf($id, $pathToFile)
    {
        try {
            $businessPayroll = BusinessPayroll::with('businessPayrollItem', 'notes', 'employee', 'taxRate', 'businessProfile')
                                ->where('business_profile_id', $this->_business_profile_id)->find($id);

            $businessPayroll['payment_date'] = \Carbon\Carbon::parse($businessPayroll->payment_date)->toFormattedDateString();
            $businessPayroll['pay_period_from'] = \Carbon\Carbon::parse($businessPayroll->pay_period_from)->toFormattedDateString();
            $businessPayroll['pay_period_to'] = \Carbon\Carbon::parse($businessPayroll->pay_period_to)->toFormattedDateString();

            $pdf = app('dompdf.wrapper');
            $pdf = $pdf->loadView('payroll.payroll_pdf_layout', compact('businessPayroll'))->save($pathToFile);

            return view('payroll.payroll_pdf_layout', compact('businessPayroll'));

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }
}
