<?php

namespace App\Http\Controllers;

use App\Account;
use App\BusinessExpenseConfiguration;
use App\BusinessExpenseVendor;
use App\Note;
use App\Taggable;
use Illuminate\Http\Request;
use \App\BusinessExpense;
use \App\BusinessExpenseItem;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessExpenseController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessExpenses = BusinessExpense::with('businessExpenseItem', 'vendor', 'notes', 'businessExpenseConfiguration', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->orderBy('created_at', 'desc')->whereNull('deleted_at')->get();

            return response()->success('', compact('businessExpenses'));
        } catch (\Exception $e) {
            \Log::error('Business Expense Details ' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessExpense::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;
                $businessExpense = BusinessExpense::create($input);

                if(!empty($input['vendor_id'])) {
                    $businessExpenseVendor = new BusinessExpenseVendor;
                    $businessExpenseVendor->business_expense_id = $businessExpense->id;
                    $businessExpenseVendor->vendor_id = $input['vendor_id'];
                    $businessExpenseVendor->save();
                }

                $accountPayableList = ['Advertising & Marketing', 'Meals & Rent', 'Travel & Vehicle Expense', 'Insurance', 'Entertainment',
                                       'Office Expense', 'Legal Expense', 'Telephone & Internet', 'Subscription', 'Reserve', 'Cost of goods sold', 'Purchase',
                                       'Freight & Delivery'
                ];
                $assestAccount = ['Office Equipment', 'Vehicle', 'Investment'];

                if(!empty($input['Items'])) {
                    $items = $input['Items'];
                    foreach ($items as $item) {
                        $item['business_expense_id'] = $businessExpense->id;
                        BusinessExpenseItem::create($item);
                    }
                }

                if(!empty($input['type']) && $input['type'] == 'RECURRING') {
                    $businessExpenseConfiguration = new BusinessExpenseConfiguration;
                    $businessExpenseConfiguration->business_expense_id = $businessExpense->id;
                    $businessExpenseConfiguration->schedule = $input['schedule'];
                    $businessExpenseConfiguration->custom_days = $input['custom_days'];
                    $businessExpenseConfiguration->custom_dates = $input['custom_dates'];
                    $businessExpenseConfiguration->start_on = $input['start_on'];
                    $businessExpenseConfiguration->end_on = empty($input['is_never_expires']) ? $input['end_on'] : null;
                    $businessExpenseConfiguration->is_never_expires = !empty($input['is_never_expires']) ? $input['is_never_expires'] : 0;
                    $businessExpenseConfiguration->save();
                }

                if(!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if(empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessExpense->id, 'taggable_type' => 'App\BusinessExpense']);
                    }
                }

                $businessExpense->businessExpenseItem;
                $businessExpense->businessProfile;
                $businessExpense->vendor;

                $appUrl = env('APP_UI_URL');

                if(!empty($input['status']) && $input['status'] == 'SEND EMAIL') {
                    $sendEmailTo = $input['send_email_to'];
                    if($input['type'] == 'EXPENSE') {
                        \Mail::send('emails.businessExpense', ['businessExpense' => $businessExpense, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo) {
                            $message->to($sendEmailTo)->subject('Speedy Books: Business Expense Generated');
                        });
                    }
                    if($input['type'] == 'RECURRING') {
                        \Mail::send('emails.businessExpenseRecurring', ['businessExpense' => $businessExpense, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo) {
                            $message->to($sendEmailTo)->subject('Speedy Books: Business Expense Recurring Generated');
                        });
                    }
                }

                if($input['status'] == 'SEND EMAIL')
                {
                    $status = 'Sent';
                }
                if($input['status'] == 'APPROVED')
                {
                    $status = 'Saved';
                }
                if($input['status'] == 'DRAFT') {
                    $status = 'Drafted';
                }

                return response()->success('Business '. strtolower($input['type']) .' has been '. strtolower($status) .' successfully', compact('businessExpense'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Expense creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessExpense = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor', 'businessExpenseConfiguration', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessExpense->businessExpenseItem;
            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessItem) ? $item->businessItem->name : '';
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
            }
            return response()->success('', compact('businessExpense'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request) {

        $input = $request->all();

        if (!empty($id)) {
            $businessExpense = BusinessExpense::find($id);
            if (!empty($businessExpense)) {
                $businessExpense->update($input);

                if(!empty($input['vendor_id'])) {
                    BusinessExpenseVendor::where('business_expense_id', $businessExpense->id)->delete();

                    $businessExpenseVendor = new BusinessExpenseVendor;
                    $businessExpenseVendor->business_expense_id = $businessExpense->id;
                    $businessExpenseVendor->vendor_id = $input['vendor_id'];
                    $businessExpenseVendor->save();
                }

                if(!empty($input['Items'])) {
                    BusinessExpenseItem::where('business_expense_id', $businessExpense->id)->delete();
                    $items = $input['Items'];
                    foreach ($items as $item) {
                        if(!empty($item['business_item_id'])) {
                            $item['business_expense_id'] = $businessExpense->id;
                            BusinessExpenseItem::create($item);
                        }
                    }
                }

                if(!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    Taggable::where('taggable_id', $businessExpense->id)->where('taggable_type', 'App\BusinessExpense')->delete();
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if(empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessExpense->id, 'taggable_type' => 'App\BusinessExpense']);
                    }
                }

                return response()->success('Business ' . strtolower($input['type']) . ' has been updated successfully', compact('businessExpense'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessExpense = BusinessExpense::find($id);
                $type = $businessExpense->type;

                $businessExpense->deleted_at = \Carbon\Carbon::now();
                $businessExpense->save();

                return response()->success('Business ' . strtolower($type) . 'has been deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    public function getExpensesType($type)
    {
        try {
            $businessExpenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor', 'businessExpenseConfiguration', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->where('type', $type)->orderBy('created_at', 'desc')->whereNull('deleted_at')->get();

            return response()->success('', compact('businessExpenses'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function makePayment($id, Request $request) {

        $input = $request->all();
        try {
            $businessExpense = BusinessExpense::find($id);
            $items = $businessExpense->businessExpenseItem;
            $businessExpense->update($input);
            return response()->success('Business expense make payment successfully', compact('businessExpense'));
        } catch (\Exception $e) {

        }
    }

    public function getDueExpenses()
    {
        try {
            $businessDueExpenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor', 'account', 'taxRate')
                ->where('business_profile_id', $this->_business_profile_id)
                ->where('payment_term', '!=', 'Paid')
                ->where('type', 'EXPENSE')
                ->where('status', '!=', 'DRAFT')->get(['id', 'expense_no', 'date', 'reference']);

            return response()->success('Get Due Business Expenses', compact('businessDueExpenses'));

        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }
}
