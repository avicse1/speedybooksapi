<?php

namespace App\Http\Controllers;

use App\AccountBalance;
use Illuminate\Http\Request;
use \App\Account;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class AccountController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $accounts = Account::where('parent_id', '!=', 0)->get();
            foreach ($accounts as $account) {
                $account['type'] = '';
                if($account->parent_id != 0) {
                    $mainAccount = Account::where('id', $account->parent_id)->first();
                    $account['type'] = !empty($mainAccount) ? $mainAccount->name : '';
                }
                $accountBalance = AccountBalance::where('business_profile_id', $this->_business_profile_id)->where('account_id', $account->id)->first();
                $account['account_balance'] = !empty($accountBalance) ? $accountBalance : null;
            }
            return response()->success('', compact('accounts'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();
            $validation = Validator::make($input, Account::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                if(!empty($input['is_subaccount']) && $input['is_subaccount'] == true) {
                    $input['parent_id'] = $input['detail_type_id'];
                    if(!empty($input['detail_type_id'])) {
                        $detailType = Account::find($input['detail_type_id']);
                        $input['detail_type'] = $detailType->name;
                    } else {
                        $input['detail_type'] = $input['name'];
                    }

                    $account = Account::create($input);
                    $accountBalanceArray = [32, 34, 35, 36];

                    if(in_array($input['detail_type_id'], $accountBalanceArray)) {
                        $accountBalance = ['business_profile_id' => $this->_business_profile_id,
                                           'account_id' => $account->id,
                                           'balance' => $input['balance'],
                                           'date' => \Carbon\Carbon::now()
                        ];
                        AccountBalance::create($accountBalance);
                    }

                    if(!empty($input['track_depreciation'])) {
                        $accountBalance = ['business_profile_id' => $this->_business_profile_id,
                            'account_id' => $account->id,
                            'balance' => $input['original_cost'],
                            'original_cost' => $input['original_cost'],
                            'track_depreciation' => 1,
                            'deprication_cost' => $input['depreciation_cost'],
                            'depreciation_asof'=> $input['depreciation_asof'],
                            'depreciation_at'=> $input['depreciation_at'],
                            'date' => \Carbon\Carbon::now()
                        ];

                        AccountBalance::create($accountBalance);
                    }

                } else {
                    if(!empty($input['detail_type_id'])) {
                        $detailType = Account::find($input['detail_type_id']);
                        $input['detail_type'] = $detailType->name;
                    } else {
                        $input['detail_type'] = $input['name'];
                    }

                    $account = Account::create($input);

                    if(!empty($input['track_depreciation'])) {
                        $accountBalance = ['business_profile_id' => $this->_business_profile_id,
                            'account_id' => $account->id,
                            'balance' => $input['original_cost'],
                            'original_cost' => $input['original_cost'],
                            'track_depreciation' => 1,
                            'depreciation_cost' => $input['depreciation_cost'],
                            'depreciation_asof'=> $input['depreciation_asof'],
                            'depreciation_at'=> $input['depreciation_at'],
                            'date' => \Carbon\Carbon::now()
                        ];

                        AccountBalance::create($accountBalance);
                    }
                }
                return response()->success('Account has been created successfully!', compact('account'));
            }
        } catch (\Exception $e) {
            \Log::error("Account creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $account = Account::with('accountBalance')->find($id);
            $accountBalance = AccountBalance::where('business_profile_id', $this->_business_profile_id)->where('account_id', $account->id)->first();
            $account['account_balance'] = !empty($accountBalance) ? $accountBalance : null;
            return response()->success('', compact('account'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $account = Account::with('accountBalance')->find($id);
            if (!empty($account)) {
                $validation = Validator::make($input, Account::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $account->update($input);
                    $accountBalance = AccountBalance::where('business_profile_id', $this->_business_profile_id)->where('account_id', $account->id)->first();
                    if(!empty($accountBalance)) {
                        $accountBalance->balance = $input['balance'];
                        $accountBalance->date = \Carbon\Carbon::now()->format('Y-m-d');
                        $accountBalance->save();
                    } else {
                        // AccountBalance::create([
                        //     'business_profile_id' => $this->_business_profile_id,
                        //     'account_id' => $account->id,
                        //     'date' => \Carbon\Carbon::now()->format('Y-m-d'),
                        //     'balance' => $input['balance']
                        // ]);
                    }

                    return response()->success('Your Record has been updated successfully!', compact('account'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $account = Account::find($id)->delete();

                return response()->success('Account deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     * get Parent Account list
     *
     *
     * @return response
     */

    public function getParentAccountList()
    {
        try {
            $parentAccounts = Account::with('accountBalance')->where('parent_id', '=', 0)->get();
            return response()->success('', compact('parentAccounts'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * get Receive payment account List
     *
     *
     * @return response
     */

    public function getReceivePaymentAccountList()
    {
        try {
            $receivePaymentMainAccount = Account::with('accountBalance')->where('name', 'Current Assets')->first();

            $receivePaymentAccountLists = Account::with('accountBalance')->where('parent_id', $receivePaymentMainAccount->id)->whereIn('name', ['Bank Account', 'Cash Account', 'Cash & Cash Equivalents'])->get();

            foreach ($receivePaymentAccountLists as $receivePaymentAccountList) {
                $receivePaymentAccountList['type'] = '';
                if($receivePaymentAccountList->parent_id != 0) {
                    $mainAccount = Account::where('id', $receivePaymentAccountList->parent_id)->first();
                    $receivePaymentAccountList['type'] = !empty($mainAccount) ? $mainAccount->name : '';
                }
            }

            return response()->success('', compact('receivePaymentAccountLists'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function getDetailTypeList($parentAccountId)
    {
        try {
            $detailTypeLists = Account::with('accountBalance')->where('parent_id', $parentAccountId)->get();

            return response()->success('', compact('detailTypeLists'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }
}
