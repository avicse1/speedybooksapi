<?php namespace App\Http\Controllers;

use App\BusinessProfile;
use App\BusinessProfileUser;
use App\InviteUserPermission;
use App\Libraries\Util;
use App\User;
use Dompdf\Exception;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Crypt;

class UsersController extends BaseController
{

    public function __construct()
    {
        $this->middleware('api.auth', ['except' => ['authenticate', 'store', 'forgotPassword', 'userPassword']]);
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $users = User::with('country', 'state', 'city', 'businessProfile')->where('id', $this->_user_id)->whereNull('deleted_at')->get();
            return response()->success('', compact('users'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $messages = [
                'required' => ':attribute field is required.',
            ];

            $validation = Validator::make($input, User::$rules, $messages);

            if ($validation->fails())
            {
                return $validation->messages()->toArray();
            }
            else {

                $existUser = User::where('email', $input['email'])->where('status', '!=', 'DELETED')->count();

                if(!$existUser) {
                    $token = base64_encode($input['email']);
                    $input['password'] = $token;
                    $input['expiry_date'] = \Carbon\Carbon::now()->addMonth();

                    $inviteUser = InviteUserPermission::where('email', $input['email'])->where('status', 'PENDING')->where('expiry_date', '>', \Carbon\Carbon::now())->first();

                    $user = User::create($input);
                    $user->country;
                    $user->state;
                    $user->city;

                    if(!empty($inviteUser) && $inviteUser->invited_by_cpa == 0) {
                        $inviteUser->user_id = $user->id;
                        $inviteUser->status = 'ACTIVE';
                        $inviteUser->expired = 1;
                        $inviteUser->save();

                        if($inviteUser->cpa_permission == 1) {
                            $generateNumber = rand(0,999999999999);
                            $businessProfile = new  BusinessProfile;
                            $businessProfile->company_id = $generateNumber;
                            $businessProfile->save();

                            $businessProfileUser = new BusinessProfileUser;
                            $businessProfileUser->business_profile_id = $businessProfile->id;
                            $businessProfileUser->user_id = $user->id;
                            $businessProfileUser->save();

                            $user->roles = 'CPA';
                            $user->business_profile_id = $businessProfile->id;
                            $user->save();
                        } else {
                            $user->roles = 'MANAGE';
                            $user->save();

                            $existBusinessProfile = User::where('id', $inviteUser->invite_by)->first();

                            $businessProfileUser = new BusinessProfileUser;
                            $businessProfileUser->business_profile_id = $existBusinessProfile->business_profile_id;
                            $businessProfileUser->user_id = $user->id;
                            $businessProfileUser->save();

                            $user->business_profile_id = $existBusinessProfile->business_profile_id;
                            $user->save();
                        }

                    } else {

                        if(!empty($inviteUser) && $inviteUser->invited_by_cpa == 1)
                        {
                            $inviteUser->user_id = $user->id;
                            $inviteUser->status = 'ACTIVE';
                            $inviteUser->expired = 1;
                            $inviteUser->save();
                        }
                        if(empty($inviteUser) && $input['roles'] == 'CPA'){
                            $inviteUser = new InviteUserPermission;
                            $inviteUser->first_name = $input['first_name'];
                            $inviteUser->last_name = $input['last_name'];
                            $inviteUser->email = $input['email'];
                            $inviteUser->cpa_permission = 1;
                            $inviteUser->user_id = $user->id;
                            $inviteUser->status = 'ACTIVE';
                            $inviteUser->expired = 1;
                            $inviteUser->save();
                        }
                        $generateNumber = rand(0,999999999999);
                        $businessProfile = new  BusinessProfile;
                        $businessProfile->company_id = $generateNumber;
                        $businessProfile->save();

                        $businessProfileUser = new BusinessProfileUser;
                        $businessProfileUser->business_profile_id = $businessProfile->id;
                        $businessProfileUser->user_id = $user->id;
                        $businessProfileUser->save();

                        $user->business_profile_id = $businessProfile->id;
                        $user->save();
                    }

                    $appUrl = env('APP_UI_URL');

                    \Mail::send('emails.users.UserRegistered', ['user' => $user, 'app_url' => $appUrl, 'token' => $token], function ($message) use ($user) {
                        $message->to($user->email)->subject('New User Registered - Speedybooks');
                    });

                    return response()->success('Your registration is sucessful, kindly check your email for the activation link.', compact('user'));
                } else {
                    return response()->error('This email is already registered. Please try with another email.');
                }
            }
        } catch (\Exception $e) {
            \Log::error("User Registration: " . $e->getTraceAsString());
            return response()->error('Something went wrong ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $user = User::with('country', 'state', 'city', 'businessProfile')->find($id);
            return response()->success('', compact('user'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        if (!empty($id)) {
            $user = User::find($id);
            if (!empty($user)) {
                $user->first_name = \Request::get('first_name');
                $user->last_name = $request->input('last_name');
                $user->phone = $request->input('phone');
                $user->email = $request->input('email');
                $user->role = $request->input('role');
                $user->country_id = $request->input('country_id');
                $user->state_id = $request->input('state_id');
                $user->city_id = $request->input('city_id');
                $user->save();

                return response()->success('Your Record has been updated successfully !', compact('user'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $user = User::find($id)->update(['deleted_at' => \Carbon\Carbon::now(), 'status' => 'DELETED']);

                return reponse()->success('User deleted successfully !');
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     * Reset Password for the User
     *
     * @return Response
     */
    public function forgotPassword()
    {
        $email = \Request::get('email', '');
        $phone = \Request::get('phone', '');

        if (isset($email) && !empty($email)) {
            try {
                $user = User::whereEmail($email)->first();
                $password = str_random(16);
                $user->password = $password;
                $user->save();

                \Mail::send('emails.users.password', ['name' => $user->name, 'password' => $password, 'email' => $email], function ($message) use ($user) {
                    $message->to($user->email)->subject('Speedy Books: Reset Password');
                });

                return response()->success('Please check your email for the new password.', compact('user'));
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records founds.');
            }
        }

        if (isset($phone) && !empty($phone)) {
            try {
                $user = User::wherePhone($phone)->first();
                $password = str_random(16);
                $user->password = $password;
                $user->save();

                return response()->success('Your new password is sent over the phone.', compact('user'));
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found.');
            }
        }
    }

    /**
     * Authenticate the User
     *
     * @param  Request $request
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $input = $request->only('username', 'password');

        $password = $input['password'];
        $username = $input['username'];

        if (!empty($username) || !empty($password)) {
            try {

                if (is_numeric($username)) {
                    $credentials = ['phone' => $username, 'password' => $password, 'status' => 'ACTIVE'];
                } else {
                    $credentials = ['email' => $username, 'password' => $password, 'status' => 'ACTIVE'];
                }

                if (\Auth::attempt($credentials)) {
                    $user = \Auth::user();
                    $inviteUser = InviteUserPermission::where('invite_by', $user->id)->where('status', 'ACTIVE')->count();
                    $user['invite_user_count'] = $inviteUser;
                    $user->businessProfile;
                    $user->userPermission;
                    $user->token = \JWTAuth::fromUser($user);
                    return response()->success('', compact('user'));
                } else {
                    \Log::error('Log in user');
                    return response()->error('Invalid username or password.');
                }
            } catch (\Exception $e) {
                \Log::error('Authenticate User ' . $e->getTraceAsString());
                return response()->error('Invalid username or password.');
            }
        } else {
            return response()->error('Invalid username or password.');
        }
    }

    public function userPassword(Request $request)
    {
        $token = $request->input('token');
        $email = base64_decode($request->input('email'));
        $password = $request->input('password');

        $rules = ['password' => 'required'];

        try {
            $validation = Validator::make($request->all(), $rules);

            if ($validation->fails())
            {
                return $validation->messages()->toArray();
            }
            else {
                $user = User::where('email', $email)->where('password', $token)->first();

                if (!empty($user)) {
                    $user->password = $password;
                    $user->status = 'ACTIVE';
                    $user->save();

                    return response()->success('Your password has been created successfully!');
                } else {
                    \Log::error('Invalid Token');
                    return response()->error('Invalid Token');
                }
            }

        } catch (\Exception $e) {
            \Log::error('User password creation' . $e->getTraceAsString());
            return response()->error('Something went wrong !');
        }
    }

    public function userChangePassword(Request $request)
    {
        $password = $request->input('password');
        $email = $request->input('email');

        $rules = ['password' => 'required'];

        try {
            $validation = Validator::make($request->all(), $rules);

            if ($validation->fails())
            {
                return $validation->messages()->toArray();
            }
            else {
                $user = User::where('email', $email)->first();
                if (!empty($user)) {
                    $user->password = $password;
                    $user->save();
                    return response()->success('Your password has been changed successfully !');
                }
            }
        } catch (\Exception $e) {
            \Log::error('User password creation' . $e->getTraceAsString());
            return response()->error('Something went wrong !');
        }
    }

    public function getCPAUsers()
    {
        $role = $this->_user_role;

        try {
            $users = array();
            if ($role == 'CPA') {
                $inviteUser = InviteUserPermission::where('user_id', $this->_user_id)->first();

//            invited by user
                if (!empty($inviteUser)) {
                    $user = User::with('country', 'state', 'city', 'businessProfile')->where('id', $inviteUser->invite_by)->first();
                    array_push($users, array('invitedByUser' => $user));
                }

//            invited by cpa
                $inviteUsers = InviteUserPermission::with('user')->where('invited_by_cpa', 1)->where('invite_by', $this->_user_id)->get();

                foreach ($inviteUsers as $inviteUser) {
                    if (!empty($inviteUser->user)) {
                        $inviteUser->user->businessProfile;
                    }
                }

                array_push($users, array('invitedByCpa' => $inviteUsers));
            }

            return response()->success('', compact('users'));
        } catch (Exception $e) {
            \Log::error('Get all CPA Users' . $e->getTraceAsString());
            return response()->error('Something went wrong !');
        }
    }
}
