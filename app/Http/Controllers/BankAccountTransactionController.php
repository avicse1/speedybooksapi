<?php

namespace App\Http\Controllers;

use App\Account;
use Dompdf\Exception;
use Illuminate\Http\Request;
use \App\BankAccount;
use \App\BankAccountTransaction;
use App\Http\Requests;

class BankAccountTransactionController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $bankAccount = BankAccount::with('bankAccountTransaction', 'account')->where('user_id', $this->_user_id)->orderBy('id', 'desc')->take(1)->first();

            return response()->success('', compact('bankAccount'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function changeTransactionAccount($id, Request $request)
    {
        $input = $request->all();

        try {
            $bankAccountTransaction = BankAccountTransaction::find($id);
            $account = Account::find($input['account_id']);

            $bankAccountTransaction->category = $account->name;
            $bankAccountTransaction->description = $input['description'];
            $bankAccountTransaction->save();

            return response()->success('You have changed transaction in different account');
        } catch (Exception $e) {
            return response()->error('No records found.');
        }
    }

    public function destroy($id) {
        if (!empty($id)) {
            try {
                $bankAccountTransaction = BankAccountTransaction::find($id);
                $bankAccountTransaction->deleted_at = \Carbon\Carbon::now();
                $bankAccountTransaction->save();

                return response()->success('Bank Account transaction deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
