<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Vendor;
use \App\BusinessVendor;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class VendorController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $vendors = Vendor::where('business_profile_id', $this->_business_profile_id)->where('status', '!=', 'DELETED')->whereNull('deleted_at')->get();

            return response()->success('', compact('vendors'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $existVendor = Vendor::where('email', $input['email'])->first();
            $businessVendor = BusinessVendor::where('business_profile_id', $this->_business_profile_id)->where('vendor_id', $existVendor['id'])->first();

            if(empty($businessVendor)) {
                $vendor = Vendor::create($input);
                $businessVendor = BusinessVendor::create([
                    'business_profile_id' => $this->_business_profile_id,
                    'vendor_id' => $vendor->id
                ]);

                return response()->success('Vendor has been created successfully', compact('vendor'));
            } else {
                return response()->error('Vendor already exists');
            }
        } catch (\Exception $e) {
            \Log::error("Vendor creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $vendor = Vendor::find($id);
            return response()->success('', compact('vendor'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $vendor = Vendor::find($id);
            if (!empty($vendor)) {
                $vendor->update($input);
                return response()->success('Your Record has been updated successfully !', compact('vendor'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $vendor = Vendor::find($id)->update(['deleted_at' => \Carbon\Carbon::now(), 'status' => 'DELETED']);
                return response()->success('Vendor deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
