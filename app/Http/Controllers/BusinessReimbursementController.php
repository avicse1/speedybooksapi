<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BusinessReimbursement;
use App\BusinessReimbursementItem;
use App\Note;
use App\Taggable;
use App\BusinessProfile;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class BusinessReimbursementController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessReimbursements = BusinessReimbursement::with('businessReimbursementItem','businessProfile', 'customer', 'notes', 'taxRate')
                                    ->where('business_profile_id', $this->_business_profile_id)
                                    ->whereNull('deleted_at')->orderBy('created_at', 'desc')->get();

            return response()->success('', compact('businessReimbursements'));
        } catch (\Exception $e) {
            \Log::error('Business Reimbursement Order Details ' . $e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BusinessReimbursement::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $input['created_by'] = $this->_user_id;
                $businessReimbursement = BusinessReimbursement::create($input);

                if (!empty($input['Items'])) {
                    $items = $input['Items'];

                    foreach ($items as $item) {
                        $item['business_reimbursement_id'] = $businessReimbursement->id;
                        BusinessReimbursementItem::create($item);
                    }
                }

                if (!empty($input['Notes'])) {
                    $notes = $input['Notes'];
                    foreach ($notes as $note) {
                        $noteObject = Note::where('title', $note)->first();
                        if (empty($noteObject)) {
                            $noteObject = new Note;
                            $noteObject->title = $note;
                            $noteObject->save();
                        }
                        $taggable = Taggable::firstorCreate(['note_id' => $noteObject->id, 'taggable_id' => $businessReimbursement->id, 'taggable_type' => 'App\BusinessReimbursement']);
                    }
                }

                $businessReimbursement->businessReimbursementItem;
                $businessReimbursement->businessProfile;
                $businessReimbursement->vendor;
                

                $appUrl = env('APP_UI_URL');

                if (!empty($input['status']) && $input['status'] == 'SEND EMAIL') {
                    $sendEmailTo = $input['send_email_to'];
                    \Mail::send('emails.businessReimbursement', ['businessReimbursement' => $businessReimbursement, 'appUrl' => $appUrl], function ($message) use ($sendEmailTo) {
                        $message->to($sendEmailTo)->subject('Speedy Books: Business Reimbursement Order Generated');
                    });
                }


                // return response()->success('Business Reimbursement has been '. strtolower($input['status']) .' successfully', compact('businessReimbursement'));
                return response()->success('Business Reimbursement has been sent successfully', compact('businessReimbursement'));
            }
        } catch (\Exception $e) {
            \Log::error("Business Reimbursement order creation " . $e->getTraceAsString());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessReimbursement = BusinessReimbursement::with('businessReimbursementItem','businessProfile', 'customer', 'notes', 'taxRate')->where('business_profile_id', $this->_business_profile_id)->find($id);
            $items = $businessReimbursement->businessReimbursementItem;

            foreach ($items as $item) {
                $item['item_name'] = !empty($item->businessItem) ?  $item->businessItem->name : '';
                $item['account_name'] = !empty($item->account) ? $item->account->name : '';
            }
            return response()->success('', compact('businessReimbursement'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessReimbursementItem = BusinessReimbursementItem::where('business_reimbursement_id', '=', $id)->delete();
                $businessReimbursement = BusinessReimbursement::find($id)->delete();

                return response()->success('Business Reimbursement deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     * Generate unique invoice no.
     *
     * @return Response
     */
    public function generateReimbursementNo()
    {
        $businessProfileId = $this->_business_profile_id;

        $businessProfile = BusinessProfile::find($businessProfileId);

        $generateNumber = 'RM' . strtoupper(substr($businessProfile->name, 0, 3));

        $generateNumber .= rand(0, 9999999999);

        self::checkReimbursementNumberExists($generateNumber);

        return $generateNumber;
    }

    public function checkReimbursementNumberExists($number)
    {
        $businessReimbursement = BusinessReimbursement::where('reimbursement_no', $number)->first();

        if (!empty($businessReimbursement)) {
            self::generateInvoiceNo();
        } else {
            return $number;
        }
    }
}
