<?php

namespace App\Http\Controllers;

use App\BusinessProfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Transformers\BusinessProfileTransformer;

class BusinessProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $businessProfiles = BusinessProfile::with('country', 'state', 'user', 'businessNature', 'financialYear')->where('status', '!=', 'DELETED')->whereNull('deleted_at')->get();

            return response()->success('', compact('businessProfiles'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            if(!empty($input['is_skipped'])) {
                $validation = Validator::make($input,BusinessProfile::$rules);

                if ($validation->fails())
                {
                    return response()->error($validation->messages()->toArray());
                }
            }
            $input['created_by'] = $this->_user_id;
            $input['user_id'] = $this->_user_id;
            $businessProfile = BusinessProfile::create($input);
            $businessProfile->country;
            $businessProfile->state;
            $businessProfile->currency;
            $businessProfile->financialYear;
            $businessProfile->businessNature;
            return response()->success('Business Profile has been created successfully!', compact('businessProfile'));
        } catch (\Exception $e) {
            \Log::error("Business Profile creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $businessProfile = BusinessProfile::with('country', 'state', 'user', 'businessNature', 'financialYear')->find($id);
            $businessProfile->logourl = !empty($businessProfile->logo) ?  \URL::to('/' . $businessProfile->logo) : '';
            return response()->success('', compact('businessProfile'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $businessProfile = BusinessProfile::find($id);
            if (!empty($businessProfile)) {

                if($input['is_skipped'] == 0) {
                    $validation = Validator::make($input,BusinessProfile::$rules);

                    if ($validation->fails())
                    {
                        return response()->error($validation->messages()->toArray());
                    }
                }

                $input['status'] = 'ACTIVE';
                $input['updated_by'] = $this->_user_id;
                $businessProfile->update($input);
                return response()->success('Your Record has been updated successfully!', compact('businessProfile'));
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $businessProfile = BusinessProfile::find($id)->update(['deleted_at' => \Carbon\Carbon::now(), 'status' => 'DELETED']);

                return response()->success('Business Profile deleted successfully!', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
