<?php

namespace App\Http\Controllers;

use App\Account;
use App\BankAccountTransaction;
use App\BusinessExpense;
use App\BusinessInvoice;
use App\BusinessJournal;
use App\BusinessJournalItem;
use Illuminate\Http\Request;
use \App\BankAccount;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use \App\Libraries\Util;
use Excel;

class BankAccountController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $match = array();
            $inSpeedybooks = array();
            $balance = 0;

            $bankAccount = BankAccount::with('account')->where('business_profile_id', $this->_business_profile_id)->orderBy('id', 'desc')->take(1)->first();
            if(!empty($bankAccount)) {
                $bankAccountTransactions = BankAccountTransaction::whereNull('deleted_at')->where('bank_account_id', $bankAccount->id)->get();
                foreach ($bankAccountTransactions as $transaction) {
                    if ($transaction->is_speedybooks == 1) {
                        array_push($inSpeedybooks, $transaction);
                    } else {
                        array_push($match, $transaction);
                    }
                }

                $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
                    ->where('business_profile_id', $this->_business_profile_id)
                    ->where('type', 'INVOICE')
                    ->where('status', '!=', 'DRAFT')
                    ->where('payment_term', 'Paid')
                    ->where('account_id', $bankAccount->account_id)
                    ->whereNull('deleted_at')->get();
                foreach ($invoices as $invoice) {
                    $items = $invoice->businessInvoiceItem;
                    foreach ($items as $item) {
                        $balance += $item->amount;
                    }
                }

                $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
                    ->where('business_profile_id', $this->_business_profile_id)
                    ->where('type', 'EXPENSE')
                    ->where('status', '!=', 'DRAFT')
                    ->where('payment_term', 'Paid')
                    ->where('account_id', $bankAccount->account_id)
                    ->whereNull('deleted_at')->get();

                foreach ($expenses as $expense) {
                    $items = $expense->businessExpenseItem;
                    foreach ($items as $item) {
                        $balance -= $item->amount;
                    }
                }

                $businessJournals = BusinessJournal::with('businessJournalItem')
                    ->where('business_profile_id', $this->_business_profile_id)
                    ->where('status', '!=', 'DRAFT')
                    ->whereNull('deleted_at')->get();

                foreach ($businessJournals as $businessJournal) {
                    $items = $businessJournal->businessJournalItem;
                    foreach ($items as $item) {
                        if($item->account_id == $bankAccount->account_id) {
                            if(!empty($item->debit) && $item->debit > 0) {
                                $balance += $item->debit;
                            } else {
                                $balance -= $item->credit;
                            }
                        }
                    }
                }

                $bankAccount['match'] = $match;
                $bankAccount['in_speedybooks'] = $inSpeedybooks;
                $bankAccount['bank_account_transactions'] = $bankAccountTransactions;
            } else {
                $bankAccount['bank_account_transactions'] = array();
                $bankAccount['match'] = array();
                $bankAccount['in_speedybooks'] = array();
            }

            return response()->success('', compact('bankAccount', 'balance'));
        } catch (\Exception $e) {
            \Log::error('Bank Account List' . $e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, BankAccount::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $existBankAccount = BankAccount::where('user_id', $this->_user_id)->where('account_id', $input['account_id'])->first();

                $file = $request->file('upload');
                $count = 0;

                if (!empty($file)) {
                    $path = $file->getRealPath();
                    $data = Excel::load($path, function ($reader) {
                        $results = $reader->get();
                    })->get();

                    $count =  count($data[0]);
                    $column = array();
                    $i = 1;
                    foreach ($data[0] as $key=>$value) {
                        array_push($column, array('name' => 'Column ' . $i . ': ' . $key, 'value' => $i));
                        $i++;
                    }
                    // define input file name.
                    $filename = Util::rand_filename($file->getClientOriginalExtension());

                    $attachment = Util::upload_file($file, $filename);
                }

                if(!empty($existBankAccount)) {
                    $existBankAccount['upload'] = !empty($attachment) ? $attachment : null;
                    $existBankAccount->save();
                    $existBankAccount['columnCount'] = $count;
                    $existBankAccount['columns'] = $column;
                    $bankAccount = $existBankAccount;
                } else {
                    $input['upload'] =!empty($attachment) ? $attachment : null;
                    $input['business_profile_id'] = $this->_business_profile_id;
                    $input['user_id'] = $this->_user_id;
                    $bankAccount = BankAccount::create($input);
                    $bankAccount['columnCount'] = $count;
                    $bankAccount['columns'] = $column;
                }
                return response()->success('Bank Account has been created successfully', compact('bankAccount'));
            }
        } catch (\Exception $e) {
            \Log::error("Account creation " . $e->getTraceAsString());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $bankAccount = BankAccount::with('account')->where('user_id', $this->_user_id)->where('account_id', $id)->first();
            $match = array();
            $inSpeedybooks = array();
            $balance = 0;

            if(!empty($bankAccount)) {
                $bankAccountTransactions = BankAccountTransaction::whereNull('deleted_at')->where('bank_account_id', $bankAccount->id)->get();
                $bankAccount['bank_account_transactions'] = $bankAccountTransactions;

                foreach ($bankAccountTransactions as $transaction) {
                    if($transaction->is_speedybooks == 1) {
                        array_push($inSpeedybooks, $transaction);
                    } else {
                        array_push($match, $transaction);
                    }
                }
                $invoices = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')
                    ->where('business_profile_id', $this->_business_profile_id)
                    ->where('type', 'INVOICE')
                    ->where('status', '!=', 'DRAFT')
                    ->where('payment_term', 'Paid')
                    ->where('account_id', $id)
                    ->whereNull('deleted_at')->get();
                foreach ($invoices as $invoice) {
                    $items = $invoice->businessInvoiceItem;
                    foreach ($items as $item) {
                        $balance += $item->amount;
                    }
                }

                $expenses = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')
                    ->where('business_profile_id', $this->_business_profile_id)
                    ->where('type', 'EXPENSE')
                    ->where('status', '!=', 'DRAFT')
                    ->where('payment_term', 'Paid')
                    ->where('account_id', $id)
                    ->whereNull('deleted_at')->get();

                foreach ($expenses as $expense) {
                    $items = $expense->businessExpenseItem;
                    foreach ($items as $item) {
                        $balance -= $item->amount;
                    }
                }

                $bankAccount['match'] = $match;
                $bankAccount['in_speedybooks'] = $inSpeedybooks;
            } else {
                $bankAccount['bank_account_transactions'] = array();
                $bankAccount['match'] = array();
                $bankAccount['in_speedybooks'] = array();
            }
            return response()->success('', compact('bankAccount', 'balance'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $bankAccount = BankAccount::find($id);
            if (!empty($currency)) {
                $validation = Validator::make($input, BankAccount::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $bankAccount->update($input);
                    return response()->success('Bank Account has been updated successfully !', compact('bankAccount'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $bankAccount = BankAccount::find($id)->delete();

                return response()->success('Bank Account deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     *
     *
     */
    public function uploadTransaction($id, Request $request)
    {
        if (!empty($id)) {
            try {
                $bankAccount = BankAccount::find($id);
                $input = $request->all();
                $path = public_path($bankAccount->upload);
                $csvData = array();
                $transactions = array();
                $i = 1;

                if(!empty($path)) {
                    $file = fopen($path, 'r');
                    while (($result = fgetcsv($file)) !== false)
                    {
                        $csvData[] = $result;
                    }

                    foreach ($csvData as $key => $value) {
                        if(!empty($input['header'])) {
                            if ($key != 0) {
                                if(!empty($input['amountIn']) && $input['amountIn'] == 1){
                                   $amount = $value[$input['first_amount'] - $i];
                                } else {
                                    if(!empty($value[$input['first_amount'] - $i])) {
                                        $amount = $value[$input['first_amount'] - $i];
                                    } else {
                                        $amount = $value[$input['second_amount'] - $i];
                                    }
                                }
                                $transactions[] = ['id' => $key + 1,
                                             'date' => date('Y-m-d', strtotime($value[$input['date_column'] - $i])),
                                             'description' => $value[$input['description'] - $i],
                                             'amount' => $amount,
                                             'isChecked' => true,
                                ];
                            }
                        } else {
                            if(!empty($input['amountIn']) && $input['amountIn'] == 1){
                                $amount = $value[$input['first_amount'] - $i];
                            } else {
                                if(!empty($value[$input['first_amount'] - $i])) {
                                    $amount = $value[$input['first_amount'] - $i];
                                } else {
                                    $amount = $value[$input['second_amount'] - $i];
                                }
                            }
                            $transactions[] = ['id' => $key + 1,
                                'date' => date('Y-m-d', strtotime($value[$input['date_column'] - $i])),
                                'description' => $value[$input['description'] - $i],
                                'amount' => $amount,
                                'isChecked' => true,
                            ];
                        }
                    }
                    fclose($file);
                }
                return response()->success('Bank Transaction', compact('transactions'));
            } catch (\Exception $e) {
                \Log::error('Upload transaction' . $e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    public function saveBankTransaction($id, Request $request)
    {
        if (!empty($id)) {
            try {
                $input = $request->all();
                $bankAccount = BankAccount::find($id);
                $transactions = $input['transaction'];
                foreach ($transactions as $transaction) {
                    if($transaction['isChecked'] == true) {
                        $transaction['bank_account_id'] = $bankAccount->id;
                        if (strpos($transaction['amount'], '-') !== false) {
                            $transaction['type'] = 'Expense';
                            $transaction['category'] = 'Expense';
                            $transaction['amount'] = ltrim($transaction['amount'], '-');
                        } else {
                            $transaction['type'] = 'Income';
                            $transaction['category'] = 'Income';
                        }
                        BankAccountTransaction::create($transaction);
                    }
                }
                return response()->success('Bank Account transactions saved successfully !');
            } catch (\Exception $e) {
                \Log::error('Save Bank Transaction' . $e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }

    /**
     * Create Journal Entries
     * @param $id transaction Id
     * @return mixed
     */
    public function createJournal($id, Request $request)
    {
        $input = $request->all();
        try {
            $bankAccountTransaction = BankAccountTransaction::find($id);
            $bankAccount = BankAccount::with('account')->find($bankAccountTransaction->bank_account_id);
            $journal = ['business_profile_id' => $this->_business_profile_id,
                'name' => $input['name'],
                'date' => $input['date'],
                'status' => 'APPROVED',
                'total_debit' => $bankAccountTransaction->amount,
                'total_credit' => $bankAccountTransaction->amount,
                'banking_type' => 1
            ];
            $businessJournal = BusinessJournal::create($journal);

            if($bankAccountTransaction->type == 'Income') {
                $items[] = ['business_journal_id' => $businessJournal->id,
                    'account_id' => $input['account_id'],
                    'description'=> $input['description'],
                    'debit' => 0,
                    'credit' => $bankAccountTransaction->amount
                ];

                $items[] = ['business_journal_id' => $businessJournal->id,
                    'account_id' => $bankAccount->account_id,
                    'description'=> $input['description'],
                    'debit' => $bankAccountTransaction->amount,
                    'credit' => 0
                ];
            } else {
                $items[] = ['business_journal_id' => $businessJournal->id,
                    'account_id' => $input['account_id'],
                    'description'=> $input['description'],
                    'debit' => $bankAccountTransaction->amount,
                    'credit' => 0
                ];

                $items[] = ['business_journal_id' => $businessJournal->id,
                    'account_id' => $bankAccount->account_id,
                    'description'=> $input['description'],
                    'debit' => 0,
                    'credit' => $bankAccountTransaction->amount
                ];
            }

            foreach ($items as $item) {
                BusinessJournalItem::create($item);
            }

            $input['transaction_id'] = $id;
            $input['object_id'] = $businessJournal->id;
            $bankAccount = BankAccount::addInSpeedybooks($input, 'JOURNAL');

            return response()->success('Transaction added in ' . $bankAccount->account->name, compact('bankAccount'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    public function undoTransaction($id) {
        try {
            $bankAccountTransaction = BankAccountTransaction::find($id);
            $bankAccount = BankAccount::with('account')->find($bankAccountTransaction->bank_account_id);
            $amount = $bankAccount->speedybooks_balance;

            if($bankAccountTransaction->type == 'Income') {
                $amount =  $amount - $bankAccountTransaction->amount;
            } else {
                $amount = $amount + $bankAccountTransaction->amount;
            }

            $bankAccount->speedybooks_balance = $amount;
            $bankAccount->save();

            $bankAccountTransaction->is_speedybooks = 0;
            $bankAccountTransaction->save();

            $match = array();
            $inSpeedybooks = array();

            $bankAccountTransactions = BankAccountTransaction::whereNull('deleted_at')->where('bank_account_id', $bankAccount->id)->get();
            $bankAccount['bank_account_transactions'] = $bankAccountTransactions;

            foreach ($bankAccountTransactions as $transaction) {
                if($transaction->is_speedybooks == 1) {
                    array_push($inSpeedybooks, $transaction);
                } else {
                    array_push($match, $transaction);
                }
            }
            $bankAccount['match'] = $match;
            $bankAccount['in_speedybooks'] = $inSpeedybooks;

            return response()->success('', compact('bankAccount'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Update Invoice Due to paid and transaction move to Is Speedybooks
     *
     * @param $id Invoice Id
     * @param Request $request
     * @return mixed
     */
    public function invoiceReceivePayment($id, Request $request)
    {
        $input = $request->all();
        $input['object_id'] = $id;
        try {
            $bankAccount = BankAccount::addInSpeedybooks($input, 'INVOICE');

            $account = Account::find($bankAccount->account_id);

            $businessInvoice = BusinessInvoice::with('businessInvoiceItem', 'notes', 'customer')->find($id);
            $items = $businessInvoice->businessInvoiceItem;
            $input['account_id'] = $account->id;
            $input['banking_type'] = 1;
            $businessInvoice->update($input);

            return response()->success('Receive Payment successfully!', compact('bankAccount'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }

    /**
     * Update Expense Due to paid and transaction move to Is Speedybooks
     *
     * @param $id Expense Id
     * @param Request $request
     * @return mixed
     */
    public function expenseMakePayment($id, Request $request)
    {
        $input = $request->all();
        $input['object_id'] = $id;
        try {
            $bankAccount = BankAccount::addInSpeedybooks($input, 'EXPENSE');

            $account = Account::find($bankAccount->account_id);

            $businessExpense = BusinessExpense::with('businessExpenseItem', 'notes', 'vendor')->find($id);
            $items = $businessExpense->businessExpenseItem;
            $input['account_id'] = $account->id;
            $input['banking_type'] = 1;
            $businessExpense->update($input);

            return response()->success('Make Payment successfully!', compact('bankAccount'));
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            return response()->error('No records found.');
        }
    }
}
