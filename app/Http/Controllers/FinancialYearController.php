<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FinancialYear;
use App\Http\Requests;
use Illuminate\Support\Facades\Validator;

class FinancialYearController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try {
            $financialYears = FinancialYear::all();

            return response()->success('', compact('financialYears'));
        } catch (\Exception $e) {
            return response()->error('No records found.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $validation = Validator::make($input, FinancialYear::$rules);

            if ($validation->fails()) {
                return response()->error($validation->messages()->toArray());
            } else {
                $financialYear = FinancialYear::create($input);
                return response()->success('Financial Year has been created successfully', compact('financialYear'));
            }
        } catch (\Exception $e) {
            \Log::error("Financial Year creation " . $e->getMessage());
            return response()->error('Something went wrong. Please try again !');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        try {
            $financialYear = FinancialYear::find($id);
            return response()->success('', compact('financialYear'));
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
            return response()->error('No records found.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  Request $request
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        if (!empty($id)) {
            $financialYear = FinancialYear::find($id);
            if (!empty($financialYear)) {
                $validation = Validator::make($input, FinancialYear::$rules);

                if ($validation->fails()) {
                    return $validation->messages()->toArray();
                } else {
                    $financialYear->update($input);
                    return response()->success('Your Record has been updated successfully !', compact('financialYear'));
                }
            } else {
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        if (!empty($id)) {
            try {
                $financialYear = FinancialYear::find($id)->delete();

                return response()->success('Financial Year deleted successfully !', null);
            } catch (\Exception $e) {
                \Log::error($e->getMessage());
                return response()->error('No records found');
            }
        } else {
            \Log::error("No parameter passed");
            return response()->error('No records found.');
        }
    }
}
