<?php

namespace App\Http\Middleware;

use Closure;

class RequestResponseLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        /*\Log::info('requests', [
            'url' => \URL::current(),
            'method' => \Request::method(),
            'request' => $request->all(),
            'response' => $response->original
        ]);*/
    }
}
