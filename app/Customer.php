<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model {

    protected $table = 'customers';

    protected $fillable = array('business_profile_id', 'title', 'first_name', 'last_name','email', 'phone', 'mobile',
                                'company_name', 'website', 'notes', 'attachment',
                                'billing_street', 'billing_zip', 'billing_city', 'billing_state', 'billing_country',
                                'shipping_street', 'shipping_zip', 'shipping_city', 'shipping_state', 'shipping_country',
                                'status', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'first_name' => 'required',
        'email' => 'required',
    ];

    public function businessCustomer()
    {
        return $this->hasMany('\App\BusinessCustomer');
    }

    public function businessProfile()
    {
        return $this->belongsToMany('\App\BusinessProfile', 'business_customers', 'customer_id', 'business_profile_id');
    }

    public function businessInvocie()
    {
        return $this->belongsToMany('\App\BusinessInvoice', 'business_invoice_customers', 'customer_id', 'business_invoice_id');
    }

    public function businessInvoiceCustomer()
    {
        return $this->hasMany('\App\BusinessInvoiceCustomer');
    }

    public function businessRefund()
    {
        return $this->belongsToMany('\App\BusinessRefund', 'business_refund_customers', 'customer_id', 'business_refund_id');
    }

    public function businessRefundCustomer()
    {
        return $this->hasMany('\App\BusinessRefundCustomer');
    }

    public function businessReimbursement()
    {
        return $this->hasMany('\App\BusinessReimbursement');
    }
}