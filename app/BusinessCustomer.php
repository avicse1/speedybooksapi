<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessCustomer extends Model {

    protected $table = 'business_customers';

    protected $fillable = array('business_profile_id', 'customer_id');

    public $timestamps = false;

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile', 'business_profile_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer', 'customer_id', 'id');
    }
}