<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Response::macro('success', function ($message = '', $response = null) {
            return \Response::json([
                'code'  => 200,
                'message'  => $message,
                'response' => $response
            ]);
        });

        \Response::macro('error', function ($message, $status = 400) {
            return \Response::json([
                'code'  => $status,
                'message' => $message,
            ], $status);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
