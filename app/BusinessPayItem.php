<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPayItem extends Model {

    protected $table = 'business_pay_items';

    protected $fillable = array('business_pay_item_type_id', 'account_id', 'name', 'description');

    public $timestamps = false;

    public static $rules = [
        'business_pay_item_type_id' => 'required',
        'name' => 'required',
        'description' => 'required',
    ];

    public function businessPayItemType()
    {
        return $this->belongsTo('\App\BusinessPayItemType');
    }

    public function businessPayrollItem()
    {
        return $this->hasMany('\App\BusinessPayrollItem');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }
}