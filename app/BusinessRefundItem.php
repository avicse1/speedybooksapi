<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessRefundItem extends Model {

    protected $table = 'business_refund_items';

    protected $fillable = array('business_refund_id', 'business_item_id', 'description', 'quantity', 'price', 'amount', 'discount', 'account_id', 'item_code');

    public $timestamps = false;

    public static $rules = [
        'business_refund_id' => 'required',
        'business_item_id' => 'required',
        'description' => 'required',
        'quantity' => 'required',
        'price' => 'required',
        'amount' => 'required',
        'account_id' => 'required',
    ];

    public function businessRefund()
    {
        return $this->belongsTo('\App\BusinessRefund');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function businessItem()
    {
        return $this->belongsTo('\App\BusinessItem');
    }
}