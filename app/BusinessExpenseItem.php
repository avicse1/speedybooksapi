<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessExpenseItem extends Model {

    protected $table = 'business_expense_items';

    protected $fillable = array('business_expense_id', 'business_item_id', 'description', 'quantity', 'price', 'amount', 'discount', 'account_id', 'item_code');

    public $timestamps = false;

    public static $rules = [
        'business_expense_id' => 'required',
        'business_item_id' => 'required',
        'description' => 'required',
        'quantity' => 'required',
        'price' => 'required',
        'amount' => 'required',
        'account_id' => 'required',
    ];

    public function businessExpense()
    {
        return $this->belongsTo('\App\BusinessExpense');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public function businessItem()
    {
        return $this->belongsTo('\App\BusinessItem');
    }
}