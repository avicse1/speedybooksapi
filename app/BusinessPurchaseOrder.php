<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPurchaseOrder extends Model {

    protected $table = 'business_purchase_orders';

    protected $fillable = array('business_profile_id', 'vendor_id', 'tax_rate_id','purchase_order_no', 'reference', 'date', 'delivery_date', 'tax_amount', 'attachment', 'discount', 'amount', 'status', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'business_profile_id' => 'required',
        'vendor_id' => 'required',
        'purchase_order_no' => 'required',
        'date' => 'required',
        'delivery_date' => 'required',
    ];

    public function businessPurchaseOrderItem()
    {
        return $this->hasMany('\App\BusinessPurchaseOrderItem');
    }

    public function vendor()
    {
        return $this->belongsTo('\App\Vendor');
    }

    public function notes()
    {
        return $this->morphToMany('\App\Note', 'taggable');
    }

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function taxRate()
    {
        return $this->belongsTo('\App\TaxRate');
    }
}