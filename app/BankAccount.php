<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model {

    protected $table = 'bank_accounts';

    protected $fillable = array('name', 'description', 'business_profile_id', 'user_id', 'account_id', 'upload', 'speedybooks_balance');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required',
    ];

    public function bankAccountTransaction()
    {
        return $this->hasMany('\App\BankAccountTransaction');
    }

    public function user()
    {
        return $this->belongsTo('\App\User');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }

    public static function addInSpeedybooks($input, $type)
    {
        $bankAccountTransaction = BankAccountTransaction::find($input['transaction_id']);
        $bankAccountTransaction['transaction_type'] = $type;
        $bankAccountTransaction['object_id'] = $input['object_id'];
        $bankAccountTransaction->is_speedybooks = 1;
        $bankAccountTransaction->save();

        $bankAccount = BankAccount::with('account')->find($bankAccountTransaction->bank_account_id);

        $match = array();
        $inSpeedybooks = array();

        $bankAccountTransactions = BankAccountTransaction::whereNull('deleted_at')->where('bank_account_id', $bankAccount->id)->get();
        $bankAccount['bank_account_transactions'] = $bankAccountTransactions;

        foreach ($bankAccountTransactions as $transaction) {
            if($transaction->is_speedybooks == 1) {
                array_push($inSpeedybooks, $transaction);
            } else {
                array_push($match, $transaction);
            }
        }
        $bankAccount['match'] = $match;
        $bankAccount['in_speedybooks'] = $inSpeedybooks;

        return $bankAccount;
    }
}