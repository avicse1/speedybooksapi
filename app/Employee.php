<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model {

    protected $table = 'employees';

    protected $fillable = array('employement_no', 'name', 'date_of_birth', 'type', 'date_of_joining', 'blood_group', 'gender', 'marital_status', 'address', 'landline_no', 'mobile_no',
        'email', 'contact_name', 'contact_mobile_no', 'contact_relation', 'status', 'is_terminated', 'last_working_day', 'description', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [

    ];

    public function businessProfile()
    {
        return $this->belongsToMany('\App\BusinessProfile', 'business_employees', 'employee_id', 'business_profile_id');
    }

    public function businessPayroll()
    {
        return $this->belongsToMany('\App\BusinessPayroll', 'business_payroll_employees', 'employee_id', 'business_payroll_id');
    }

    public function businessPayrollEmployee()
    {
        return $this->hasMany('\App\BusinessPayrollEmployee');
    }
}