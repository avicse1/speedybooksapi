<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessNature extends Model {

    protected $table = 'business_natures';

    protected $fillable = array('name');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required'
    ];

    public function businessProfile()
    {
        return $this->hasMany('\App\BusinessProfile');
    }
}