<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessProfile extends Model {

    protected $table = 'business_profiles';

    protected $fillable = array('state_id', 'country_id', 'company_id','name', 'financial_year_id', 'currency_id', 'business_nature_id',
                                'status', 'industry', 'registration_no', 'description', 'telephone_no', 'email', 'website',
                                'physical_address','registered_address',
                                'is_skipped', 'logo', 'address', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'name' => 'required',
        'financial_year_id' => 'required',
        'currency_id' => 'required',
        'business_nature_id' => 'required',
        'state_id' => 'required',
        'country_id' => 'required'
    ];

    public function businessProfileUser()
    {
        return $this->hasMany('\App\BusinessProfileUser');
    }

    public function user()
    {
        return $this->belongsToMany('\App\User', 'business_profile_users', 'business_profile_id', 'user_id');
    }

    public function country()
    {
        return $this->belongsTo('\App\Country');
    }

    public function state()
    {
        return $this->belongsTo('\App\State');
    }

    public function currency()
    {
        return $this->belongsTo('\App\Currency');
    }

    public function businessNature()
    {
        return $this->belongsTo('\App\BusinessNature');
    }

    public function financialYear()
    {
        return $this->belongsTo('\App\FinancialYear');
    }

    public function businessCustomer()
    {
        return $this->hasMany('\App\BusinessCustomer');
    }

    public function customer()
    {
        return $this->belongsToMany('\App\Customer', 'business_customers', 'business_profile_id', 'customer_id');
    }

    public function businessVendor()
    {
        return $this->hasMany('\App\BusinessVendor');
    }

    public function vendor()
    {
        return $this->belongsToMany('\App\Vendor', 'business_vendors', 'business_profile_id', 'vendor_id');
    }

    public function businessItem()
    {
        return $this->hasMany('\App\BusinessItem');
    }

    public function businessExpense()
    {
        return $this->hasMany('\App\BusinessExpense');
    }

    public function businessInvoice()
    {
        return $this->hasMany('\App\BusinessInvoice');
    }

    public function businessPurchaseOrder()
    {
        return $this->hasMany('\App\BusinessPurchaseOrder');
    }

    public function businessReimbursement()
    {
        return $this->hasMany('\App\BusinessReimbursement');
    }

    public function employee()
    {
        return $this->belongsToMany('\App\Employee', 'business_employees', 'business_profile_id', 'employee_id');
    }

    public function businessEmployee()
    {
        return $this->hasMany('\App\BusinessEmployee');
    }

    public function businessPayroll()
    {
        return $this->hasMany('\App\BusinessPayroll');
    }

    public function businessJournal()
    {
        return $this->hasMany('\App\BusinessJournal');
    }

    public function businessCheque()
    {
        return $this->hasMany('\App\BusinessCheque');
    }

    public function businessReconcile()
    {
        return $this->hasMany('\App\BusinessReconcile');
    }

    public function accountBalance()
    {
        return $this->hasMany('\App\AccountBalance');
    }
}