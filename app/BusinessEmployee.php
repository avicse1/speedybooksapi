<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessEmployee extends Model {

    protected $table = 'business_employees';

    protected $fillable = array('business_profile_id', 'employee_id');

    public $timestamps = false;

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile', 'business_profile_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo('\App\Employee', 'employee_id', 'id');
    }
}