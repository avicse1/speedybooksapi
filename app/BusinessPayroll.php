<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessPayroll extends Model {

    protected $table = 'business_payrolls';

    protected $fillable = array('business_profile_id', 'tax_rate_id', 'pay_period_from', 'pay_frequency', 'pay_period_to', 'payment_date', 'status', 'tax_amount', 'attachment', 'amount', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'business_profile_id' => 'required',
        'pay_period_from' => 'required',
        'pay_period_to' => 'required',
        'payment_date' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function employee()
    {
        return $this->belongsToMany('\App\Employee', 'business_payroll_employees', 'business_payroll_id', 'employee_id');
    }

    public function businessPayrollEmployee()
    {
        return $this->hasMany('\App\BusinessPayrollEmployee');
    }

    public function notes()
    {
        return $this->morphToMany('\App\Note', 'taggable');
    }

    public function businessPayrollItem()
    {
        return $this->hasMany('\App\BusinessPayrollItem');
    }

    public function taxRate()
    {
        return $this->belongsTo('\App\TaxRate');
    }
}