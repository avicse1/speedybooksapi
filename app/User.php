<?php namespace App;

use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nicolaslopezj\Searchable\SearchableTrait;
use Eloquence\Behaviours\CamelCasing;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, SearchableTrait, CamelCasing;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name' ,'email', 'phone', 'password', 'roles', 'country_id', 'state_id', 'city_id', 'status', 'business_profile_id', 'account_status', 'expiry_date'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    /**
     * Validation rules.
     *
     * @var array
     */
    public static $rules = array(
        'first_name'=>'required|alpha|min:2',
        'last_name'=>'required|alpha|min:2',
        'email'=>'required|email',
    );

	/**
	 * Passwords must always be hashed
	 * @param $password
	 */
	public function setPasswordAttribute($password)
	{
		$this->attributes['password'] = Hash::make($password);
	}

    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    public function scopeOfRoleId($query, $role_id)
    {
        return $query->where('role_id', '>=', $role_id);
    }

    public function scopeOfEmail($query, $email)
    {
        return $query->whereEmail($email);
    }

    public function scopeActive($query)
    {
        return $query->whereStatus("active");
    }

    public function country()
    {
        return $this->belongsTo('\App\Country');
    }

    public function state()
    {
        return $this->belongsTo('\App\State');
    }

    public function city()
    {
        return $this->belongsTo('\App\City');
    }

    public function businessProfileUser()
    {
        return $this->hasOne('\App\BusinessProfileUser');
    }

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function businessInvoice()
    {
        return $this->hasMany('\App\BusinessInvoice');
    }

    public function businessExpense()
    {
        return $this->hasMany('\App\BusinessExpense');
    }

    public function userPermission()
    {
        return $this->hasOne('\App\InviteUserPermission');
    }

    public function bankAccount(){
        return $this->hasMany('\App\BankAccount');
    }
}
