<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model {

    protected $table = 'countries';

    protected $fillable = array('name');

    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('\App\User');
    }

    public function state()
    {
        return $this->hasMany('\App\State');
    }

    public function businessProfile()
    {
        return $this->hasMany('\App\BusinessProfile');
    }
}