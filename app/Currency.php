<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model {

    protected $table = 'currencies';

    protected $fillable = array('name', 'code');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required',
        'code' => 'required'
    ];

    public function businessProfile()
    {
        return $this->hasMany('\App\BusinessProfile');
    }
}