<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessRefund extends Model {

    protected $table = 'business_refunds';

    protected $fillable = array('business_profile_id', 'invoice_no', 'invoice_date', 'refund_no', 'date', 'reference', 'status', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'business_profile_id' => 'required',
        'invoice_no' => 'required',
        'invoice_date' => 'required',
        'date' => 'required',
        'reference' => 'required',
    ];

    public function businessRefundItem()
    {
        return $this->hasMany('\App\BusinessRefundItem');
    }

    public function customer()
    {
        return $this->belongsToMany('\App\Customer', 'business_refund_customers', 'business_refund_id', 'customer_id');
    }

    public function businessRefundCustomer()
    {
        return $this->hasMany('\App\BusinessRefundCustomer');
    }

    public function notes()
    {
        return $this->morphToMany('\App\Note', 'taggable');
    }

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }
}