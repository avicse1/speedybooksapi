<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessExpenseConfiguration extends Model {

    protected $table = 'business_expense_configurations';

    protected $fillable = array('business_expense_id', 'schedule', 'custom_days', 'custom_dates', 'start_on', 'end_on', 'is_never_expires');

    public $timestamps = false;

    public static $rules = [
        'business_expense_id' => 'required',
        'schedule' => 'required',
        'custom_days' => 'required',
        'custom_dates' => 'required',
        'start_on' => 'required',
        'end_on' => 'required',
        'is_never_expires' => 'required',
    ];

    public function businessExpense()
    {
        return $this->belongsTo('\App\BusinessExpense');
    }
}