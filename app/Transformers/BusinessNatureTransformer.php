<?php

namespace App\Transformers;

use App\BusinessNature;
use Illuminate\Support\Facades\Bus;
use \League\Fractal\TransformerAbstract;

class BusinessNatureTransformer extends TransformerAbstract
{
    public function transform(BusinessNature $businessNature) {
        return [
            'id' => (int) $businessNature->id,
            'name' => $businessNature->name,
        ];
    }
}