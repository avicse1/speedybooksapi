<?php

namespace App\Transformers;

use App\FinancialYear;
use Illuminate\Support\Facades\Bus;
use \League\Fractal\TransformerAbstract;

class FinancialYearTransformer extends TransformerAbstract
{
    public function transform(FinancialYear $financialYear) {
        return [
            'id' => (int) $financialYear->id,
            'name' => $financialYear->name
        ];
    }
}