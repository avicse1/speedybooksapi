<?php

namespace App\Transformers;

use App\BusinessProfile;
use App\FinancialYear;
use App\Currency;
use Illuminate\Support\Facades\Bus;
use \League\Fractal\TransformerAbstract;
use App\Transformers\FinancialYearTransformer;

class BusinessProfileTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'financialYear',
        'currency',
        'businessNature',
        'country',
        'state'
    ];

    public function transform(BusinessProfile $businessProfile) {

        return [
            'id' => (int) $businessProfile->id,
            'financial_year_id' => $businessProfile->financial_year_id,
            'currency_id' => $businessProfile->currency_id,
            'business_nature_id' => $businessProfile->business_nature_id,
            'country_id' => $businessProfile->country_id,
            'state_id' => $businessProfile->state_id,
            'status' => $businessProfile->status,
            'is_skipped' => $businessProfile->is_skipped,
        ];
    }

    public function includeFinancialYear(BusinessProfile $businessProfile)
    {
        if(!empty($businessProfile->financialYear)) {
            return $this->item($businessProfile->financialYear, new FinancialYearTransformer());
        }
    }

    public function includeCurrency(BusinessProfile $businessProfile)
    {
        if(!empty($businessProfile->currency)) {
            return $this->item($businessProfile->currency, new CurrencyTransformer());
        }
    }

    public function includeBusinessNature(BusinessProfile $businessProfile)
    {
        if(!empty($businessProfile->businessNature)) {
            return $this->item($businessProfile->businessNature, new BusinessNatureTransformer());
        }
    }

    public function includeCountry(BusinessProfile $businessProfile)
    {
        if(!empty($businessProfile->country)) {
            return $this->item($businessProfile->country, new CountryTransformer());
        }
    }

    public function includeState(BusinessProfile $businessProfile)
    {
        if(!empty($businessProfile->state)) {
            return $this->item($businessProfile->state, new StateTransformer());
        }
    }
}