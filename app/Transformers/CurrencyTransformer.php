<?php

namespace App\Transformers;

use App\Currency;
use Illuminate\Support\Facades\Bus;
use \League\Fractal\TransformerAbstract;

class CurrencyTransformer extends TransformerAbstract
{
    public function transform(Currency $currency) {
        return [
            'id' => (int) $currency->id,
            'name' => $currency->name,
            'code' => $currency->code,
        ];
    }
}