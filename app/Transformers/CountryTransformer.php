<?php

namespace App\Transformers;

use App\Country;
use Illuminate\Support\Facades\Bus;
use \League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    public function transform(Country $country) {
        return [
            'id' => (int) $country->id,
            'name' => $country->name,
            'code' => $country->code,
        ];
    }
}