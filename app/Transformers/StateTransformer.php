<?php

namespace App\Transformers;

use App\State;
use Illuminate\Support\Facades\Bus;
use \League\Fractal\TransformerAbstract;

class StateTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'country'
    ];

    public function transform(State $state) {
        return [
            'id' => (int) $state->id,
            'name' => $state->name,
            'code' => $state->code,
            'priority' => $state->priority,
        ];
    }

    public function includeCountry(State $state)
    {
        if(!empty($state->country)) {
            return $this->item($state->country, new CountryTransformer());
        }
    }
}