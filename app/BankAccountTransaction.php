<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BankAccountTransaction extends Model {

    protected $table = 'bank_account_transactions';

    protected $fillable = array('bank_account_id', 'object_id', 'date', 'description', 'type', 'category', 'payee', 'amount', 'is_speedybooks', 'transaction_type');

    public $timestamps = false;

    public function bankAccount()
    {
        return $this->belongsTo('\App\BankAccount');
    }
}