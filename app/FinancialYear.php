<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FinancialYear extends Model {

    protected $table = 'financial_years';

    protected $fillable = array('name');

    public $timestamps = false;

    public static $rules = [
        'name' => 'required'
    ];

    public function businessProfile()
    {
        return $this->hasMany('\App\BusinessProfile');
    }
}