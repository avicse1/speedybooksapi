<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessExpenseVendor extends Model {

    protected $table = 'business_expense_vendors';

    protected $fillable = array('business_expense_id', 'vendor_id');

    public $timestamps = false;

    public function businessExpense()
    {
        return $this->belongsTo('\App\BusinessExpense', 'business_expense_id', 'id');
    }

    public function vendor()
    {
        return $this->belongsTo('\App\Vendor', 'vendor_id', 'id');
    }
}