<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessReimbursement extends Model {

    protected $table = 'business_reimbursements';

    protected $fillable = array('business_profile_id', 'customer_id', 'tax_rate_id', 'reimbursement_no', 'reference', 'date', 'description', 'status', 'discount', 'tax_amount', 'attachment', 'amount', 'created_by', 'updated_by', 'deleted_by');

    public static $rules = [
        'business_profile_id' => 'required',
        'customer_id' => 'required',
        'reimbursement_no' => 'required',
        'date' => 'required'
    ];

    public function businessReimbursementItem()
    {
        return $this->hasMany('\App\BusinessReimbursementItem');
    }

    public function customer()
    {
        return $this->belongsTo('\App\Customer');
    }

    public function notes()
    {
        return $this->morphToMany('\App\Note', 'taggable');
    }

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function taxRate()
    {
        return $this->belongsTo('\App\TaxRate');
    }
}