<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountBalance extends Model {

    protected $table = 'account_balances';

    protected $fillable = array('business_profile_id', 'account_id', 'balance', 'date', 'track_depreciation', 'original_cost', 'depreciation_cost',
        'depreciation_asof', 'depreciation_at'
        );

    public $timestamps = false;

    public static $rules = [
        'business_profile_id' => 'required',
        'account_id' => 'required',
        'date' => 'required',
    ];

    public function businessProfile()
    {
        return $this->belongsTo('\App\BusinessProfile');
    }

    public function account()
    {
        return $this->belongsTo('\App\Account');
    }
}