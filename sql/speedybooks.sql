/*
SQLyog Ultimate v9.63 
MySQL - 5.5.5-10.1.13-MariaDB 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

create table `sb_users` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` VARCHAR (255),
	`role` VARCHAR (50),
	`email` VARCHAR (255),
	`password` VARCHAR (255),
	`phone` VARCHAR (20),
	`remember_token` VARCHAR (255),
	`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;