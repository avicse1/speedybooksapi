ALTER TABLE `sb_users`
ADD `last_name` VARCHAR (100) NOT NULL AFTER `name`,
ADD `country_id` BIGINT(20) NOT NULL AFTER `phone`,
ADD `state_id` BIGINT(20) NOT NULL AFTER `country_id`,
ADD `city_id` BIGINT(20) NOT NULL AFTER `state_id`;

ALTER TABLE `sb_users`
CHANGE COLUMN `name` `first_name` VARCHAR(255) COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL ;

CREATE TABLE `sb_countries` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) DEFAULT NULL,
  `code` varchar(5)  DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_states` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` BIGINT(20)  NOT NULL,
  `name` VARCHAR(100) DEFAULT NULL,
  `code` varchar(2) DEFAULT NULL,
  `priority` tinyint(1) DEFAULT '0',
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_cities` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `state_id` BIGINT(20) NOT NULL,
  `name` VARCHAR(100) DEFAULT NULL,
  `is_popular` int(1) NOT NULL DEFAULT '0',
  `latitude` float(10,6) DEFAULT NULL,
  `longitude` float(10,6) DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_profiles` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` BIGINT(20) NOT NULL,
  `state_id` BIGINT(20) NOT NULL,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `financial_year` VARCHAR (100) NULL DEFAULT NULL,
  `currency` VARCHAR (100) NULL DEFAULT NULL,
  `business_nature` VARCHAR (100) NULL DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE','PENDING','DELETED') NOT NULL DEFAULT 'PENDING',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_users`
ADD `business_profile_id` BIGINT(20) NOT NULL AFTER `state_id`;

CREATE TABLE `sb_currencies` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `code` VARCHAR(100) NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `sb_business_profiles`
CHANGE `currency` `currency_id` BIGINT(20) NOT NULL DEFAULT '0';

CREATE TABLE `sb_business_invoices` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_no` VARCHAR (255) NOT NULL,
  `date` DATE NOT NULL,
  `reference` VARCHAR(255) NULL DEFAULT NULL,
  `due_date` DATE NOT NULL,
  `status` enum('ACTIVE','INACTIVE','PENDING','DELETED') NOT NULL DEFAULT 'PENDING',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_invoice_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_invoice_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR (255) NUll DEFAULT  NULL,
  `description` text NUll DEFAULT  NULL,
  `quantity` int(11) NUll DEFAULT  NULL,
  `price` DECIMAL (11,2) NUll DEFAULT  NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_customers` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` BIGINT(20) UNSIGNED NOT NULL,
  `state_id` BIGINT(20) UNSIGNED NOT NULL,
  `city_id` BIGINT(20) UNSIGNED NOT NUll,
  `first_name` VARCHAR (255) NUll DEFAULT  NULL,
  `last_name` VARCHAR (255) NUll DEFAULT  NULL,
  `email` VARCHAR (100) NUll DEFAULT  NULL,
  `phone` VARCHAR (20) NUll DEFAULT  NULL,
  `address` text NUll DEFAULT  NULL,
  `status` enum('ACTIVE','INACTIVE','PENDING','DELETED') NOT NULL DEFAULT 'PENDING',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_profiles`
ADD `user_id` BIGINT(20) UNSIGNED NOT NULL AFTER `state_id`;

/* 2016-09-17 */

CREATE TABLE `sb_business_natures` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_financial_years` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_profiles` DROP `business_nature` , DROP `financial_year`;

ALTER TABLE `sb_business_profiles`
 ADD `business_nature_id` BIGINT(20) UNSIGNED NOT NULL AFTER `state_id`,
 ADD `financial_year_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_nature_id`;

CREATE TABLE `sb_business_customers` (
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `customer_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR (255) NULL DEFAULT NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_invoices`
 ADD  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `id`;

ALTER TABLE `sb_business_invoice_items`
 ADD  `business_item_id` BIGINT(20) UNSIGNED NOT NULL AFTER `id`;

 CREATE TABLE `sb_business_expenses` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `customer_id` BIGINT(20) UNSIGNED NOT NULL,
  `payment_term` VARCHAR (255) NULL DEFAULT NULL,
  `reference` VARCHAR(255) NULL DEFAULT NULL,
  `date` DATE NOT NULL,
  `due_date` DATE NOT NULL,
  `notes` text NULL DEFAULT NULL,
  `discount` VARCHAR (50) NULL DEFAULT NULL,
  `tax` VARCHAR (50) NULL DEFAULT NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `status` enum('APPROVED','DISCARD','DRAFT') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_expense_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_expense_id` BIGINT(20) UNSIGNED NOT NULL,
  `business_item_id` BIGINT(20) UNSIGNED NOT NULL,
  `description` text NUll DEFAULT  NULL,
  `account`  VARCHAR (100) NUll DEFAULT  NULL,
  `quantity` int(11) NUll DEFAULT  NULL,
  `discount` VARCHAR (50) NUll DEFAULT  NULL,
  `price` DECIMAL (11,2) NUll DEFAULT  NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_users`
 ADD  `status` enum('ACTIVE','INACTIVE','PENDING','DELETED') NOT NULL DEFAULT 'PENDING' AFTER `city_id`;

ALTER TABLE `sb_business_profiles`
ADD `is_skipped` tinyint(0) NOT NULL AFTER `status`;


/* 2016-09-27 */

ALTER TABLE `sb_business_invoices`
 ADD  `payment_term` VARCHAR (255) NULL DEFAULT NULL AFTER `status`,
 ADD  `amount` DECIMAL (11,2) NUll DEFAULT  NULL AFTER `payment_term`;

CREATE TABLE `sb_notes` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title`  VARCHAR (100) NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_taggables` (
  `note_id` bigint(20) NOT NULL ,
  `taggable_id` bigint(20) NOT NULL ,
  `taggable_type` VARCHAR(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


/* 2016-09-28 */

CREATE TABLE `sb_accounts` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name`  VARCHAR (100) NUll DEFAULT  NULL,
  `description`  VARCHAR (100) NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_invoice_items`
ADD  `account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_invoice_id`;

ALTER TABLE `sb_business_expense_items`
CHANGE COLUMN `account` `account_id` BIGINT(20) UNSIGNED NOT NULL;

CREATE TABLE `sb_business_invoice_customers` (
  `business_invoice_id` BIGINT(20) UNSIGNED NOT NULL,
  `customer_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_vendors` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` BIGINT(20) UNSIGNED NOT NULL,
  `state_id` BIGINT(20) UNSIGNED NOT NULL,
  `city_id` BIGINT(20) UNSIGNED NOT NUll,
  `first_name` VARCHAR (255) NUll DEFAULT  NULL,
  `last_name` VARCHAR (255) NUll DEFAULT  NULL,
  `email` VARCHAR (100) NUll DEFAULT  NULL,
  `phone` VARCHAR (20) NUll DEFAULT  NULL,
  `address` text NUll DEFAULT  NULL,
  `status` enum('ACTIVE','INACTIVE','PENDING','DELETED') NOT NULL DEFAULT 'PENDING',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_vendors` (
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `vendor_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_expense_vendors` (
  `business_expense_id` BIGINT(20) UNSIGNED NOT NULL,
  `vendor_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER  TABLE `sb_business_expenses` DROP `customer_id`;

ALTER TABLE `sb_business_invoices`
 CHANGE  `status` `status` enum('APPROVED','DISCARD','DRAFT','SEND EMAIL') NOT NULL DEFAULT 'DRAFT';

 ALTER TABLE `sb_business_expenses`
  CHANGE  `status` `status` enum('APPROVED','DISCARD','DRAFT','SEND EMAIL') NOT NULL DEFAULT 'DRAFT';

ALTER TABLE `sb_business_invoices`
 ADD `discount` VARCHAR (50) NULL DEFAULT NULL AFTER `amount`,
 ADD `tax` VARCHAR (50) NULL DEFAULT NULL AFTER `discount`;

 ALTER TABLE `sb_business_invoice_items`
ADD  `item_code` VARCHAR (100) NULL DEFAULT NULL AFTER `account_id`;

ALTER TABLE `sb_business_expense_items`
ADD  `item_code` VARCHAR (100) NULL DEFAULT NULL AFTER `account_id`;

/* 2016-10-08 */

 CREATE TABLE `sb_business_refunds` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `invoice_no` VARCHAR (255) NULL DEFAULT NULL,
  `invoice_date` DATE NULL DEFAULT NULL,
  `refund_no` VARCHAR (255) NULL DEFAULT NULL,
  `reference` VARCHAR (255) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `status` enum('APPROVED','DISCARD','DRAFT', 'SEND EMAIL') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_refund_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_refund_id` BIGINT(20) UNSIGNED NOT NULL,
  `business_item_id` BIGINT(20) UNSIGNED NOT NULL,
  `item_code`  VARCHAR (100) NUll DEFAULT  NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `quantity` int(11) NUll DEFAULT  NULL,
  `discount` VARCHAR (50) NUll DEFAULT  NULL,
  `price` DECIMAL (11,2) NUll DEFAULT  NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `description` text NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_refund_customers` (
  `business_refund_id` BIGINT(20) UNSIGNED NOT NULL,
  `customer_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

/* 2016-10-10 */

ALTER TABLE `sb_business_invoices`
 ADD `type` enum('RECURRING', 'ESTIMATES', 'REFUND', 'INVOICE') NOT NULL DEFAULT 'INVOICE'  AFTER `discount`;

CREATE TABLE `sb_business_invoice_configurations` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_invoice_id` BIGINT(20) UNSIGNED NOT NULL,
  `schedule` INT(11) NULL DEFAULT NULL,
  `custom_days` INT(11) NULL DEFAULT NULL,
  `custom_dates` VARCHAR(50) NULL DEFAULT NULL,
  `start_on` DATE NULL DEFAULT NULL,
  `end_on` DATE NULL DEFAULT NULL,
  `is_never_expires` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

/* 2016-10-13 */

ALTER TABLE `sb_business_items`
ADD `account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`,
ADD `vendor_id` BIGINT(20) UNSIGNED NOT NULL AFTER `account_id`,
ADD `item_code` VARCHAR (100) NULL DEFAULT NULL AFTER `name`,
ADD `description` text NULL DEFAULT NULL AFTER `item_code`,
ADD `unit` VARCHAR (100) NULL DEFAULT NULL AFTER  `description`,
ADD `selling_price` VARCHAR (100) NULL DEFAULT NULL AFTER  `unit`;

/* 2016-10-14 */

CREATE TABLE `sb_business_purchase_orders` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `vendor_id` BIGINT(20) UNSIGNED NOT NULL,
  `purchase_order_no` VARCHAR (100) NULL DEFAULT NULL,
  `reference` VARCHAR (255) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `delivery_date` DATE NULL DEFAULT NULL,
  `discount` VARCHAR (50) NULL DEFAULT NULL,
  `tax` VARCHAR (50) NULL DEFAULT NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `status` enum('APPROVED','DISCARD','DRAFT', 'SEND EMAIL') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_purchase_order_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_purchase_order_id` BIGINT(20) UNSIGNED NOT NULL,
  `business_item_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `item_code`  VARCHAR (100) NUll DEFAULT  NULL,
  `quantity` int(11) NUll DEFAULT  NULL,
  `discount` VARCHAR (50) NUll DEFAULT  NULL,
  `price` DECIMAL (11,2) NUll DEFAULT  NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `description` text NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;


/* 2016-10-17 */

CREATE TABLE `sb_business_reimbursements` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `vendor_id` BIGINT(20) UNSIGNED NOT NULL,
  `reimbursement_no` VARCHAR (100) NULL DEFAULT NULL,
  `reference` VARCHAR (255) NULL DEFAULT NULL,
  `date` DATE NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  `status` enum('APPROVED','DISCARD','DRAFT', 'SEND EMAIL') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_reimbursement_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_reimbursement_id` BIGINT(20) UNSIGNED NOT NULL,
  `business_item_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `item_code`  VARCHAR (100) NUll DEFAULT  NULL,
  `quantity` int(11) NUll DEFAULT  NULL,
  `discount` VARCHAR (50) NUll DEFAULT  NULL,
  `price` DECIMAL (11,2) NUll DEFAULT  NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `description` text NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;


/* 2016-10-19 */

CREATE TABLE `sb_employees` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `employement_no` VARCHAR (100) NULL DEFAULT NULL,
  `name` VARCHAR (100) NULL DEFAULT NULL,
  `date_of_birth` DATE NULL DEFAULT NULL,
  `type`  enum('PERMANENT', 'PART TIME', 'CONTRACT')  NOT NULL DEFAULT 'PERMANENT',
  `date_of_joining` DATE NULL DEFAULT NULL,
  `blood_group` VARCHAR (50) NULL DEFAULT NULL,
  `gender`  enum('MALE', 'FEMALE', 'OTHERS')  NOT NULL DEFAULT 'MALE',
  `marital_status`  enum('MARRIED', 'SINGLE')  NOT NULL DEFAULT 'SINGLE',
  `address` VARCHAR (255) NULL DEFAULT NULL,
  `landline_no` VARCHAR (20) NULL DEFAULT NULL,
  `mobile_no` VARCHAR (20) NULL DEFAULT NULL,
  `email` VARCHAR (100) NULL DEFAULT NULL,
  `contact_name` VARCHAR (100) NULL DEFAULT NULL,
  `contact_mobile_no` VARCHAR (20) NULL DEFAULT NULL,
  `contact_relation` VARCHAR (255) NULL DEFAULT NULL,
  `status`  enum('ACTIVE','INACTIVE','PENDING','DELETED')  NOT NULL DEFAULT 'PENDING',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_employees` (
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `employee_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_pay_item_types` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR (255) NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_payrolls` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `pay_period_from` DATE NULL DEFAULT NULL,
  `pay_period_to` DATE NULL DEFAULT NULL,
  `payment_date` DATE NULL DEFAULT NULL,
  `status` enum('APPROVED','DISCARD','DRAFT', 'SEND EMAIL') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_payroll_employees` (
  `business_payroll_id` BIGINT(20) UNSIGNED NOT NULL,
  `employee_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_payroll_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_payroll_id` BIGINT(20) UNSIGNED NOT NULL,
  `business_pay_item_type_id` BIGINT(20) UNSIGNED NOT NULL,
  `item` VARCHAR (100) NULL DEFAULT NULL,
  `quantity` int(11) NUll DEFAULT  NULL,
  `price` DECIMAL (11,2) NUll DEFAULT  NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `description` text NUll DEFAULT  NULL,
 PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

/* 2016-11-08 */

ALTER TABLE `sb_business_expenses`
 ADD `type` enum('RECURRING', 'EXPENSE') NOT NULL DEFAULT 'EXPENSE'  AFTER `discount`;

CREATE TABLE `sb_business_expense_configurations` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_expense_id` BIGINT(20) UNSIGNED NOT NULL,
  `schedule` INT(11) NULL DEFAULT NULL,
  `custom_days` INT(11) NULL DEFAULT NULL,
  `custom_dates` VARCHAR(50) NULL DEFAULT NULL,
  `start_on` DATE NULL DEFAULT NULL,
  `end_on` DATE NULL DEFAULT NULL,
  `is_never_expires` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

/* 2016-11-12 */
ALTER TABLE `sb_business_payrolls`
 ADD `pay_frequency` VARCHAR (100) NULL DEFAULT NULL  AFTER `pay_period_from`,
 ADD `tax` VARCHAR (50) NULL DEFAULT NULL  AFTER `status`,
 ADD `amount` DECIMAL (11,2) NUll DEFAULT  NULL  AFTER `tax`;

CREATE TABLE `sb_business_pay_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_pay_item_type_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR (255) NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_payroll_items`
CHANGE `business_pay_item_type_id` `business_pay_item_id` BIGINT(20) UNSIGNED NOT NULL;

/* 2016-11-16 */
ALTER TABLE `sb_business_reimbursements`
CHANGE `vendor_id` `customer_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `sb_business_reimbursements`
 ADD `discount` VARCHAR (50) NULL DEFAULT NULL AFTER `status`,
 ADD `tax` VARCHAR (50) NULL DEFAULT NULL AFTER `discount`,
 ADD `amount` DECIMAL (11,2) NULL DEFAULT NULL AFTER `tax`;

ALTER TABLE `sb_business_pay_items`
ADD `account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_pay_item_type_id`;

/* 2016-11-22 */

CREATE TABLE `sb_main_accounts` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR (255) NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_sub_accounts` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `main_account_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR (255) NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_accounts`
 ADD `sub_account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `id`,
 ADD `code` VARCHAR (20) NULL DEFAULT NULL AFTER `sub_account_id`;

/* 2016-11-27 */
ALTER TABLE `sb_business_invoice_items`
 ADD  `discount` VARCHAR (50) NULL DEFAULT NULL AFTER `price`;

ALTER TABLE `sb_business_invoices`
 ADD  `description` VARCHAR (50) NULL DEFAULT NULL AFTER `status`;

 /* 2016-12-28 */
ALTER TABLE `sb_business_profiles`
  ADD `logo` VARCHAR(255) NULL DEFAULT NULL AFTER `is_skipped`,
  ADD `address` text NULL DEFAULT NULL AFTER `logo`;

ALTER TABLE `sb_business_invoice_configurations`
CHANGE `schedule` `schedule` varchar(50) NULL AFTER `business_invoice_id`;

/* 2017-01-05 */
ALTER TABLE `sb_notes`
CHANGE `title` `title` text NULL AFTER `id`;

/* 2017-01-09 */
ALTER TABLE `sb_accounts`
 ADD `parent_id` BIGINT(20) UNSIGNED NOT NULL AFTER `id`;

/*2017-01-17*/
ALTER TABLE `sb_customers`
 ADD `business_profile_id` BIGINT(20) NOT NULL AFTER `id`;

ALTER TABLE `sb_vendors`
 ADD `business_profile_id` BIGINT(20) NOT NULL AFTER `id`;

/* 2017-01-27 */
 ALTER TABLE `sb_employees`
 ADD `is_terminated` tinyint(1) DEFAULT '0' AFTER `status`,
 ADD `last_working_day` date DEFAULT NUll AFTER `is_terminated`,
 ADD `description` text DEFAULT NUll AFTER `last_working_day`;

/* 2017-02-03 */
CREATE TABLE `sb_bank_accounts` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR (255) NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_invoices`
 ADD  `bank_account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`;

/*2017-02-08*/

ALTER TABLE `sb_business_profiles`
  ADD `industry` VARCHAR (255) DEFAULT NULL AFTER `status`,
  ADD `registration_no` VARCHAR (255) DEFAULT NULL AFTER `industry`,
  ADD `description` TEXT DEFAULT NULL AFTER `registration_no`,
  ADD `telephone_no` VARCHAR (255) DEFAULT NULL AFTER `description`,
  ADD `email` VARCHAR (255) DEFAULT NULL AFTER `telephone_no`,
  ADD `website` VARCHAR (255) DEFAULT NULL AFTER `email`,
  ADD `physical_address` Text DEFAULT NULL AFTER `status`,
  ADD `registered_address` Text DEFAULT NULL AFTER `physical_address`,

ALTER TABLE `sb_business_profiles` DROP `address`;

ALTER TABLE `sb_business_invoices`
  CHANGE `bank_account_id` `account_id` bigint(20) unsigned NOT NULL AFTER `business_profile_id`;


/* 2017-02-10 */

CREATE TABLE `sb_invite_user_permissions`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT(20) UNSIGNED NOT NULL,
  `first_name` VARCHAR (100) NULL DEFAULT NULL,
  `last_name` VARCHAR (100) NULL DEFAULT NULL,
  `email` VARCHAR (100) NULL DEFAULT NULL,
  `permissions` enum('Read Only','Basic','Intermediate','Advance') NOT NULL DEFAULT 'Read Only',
  `invoice` VARCHAR (100) NULL DEFAULT NULL,
  `expense` VARCHAR (100) NULL DEFAULT NULL,
  `setting` VARCHAR (100) NULL DEFAULT NULL,
  `reports` VARCHAR (100) NULL DEFAULT NULL,
  `payroll` VARCHAR (100) NULL DEFAULT NULL,
  `manage_users` VARCHAR (100) NULL DEFAULT NULL,
  `banking` VARCHAR (100) NULL DEFAULT NULL,
  `invite_by` BIGINT(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_invite_user_permissions`
 ADD   `status`  enum('ACTIVE','INACTIVE','PENDING','DELETED')  NOT NULL DEFAULT 'PENDING' AFTER `invite_by`;

 /* 2017-02-15 */
 CREATE TABLE `sb_business_profile_users`(
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `user_id` BIGINT(20) UNSIGNED NOT NULL
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

INSERT INTO `sb_business_profile_users` (`business_profile_id`, `user_id`)
select `id`, `user_id` from `sb_business_profiles`

ALTER TABLE `sb_business_profiles` DROP `user_id`;

ALTER TABLE `sb_invite_user_permissions`
 ADD `expiry_date` DATETIME  NULL DEFAULT NULL AFTER `status`,
 ADD `expired` tinyint(1) DEFAULT '0' AFTER `expiry_date`;


/* 2017-02-17 */
 ALTER TABLE `sb_bank_accounts`
  ADD `business_profile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `id`,
  ADD `user_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`,
  ADD `account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `user_id`,
  ADD `upload` VARCHAR(255) NULL DEFAULT NULL AFTER `account_id`,
  ADD `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `description`,
  ADD `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`,
  ADD `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_at`;

 CREATE TABLE `sb_bank_account_transactions`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bank_account_id` BIGINT(20) UNSIGNED NOT NULL,
  `date` DATE NULL DEFAULT NULL,
  `description` text NULL DEFAULT NULL,
  `type` VARCHAR (100) NULL DEFAULT NULL,
  `payee` VARCHAR (100) NULL DEFAULT NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_bank_accounts`
  ADD `speedybooks_balance`  DECIMAL (11,2) NUll DEFAULT  NULL AFTER `description`;

   ALTER TABLE `sb_bank_account_transactions`
  ADD `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `amount`,
  ADD `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`,
  ADD `deleted_at` timestamp NULL DEFAULT NULL AFTER `updated_at`;

  /* 2017-02-21 */
 CREATE TABLE `sb_tax_rates`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR (100) NULL DEFAULT NULL,
  `tax_rates` DECIMAL (11,2) NUll DEFAULT  NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `sb_business_invoices`
 ADD  `tax_rate_id` BIGINT(20) UNSIGNED NOT NULL AFTER `account_id`;

ALTER TABLE `sb_business_invoices`
CHANGE `tax` `tax_amount` decimal(11,2) NULL AFTER `type`;

ALTER TABLE `sb_business_expenses`
 ADD  `tax_rate_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`;

ALTER TABLE `sb_business_expenses`
CHANGE `tax` `tax_amount` decimal(11,2) NULL AFTER `type`;

ALTER TABLE `sb_business_purchase_orders`
 ADD  `tax_rate_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`;

ALTER TABLE `sb_business_purchase_orders`
CHANGE `tax` `tax_amount` decimal(11,2) NULL AFTER `discount`;

ALTER TABLE `sb_tax_rates`
 ADD  `component` VARCHAR (100) NULL DEFAULT NULL AFTER `name`;

 ALTER TABLE `sb_business_reimbursements`
 ADD  `tax_rate_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`;

ALTER TABLE `sb_business_reimbursements`
CHANGE `tax` `tax_amount` decimal(11,2) NULL AFTER `discount`;

 ALTER TABLE `sb_business_payrolls`
 ADD  `tax_rate_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`;

ALTER TABLE `sb_business_payrolls`
CHANGE `tax` `tax_amount` decimal(11,2) NULL AFTER `status`;

/* 2017-02-22 */

ALTER TABLE `sb_business_profiles`
ADD `company_id` VARCHAR(20) NULL DEFAULT NULL  AFTER `state_id`;

ALTER TABLE `sb_users`
ADD `account_status` enum('FREE', 'PAID') NOT NULL DEFAULT 'FREE'  AFTER `status`,
ADD `expiry_date` DATETIME  NULL DEFAULT NULL AFTER `account_status`;


ALTER TABLE `sb_customers` DROP `country_id` , DROP `state_id`, DROP `city_id`, DROP `address`;

ALTER TABLE `sb_customers`
ADD `title` VARCHAR(100) NULL DEFAULT NULL  AFTER `business_profile_id`,
ADD `mobile` VARCHAR(20) NULL DEFAULT NULL  AFTER `phone`,
ADD `company_name` VARCHAR(100) NULL DEFAULT NULL  AFTER `mobile`,
ADD `website` VARCHAR(255) NULL DEFAULT NULL  AFTER `company_name`,
ADD `billing_street` VARCHAR(100) NULL DEFAULT NULL  AFTER `website`,
ADD `billing_zip` VARCHAR(20) NULL DEFAULT NULL  AFTER `billing_street`,
ADD `billing_city` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_zip`,
ADD `billing_state` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_city`,
ADD `billing_country` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_state`,
ADD `shipping_street` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_country`,
ADD `shipping_zip` VARCHAR(20) NULL DEFAULT NULL  AFTER `shipping_street`,
ADD `shipping_city` VARCHAR(100) NULL DEFAULT NULL  AFTER `shipping_zip`,
ADD `shipping_state` VARCHAR(100) NULL DEFAULT NULL  AFTER `shipping_city`,
ADD `shipping_country` VARCHAR(100) NULL DEFAULT NULL  AFTER `shipping_state`,
ADD `notes` text NULL DEFAULT NULL  AFTER `shipping_country`,
ADD `attachment` text NULL DEFAULT NULL  AFTER `notes`;

ALTER TABLE `sb_vendors` DROP `country_id` , DROP `state_id`, DROP `city_id`, DROP `address`;

ALTER TABLE `sb_vendors`
ADD `title` VARCHAR(100) NULL DEFAULT NULL  AFTER `business_profile_id`,
ADD `mobile` VARCHAR(20) NULL DEFAULT NULL  AFTER `phone`,
ADD `company_name` VARCHAR(100) NULL DEFAULT NULL  AFTER `mobile`,
ADD `website` VARCHAR(255) NULL DEFAULT NULL  AFTER `company_name`,
ADD `suppliers` VARCHAR(255) NULL DEFAULT NULL  AFTER `website`,
ADD `billing_street` VARCHAR(100) NULL DEFAULT NULL  AFTER `website`,
ADD `billing_zip` VARCHAR(20) NULL DEFAULT NULL  AFTER `billing_street`,
ADD `billing_city` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_zip`,
ADD `billing_state` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_city`,
ADD `billing_country` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_state`,
ADD `shipping_street` VARCHAR(100) NULL DEFAULT NULL  AFTER `billing_country`,
ADD `shipping_zip` VARCHAR(20) NULL DEFAULT NULL  AFTER `shipping_street`,
ADD `shipping_city` VARCHAR(100) NULL DEFAULT NULL  AFTER `shipping_zip`,
ADD `shipping_state` VARCHAR(100) NULL DEFAULT NULL  AFTER `shipping_city`,
ADD `shipping_country` VARCHAR(100) NULL DEFAULT NULL  AFTER `shipping_state`,
ADD `notes` text NULL DEFAULT NULL  AFTER `shipping_country`,
ADD `attachment` text NULL DEFAULT NULL  AFTER `notes`;

/* 2017-02-23 */

ALTER TABLE `sb_invite_user_permissions`
ADD `cpa_permission` tinyint(1) DEFAULT '0' AFTER  `banking`,
ADD `invited_by_cpa` tinyint(1) DEFAULT '0' AFTER  `cpa_permission`;

ALTER TABLE `sb_users`
CHANGE `role` `role` enum('USERS','CPA','MANAGE') NOT NULL DEFAULT 'USERS';

/* 2017-03-01 */
ALTER TABLE `sb_business_invoices`
ADD `attachment` VARCHAR (255) NULL  DEFAULT NULL AFTER `tax_amount`;

ALTER TABLE `sb_business_expenses`
ADD `attachment` VARCHAR (255) NULL  DEFAULT NULL AFTER `tax_amount`;

ALTER TABLE `sb_business_purchase_orders`
ADD `attachment` VARCHAR (255) NULL  DEFAULT NULL AFTER `tax_amount`;

ALTER TABLE `sb_business_reimbursements`
ADD `attachment` VARCHAR (255) NULL  DEFAULT NULL AFTER `tax_amount`;

ALTER TABLE `sb_business_payrolls`
ADD `attachment` VARCHAR (255) NULL  DEFAULT NULL AFTER `tax_amount`;

ALTER TABLE `sb_bank_account_transactions`
  ADD `is_speedybooks` tinyint(0) NOT NULL AFTER `amount`;

ALTER TABLE `sb_invite_user_permissions`
ADD `business_profile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `id`;

ALTER TABLE `sb_bank_account_transactions`
  ADD `category`  VARCHAR(255) NUll DEFAULT  NULL AFTER `type`;


/* 2017-03-10 */

DROP TABLE `sb_main_accounts`, `sb_sub_accounts`;

ALTER TABLE `sb_accounts` DROP `sub_account_id`;

ALTER TABLE `sb_accounts`
  ADD `balance`  DECIMAL (11,2) NUll DEFAULT NULL AFTER `description`;

/* 2017-03-16 */

ALTER TABLE `sb_business_expenses`
 ADD  `account_id` BIGINT(20) UNSIGNED NOT NULL AFTER `business_profile_id`;

 /* 2017-03-28 */

 ALTER TABLE `sb_business_expenses`
 ADD  `expense_no` VARCHAR (255)  NOT NULL AFTER `tax_rate_id`;

 /* 2017-04-01 */

ALTER TABLE `sb_business_invoices`
 ADD  `banking_type` tinyint(1) DEFAULT '0' AFTER `status`;

ALTER TABLE `sb_business_expenses`
 ADD  `banking_type` tinyint(1) DEFAULT '0' AFTER `status`;

ALTER TABLE `sb_bank_account_transactions`
 ADD  `object_id` BIGINT(20) UNSIGNED NOT NULL AFTER `bank_account_id`,
 ADD  `transaction_type` enum('INVOICE', 'EXPENSE', 'JOURNAL') NULL DEFAULT NULL  AFTER `bank_account_id`;


 /* 2017-04-02 */
CREATE TABLE `sb_business_journals`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `name` VARCHAR (100) NULL DEFAULT NULL,
  `date` DATE NOT NULL,
  `status` enum('APPROVED','DISCARD','DRAFT','SEND EMAIL') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `sb_business_journal_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_journal_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `description` text NUll DEFAULT  NULL,
  `debit` DECIMAL (11,2) NUll DEFAULT  NULL,
  `credit` DECIMAL (11,2) NUll DEFAULT  NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_journals`
  ADD `total_debit`  DECIMAL (11,2) NUll DEFAULT NULL AFTER `status`,
  ADD `total_credit`  DECIMAL (11,2) NUll DEFAULT NULL AFTER `total_debit`;

ALTER TABLE `sb_business_journals`
 ADD  `banking_type` tinyint(1) DEFAULT '0' AFTER `total_credit`;

/*2017-04-04*/
CREATE TABLE `sb_business_cheques`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `vendor_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `cheque_no` VARCHAR (100) NULL DEFAULT NULL,
  `payment_term` VARCHAR (100) NULL DEFAULT NULL,
  `date` DATE NOT NULL,
  `description` text NULL DEFAULT NULL,
  `amount` DECIMAL (11,2) NUll DEFAULT  NULL,
  `status` enum('APPROVED','DISCARD','DRAFT','SEND EMAIL') NOT NULL DEFAULT 'DRAFT',
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

/* 2017-04-06 */
CREATE TABLE `sb_business_reconciles`(
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `ending_date` DATE NOT NULL,
  `reconcile_on` DATE NOT NULL,
  `ending_balance` DECIMAL (11,2) NUll DEFAULT  NULL,
  `auto_adjustment` DECIMAL (11,2) NUll DEFAULT  NULL,
  `created_by` INT(11) DEFAULT NULL,
  `updated_by` INT(11) DEFAULT NULL,
  `deleted_by` INT(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `sb_business_journal_items`
 ADD  `reconcile_type` tinyint(1) DEFAULT '0' AFTER `credit`;

 ALTER TABLE `sb_business_expenses`
 ADD  `reconcile_type` tinyint(1) DEFAULT '0' AFTER `banking_type`;

ALTER TABLE `sb_business_invoices`
 ADD  `reconcile_type` tinyint(1) DEFAULT '0' AFTER `banking_type`;

 ALTER TABLE `sb_business_cheques`
 ADD  `reconcile_type` tinyint(1) DEFAULT '0' AFTER `status`;

/* 2017-04-07 */
 CREATE TABLE `sb_account_balances` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_profile_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  `balance`  DECIMAL (11,2) NUll DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_accounts` DROP `balance`;

 ALTER TABLE `sb_accounts`
 ADD  `detail_type` VARCHAR (100) NULL DEFAULT NULL AFTER `description`;


/* 2017-04-08 */

ALTER TABLE `sb_business_reconciles`
 ADD  `beginning_balance` VARCHAR (100) NULL DEFAULT NULL AFTER `reconcile_on`,
 ADD  `cheque_payments` VARCHAR (100) NULL DEFAULT NULL AFTER `ending_balance`,
 ADD  `deposite_payments` VARCHAR (100) NULL DEFAULT NULL AFTER `cheque_payments`;

 ALTER TABLE `sb_business_journal_items`
 ADD  `business_reconcile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `account_id`;

 ALTER TABLE `sb_business_expenses`
 ADD  `business_reconcile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `tax_rate_id`;

ALTER TABLE `sb_business_invoices`
 ADD  `business_reconcile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `tax_rate_id`;

 ALTER TABLE `sb_business_cheques`
 ADD  `business_reconcile_id` BIGINT(20) UNSIGNED NOT NULL AFTER `account_id`;

/* 2017-04-09 */
 ALTER TABLE `sb_business_invoices`
ADD `payment_method`  enum('CHEQUE', 'CASH', 'CREDIT CARD') NULL DEFAULT NULL AFTER `payment_term`,
ADD `payment_method_reference`  VARCHAR (255) NULL DEFAULT NULL AFTER `payment_method`;

 ALTER TABLE `sb_account_balances`
 ADD  `track_depreciation` tinyint(1) DEFAULT '0' AFTER `balance`,
 ADD  `original_cost` DECIMAL (11,2) NULL DEFAULT NULL AFTER `track_depreciation`,
 ADD  `depreciation_cost` DECIMAL (11,2) NULL DEFAULT NULL AFTER `original_cost`,
 ADD  `depreciation_asof` DATE NULL DEFAULT NULL AFTER `depreciation_cost`,
 ADD  `depreciation_at`  enum('MONTHLY', 'HALF YEARLY', 'YEARLY') NULL DEFAULT NULL AFTER `depreciation_asof`;

/*2017-04-10*/
 CREATE TABLE `sb_business_cheque_items` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `business_cheque_id` BIGINT(20) UNSIGNED NOT NULL,
  `account_id` BIGINT(20) UNSIGNED NOT NULL,
  `description` text NULL DEFAULT NULL,
  `amount`  DECIMAL (11,2) NUll DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `sb_business_invoices`
CHANGE `payment_method` `payment_method` enum('CHEQUE','CASH','CREDIT CARD','BANK TRANSFER') COLLATE 'utf8mb4_unicode_ci' NULL AFTER `payment_term`;

 ALTER TABLE `sb_business_invoices`
ADD `credit_card_no`  VARCHAR (20) NULL DEFAULT NULL AFTER `payment_method_reference`;

