@extends('index')

@section('content')
<?php 
    if($businessInvoice->type == "INVOICE")
    {
        $title = "Invoice";
        $due_date ="Due Date";
        $type = "Invoice";
    }
    if($businessInvoice->type == "ESTIMATES")
    {
        $title = "Estimate";
        $due_date ="Expiration Date";
        $type = "Estimate";
    }
    if($businessInvoice->type == "RECURRING")
    {
        $title = "Sales Recurring";
        $due_date ="Due Date";
        $type = "Invoice";
    }
    if($businessInvoice->type == "REFUND")
    {
        $title = "Refund";
        $due_date = "";
        $type = "Refund";
    }

 ?>
    <div class="container">
        <h2 class="view-title">{{$title}}</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <span for="toUser">To : <strong>{{ $businessInvoice->customer[0]->first_name }} {{ $businessInvoice->customer[0]->last_name }}</strong></span>
                        </div>
                    </div>
                </div>
                <?php 
                    if($businessInvoice->type!="REFUND")
                    {
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <span class="control-label" for="InvoiceNo">{{$due_date}} : <strong>{{ $businessInvoice->due_date }}</strong></span>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>

            <div class="col-md-6 invoice-right-side">
                <div class="row">
                    <div class="col-sm-offset-6 col-md-6">
                        <div class="form-group">
                            <span class="control-label" for="date">{{$type}} Date : <strong>{{ $businessInvoice->date }}</strong></span>
                        </div>
                    </div>
                    <div class="col-sm-offset-6 col-md-6">
                        <div class="form-group">
                            <span class="control-label" for="InvoiceNo">{{$type}} No : <strong>{{ $businessInvoice->invoice_no }}</strong></span>
                        </div>
                    </div>
                    <div class="col-sm-offset-6 col-md-6">
                        <div class="form-group">
                            <span class="control-label" for="reference">Reference : <strong>{{ $businessInvoice->reference }}</strong></span><br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="view-title">Item/Descreption</th>
                        <th class="view-title">Quantity</th>
                        <th class="view-title">Price</th>
                        <th class="view-title">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $amount = 0; ?>
                    @if(!empty($businessInvoice->businessInvoiceItem))
                        @foreach($businessInvoice->businessInvoiceItem as $item)
                            <tr>
                                <td>{{ $item->item_name }}/{{ $item->description}}</td>
                                <td>{{ $item->quantity }}</td>
                                <td>{{ $item->price }}</td>
                                <td>{{ $item->amount }}</td>
                                <?php $amount = $amount + $item->amount; ?>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row totalAmount">
            <div class="col-sm-offset-6 col-md-6">
                <div class="form-group">
                    <span for="subTotal">Sub Total : <strong>{{ $amount }}</strong></span>
                </div>
            </div>
        </div>

        <div class="row totalAmount">
            <div class="col-sm-offset-6 col-md-6">
                <div class="form-group">
                    <span for="totalTax">Tax : <strong>{{ $businessInvoice->tax }}</strong></span>
                </div>
            </div>
        </div>

        <div class="row totalAmount">
            <div class="col-sm-offset-6 col-md-6">
                <div class="form-group">
                    <span for="grandTotal">Grand Total : <strong>{{ $businessInvoice->amount }}</strong></span>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <div class="row totalAmount">
            <div class="col-sm-offset-6 col-md-6">
                <div class="form-group">
                    <span for="totalTax pull-right">SpeedyBooks Inc. 2017</span>
                </div>
            </div>
        </div>
    </div>
@stop