<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet" href="{{ asset('css/stylesheet.css')  }}"/>
    <title>Payroll</title>
</head>
<body>
<div class="wrap">
    <header>
        <div class="top-left"><a href="#"><img src="{{ asset('images/logo-g.png')  }}" alt=""/></a></div>
        <div class="top-right"><ul><li class="help"><a href="#">Help</a></li><li><a href="#">Sign In</a></li></ul></div>
    </header>

    <div class="clear"></div>
    <div class="backside"><h1>{{ $businessPayroll->businessProfile->name  }}</h1>

    </div>
    <div class="wrapper">
        <header>
            <h1>Payroll</h1>
        </header>
        <article>
            <address>
                <p>To: {{ !empty($businessPayroll->employee) ?  $businessPayroll->employee[0]->name : '' }}<br></p>
                <p class="gap">{{ !empty($businessPayroll->employee) ?  $businessPayroll->employee[0]->address : '' }}</p>

            </address>
            <table class="meta table1">
                <tr>
                    <th class="background"><span contenteditable>Payment Date</span></th>
                    <td><span contenteditable>{{ $businessPayroll->payment_date  }}</span></td>
                </tr>
                <tr>
                    <th class="background"><span contenteditable>Pay Period From</span></th>
                    <td><span contenteditable>{{ $businessPayroll->pay_period_from  }}</span></td>
                </tr>
                <tr>
                    <th class="background"><span contenteditable>Pay Period To</span></th>
                    <td><span contenteditable>{{ $businessPayroll->pay_period_to  }}</span></td>
                </tr>
                <tr>
                    <th class="background"><span contenteditable>Pay Frequency</span></th>
                    <td><span contenteditable>{{  $businessPayroll->pay_frequency }}</span></td>
                </tr>

            </table>
            <table class="inventory">
                <thead>
                <tr>
                    <th><span contenteditable>Item</span></th>
                    <th><span contenteditable>Description</span></th>
                    <th><span contenteditable>Quantity</span></th>
                    <th><span contenteditable>Price</span></th>
                    <th><span contenteditable>Amount</span></th>
                </tr>
                </thead>
                <tbody>
                <?php $subtotal = 0; ?>
                @if(!empty($businessPayroll->businessPayrollItem))
                    @foreach($businessPayroll->businessPayrollItem as $item)
                        <tr>
                            <td class="center"><span contenteditable>{{ !empty($item->businessPayItem) ? $item->businessPayItem->name : ''  }}</span></td>
                            <td class="center"><span contenteditable>{{ $item->description  }}</span></td>
                            <td class="center"><span contenteditable>{{ $item->quantity  }}</span></td>
                            <td class="center"><span data-prefix>$</span><span>{{ $item->price  }}</span></td>
                            <td class="center"><span data-prefix>$</span><span>{{ $item->amount  }}</span></td>
                        </tr>
                        <?php $subtotal += $item->amount; ?>
                    @endforeach
                @endif
                </tbody>
            </table>

            <table class="balance">
                <tr>
                    <th class="background"><span contenteditable>Total Amount</span></th>
                    <td><span data-prefix>$</span><span>{{ $subtotal  }}</span></td>
                </tr>
                <tr>
                    <th class="background"><span contenteditable>Tax</span></th>
                    <td><span data-prefix>%</span><span contenteditable>{{ !empty($businessPayroll->taxRate) ? $businessPayroll->taxRate->tax_rates : 0.00 }}</span></td>
                </tr>
                <tr>
                    <th class="background"><span contenteditable>Grand Total</span></th>
                    <td><span data-prefix>$</span><span>{{ $businessPayroll->amount  }}</span></td>
                </tr>
            </table>
        </article><br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

        <div class="thanks" style="text-align:center;"><p>**We Thank You For Your Business**</p></div>
    </div><!---wrapper-->
</div>

</body>
</html>