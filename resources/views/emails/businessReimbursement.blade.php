Dear {{ !empty($businessReimbursement->customer) ? $businessReimbursement->customer->first_name . ' ' . $businessReimbursement->customer->last_name : ''}},

<p>Thank you for your business.</p>
<p>You can now view, print or download Your Reimbursement {{ $businessReimbursement->reimbursement_no }} in a PDF file from the link that is given below. You can also choose to pay your bill online.</p>
<p><a href="{{ $appUrl }}">CLICK TO VIEW Reimbursement</a></p>
<p>We look forward to do more business with you.</p>

<p>Thanks for your Business!</p>
<p>Regards</p>

@if(!empty($businessReimbursement->businessProfile->logo))
    <p><img src="{{ asset($businessReimbursement->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
@endif

@if(!empty($businessReimbursement->businessProfile->name))
    <p>{{ $businessReimbursement->businessProfile->name }}</p>
@endif

@if(!empty($businessReimbursement->businessProfile->registered_address))
    <p>{{ $businessReimbursement->businessProfile->registered_address }} </p>
@endif