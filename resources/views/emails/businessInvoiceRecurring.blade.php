Dear {{ !empty($businessInvoice->customer[0])  ? $businessInvoice->customer[0]->first_name . ' ' . $businessInvoice->customer[0]->last_name : '' }},

<p>Here is your Invoice! Please find the attached invoice for your query.</p>
<p><a href="{{ $appUrl }}">CLICK HERE</a></p>
<p>Feel free to contact us.</p>
<p>We look forward to work with you.</p>
<p>Thanks for your business!</p>
<p>Regards,</p>

@if(!empty($businessInvoice->businessProfile->logo))
    <p><img src="{{ asset($businessInvoice->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
@endif

@if(!empty($businessInvoice->businessProfile->name))
    <p>{{ $businessInvoice->businessProfile->name }}</p>
@endif

@if(!empty($businessInvoice->businessProfile->registered_address))
    <p>{{ $businessInvoice->businessProfile->registered_address }}</p>
@endif