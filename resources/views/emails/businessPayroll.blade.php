Dear {{ !empty($businessPayroll->employee[0]) ? $businessPayroll->employee[0]->name : ''}},

<p>Thank you for your business.</p>
<p>You can now view, print or download Your payroll in a PDF file from the link that is given below. You can also choose to pay your bill online.</p>
<p><a href="{{ $appUrl }}">CLICK TO VIEW Payroll</a></p>
<p>We look forward to do more business with you.</p>

<p>Thanks for your Business!</p>
<p>Regards</p>

@if(!empty($businessPayroll->businessProfile->logo))
    <p><img src="{{ asset($businessPayroll->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
@endif

@if(!empty($businessPayroll->businessProfile->name))
    <p>{{ $businessPayroll->businessProfile->name }}</p>
@endif

@if(!empty($businessPayroll->businessProfile->registered_address))
    <p>{{ $businessPayroll->businessProfile->registered_address }} </p>
@endif