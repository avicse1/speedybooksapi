<?php 
$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0 auto; padding: 0; width: 70%; background-color: #fff;',
    'email-wrapper' => 'width: 100%; margin: 0; padding: 0; background-color: #fff;',

    /* Masthead ----------------------- */

    'email-masthead' => 'padding: 5px 0;background-color: #F5822A;',

    'email-body' => 'width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; background-color: #FFF;',
    'email-body_inner' => 'width: auto; width:100%; max-width: 100%; margin: 10px auto; padding: 0; border: 1px solid #ccc;  background: #eeeeee;',
    'email-body_cell' => 'padding: 30px 20px;',

    'email-footer' => 'width: auto; width:100%; max-width: 100%; margin: 0px auto; padding: 0 0 20px;background:#fff; font-size:12px;',

    /* Type ------------------------------ */

    'paragraph' => 'margin-top: 0; color: #444444; font-size: 15px; line-height: 1.5em;',  

    /* Buttons ------------------------------ */

    'button' => 'display: block; display: inline-block; width: 200px; min-height: 20px; padding: 10px;
                 background-color: #F5822A; border-radius: 3px; color: #ffffff; font-size: 15px; line-height: 25px;
                 text-align: center; text-decoration: none; -webkit-text-size-adjust: none;',

    /* table head */
    'table_head' => 'text-align: left;'
];

$fontFamily = 'font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;';
$alignRight = 'float:right;'

?>
<table width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="<?php echo $style['email-wrapper']; ?>" align="center">
            <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:15px">
                <!-- Logo -->
                <tr>
                    <td style="<?php echo $style['email-masthead']; ?>">
                        
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td style="<?php echo $style['email-body']; ?>" width="100%">
                        <table style="<?php echo $style['email-body_inner']; ?>" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td style="<?php echo $fontFamily; ?> <?php echo $style['email-body_cell']; ?>">

                                    <strong><p style="<?php echo $style['paragraph']; ?>">
                                        Dear {{ !empty($businessInvoice->customer[0])  ? $businessInvoice->customer[0]->first_name . ' ' . $businessInvoice->customer[0]->last_name : '' }},
                                    </p></strong>
                                    <p style="<?php echo $style['paragraph']; ?>">
                                     Please find the attached estimate for your query.  Feel free to contact us if you have any questions.  
                                    </p>

                                    <p style="<?php echo $style['paragraph']; ?>">
                                      We look forward to working with you.
                                    </p>

                                    <p style="<?php echo $style['paragraph']; ?>">
                                        <h3>
                                            @if(!empty($businessInvoice->businessProfile->name))
                                                <p>{{ $businessInvoice->businessProfile->name }}</p>
                                            @endif
                                        </h3>
                                        <table width="100%" cellpadding="0" cellspacing="0" style="margin-top:15px">
                                            <tr>
                                                <th style="<?php echo $style['table_head']; ?>">Estimate No.</th>
                                                <th style="<?php echo $style['table_head']; ?>">Expiry Date</th>
                                                <th style="<?php echo $style['table_head']; ?>">Amount</th>
                                            </tr>
                                            <tr>
                                                <td><p style="<?php echo $style['paragraph']; ?>">{{ $businessInvoice->invoice_no  }}</p></td>
                                                @if($businessInvoice->payment_term == 'Paid')
                                                    <td><p style="<?php echo $style['paragraph']; ?>"></p></td>
                                                @else
                                                    <td><p style="<?php echo $style['paragraph']; ?>">{{ \Carbon\Carbon::parse($businessInvoice->due_date)->format('Y-m-d') }}</p></td>
                                                @endif
                                                <td><p style="<?php echo $style['paragraph']; ?>">$ {{ $businessInvoice->amount }}</p></td>
                                            </tr>
                                        </table>
                                        <table width="100%" cellpadding="0" cellspacing="0" style="margin: 0 auto; width: 20%; margin-top: 20px">
                                            <tr>
                                                <td><a href="{{ $appUrl  }}" style="<?php echo $style['button'] ?>">View Estimate</a></td>
                                            </tr>
                                        </table>
                                    </p>
                                    <p style="<?php echo $style['paragraph']; ?>">
                                        Thank you for your Business!
                                    </p>
                                    <p style="<?php echo $style['paragraph']; ?>">
                                        <!-- @if(!empty($businessInvoice->businessProfile->logo))
                                            <p><img src="{{ asset($businessInvoice->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
                                        @endif -->
                                        @if(!empty($businessInvoice->businessProfile->name))
                                            <p>{{ $businessInvoice->businessProfile->name }}</p>
                                        @endif
                                        @if(!empty($businessInvoice->businessProfile->telephone_no))
                                            <p><strong>Phone#</strong> {{ $businessInvoice->businessProfile->telephone_no }}</p>
                                        @endif
                                        @if(!empty($businessInvoice->businessProfile->registered_address))
                                            <p>{{ $businessInvoice->businessProfile->registered_address }} </p>
                                        @endif
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Footer -->
                <tr>
                    <td style="background:#fff;">
                        <table style="<?php echo $style['email-footer']; ?>" align="center" width="570" cellpadding="0" cellspacing="0">
                            <tr>
                                <td><img src="{{ asset('images/logo.png') }}" style="width:150px; height: 50px;" >
                                </td>
                                <td align="right" style="<?php echo $fontFamily; ?>" style="<?php echo $alignRight; ?>"><?php echo date('Y'); ?> &copy; SpeedyBooks Inc. All Right Reserved</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
