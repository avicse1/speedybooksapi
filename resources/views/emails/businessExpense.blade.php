Dear {{ !empty($businessExpense->vendor[0]) ? $businessExpense->vendor[0]->first_name . ' ' . $businessExpense->vendor[0]->last_name : ''}},

<p>Thank you for your business.</p>
<p>You can now view, print or download Your expense in a PDF file from the link that is given below. You can also choose to pay your bill online.</p>
<p><a href="{{ $appUrl }}">CLICK TO VIEW EXPENSE</a></p>
<p>We look forward to do more business with you.</p>

<p>Thanks for your Business!</p>
<p>Regards</p>

@if(!empty($businessExpense->businessProfile->logo))
    <p><img src="{{ asset($businessExpense->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
@endif

@if(!empty($businessExpense->businessProfile->name))
    <p>{{ $businessExpense->businessProfile->name }}</p>
@endif

@if(!empty($businessExpense->businessProfile->registered_address))
    <p>{{ $businessExpense->businessProfile->registered_address }} </p>
@endif