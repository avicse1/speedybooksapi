Hello {{ $inviteUser->first_name  }},

<p>{{ $businessProfile->name  }} would like you to access their account at Speedybooks. {{ $businessProfile->name  }} uses Seedybooks to help manage their business.</p>

<p>{{ !empty($input['message']) ? $input['message'] : ''  }}</p>

<p>To accept this invitation please use the following link:</p>

<p>This link will expire in 24 hours</p>

<a href="{{ $signupUrl  }}">click here to signup SpeedyBooks</a>

<p>Regards</p>
<p><img src="{{ asset('/images/logo.png')  }} "></p>