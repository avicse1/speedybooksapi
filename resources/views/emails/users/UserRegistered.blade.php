<p>Dear {{ $user->first_name }} {{ $user->last_name }}</p>

<p>Click this link to create a password <a href="{{ $app_url }}createpassword?token={{ $user->password }}&email={{ $token }}">{{ $app_url }}createpassword?token={{ $user->password }}&email={{ $token }}</a></p>