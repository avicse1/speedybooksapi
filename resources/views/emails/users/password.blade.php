Dear {{ $name }},

<p>Welcome to SpeedyBooks!</p>
<p>This is a confirmation that the password for your SpeedyBooks Account {{ $email  }} has been changed.</p>
<p>New Password :-- {{ $password }}</p>

<p>To reset your password follow the steps.</p>
<p>1. Login to your SpeedyBooks Account</p>
<p>2. Click on your Displayed Name at the top right</p>
<p>3. From the drop-down Click on the ‘Company Account’</p>
<p>4. Click on Create password</p>
<p>5. Click on ‘Enter your Current Password’</p>
<p>6. Click on ‘New Password’</p>
<p>7. Finally, Click on ‘Confirm Password’</p>

<p>Thanks & Regards</p>
<p>SpeedyBooks Team</p>