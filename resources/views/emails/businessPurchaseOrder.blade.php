Dear {{ !empty($businessPurchaseOrder->vendor) ? $businessPurchaseOrder->vendor->first_name . ' ' . $businessPurchaseOrder->vendor->last_name : ''}},

<p>Please find our purchase order attached to this email.</p>
<p>Feel free to contact us if you have any questions.</p>

<p>Thanks for your Business!</p>
<p>Regards</p>

@if(!empty($businessPurchaseOrder->businessProfile->logo))
    <p><img src="{{ asset($businessPurchaseOrder->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
@endif

@if(!empty($businessPurchaseOrder->businessProfile->name))
    <p>{{ $businessPurchaseOrder->businessProfile->name }}</p>
@endif

@if(!empty($businessPurchaseOrder->businessProfile->registered_address))
    <p>{{ $businessPurchaseOrder->businessProfile->registered_address }} </p>
@endif