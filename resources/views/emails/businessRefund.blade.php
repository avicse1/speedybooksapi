Dear {{ !empty($businessRefund->customer[0])  ? $businessRefund->customer[0]->first_name . ' ' . $businessRefund->customer[0]->last_name : '' }},

<p>Here is your Refund Receipt!</p>
<p>Please find the attachment below:</p>

<p>Thanks for your business!</p>
<p>Regards,</p>

@if(!empty($businessRefund->businessProfile->logo))
    <p><img src="{{ asset($businessRefund->businessProfile->logo) }}" style="width:100px; height: 50px;" ></p>
@endif

@if(!empty($businessRefund->businessProfile->name))
    <p>{{ $businessRefund->businessProfile->name }}</p>
@endif

@if(!empty($businessRefund->businessProfile->registered_address))
    <p>{{ $businessRefund->businessProfile->registered_address }}</p>
@endif