<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Aged Receivables Report</h4>
        <br/><br/>
        <div class="expenses">
            <div class="table-responsive">
                <table class="table table-bordered ">
                    <thead class="">
                    <tr>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>{{ $currentMonth  }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($agedReceviables as $data)
                    <tr>
                        <td>{{ $data['date'] }}</td>
                        <td>
                            {{ $data['transactionNo'] }}
                        </td>
                        <td>{{ $data['customer'] }}</td>
                        <td>{{ number_format($data['total'], 2, '.', ',') }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="3">Total Receviables</td>
                        <td>${{ number_format($totalReceviables, 2, '.', ',') }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
    }
</style>