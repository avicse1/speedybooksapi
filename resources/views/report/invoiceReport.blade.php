<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Customer Invoice Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
     <br/><br/>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Invoice No</th>
                <th>Reference</th>
                <th>Type</th>
                <th>To</th>
                <th>Date</th>
                <th>Due Date</th>
                <th>Status</th>
                <th>Invoice Total</th>
            </tr>
            </thead>
            <tbody>
            @if(count($invoices) == 0)
            <tr>
                <td colspan="8">No Record Found</td>
            </tr>
            @endif

            @if(count($invoices) > 0)
            @foreach($invoices as $invoice)
            <tr>
                <td>{{ $invoice->invoice_no }}</td>
                <td>{{ $invoice->reference }}</td>
                <td>{{ $invoice->type }}</td>
                <td>{{ $invoice->customer[0]->first_name }}</td>
                <td>{{ $invoice->date }}</td>
                <td>{{ $invoice->date }}</td>
                <td>{{ $invoice->status }}</td>
                <td>{{ $invoice->amount }}</td>
            </tr>
             @endforeach
                @endif
            </tbody>
            <tfoot>
            <tr>
                <th id="total" colspan="7">Total :</th>
                <td><strong>${{ $total }}</strong></td>
            </tr>
            </tfoot>
        </table>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
        font-size: 13px;
    }
</style>