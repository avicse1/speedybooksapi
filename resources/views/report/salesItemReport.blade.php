<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Sales By Items Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
    <br/><br/>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Items</th>
                <th>Current Unit Price</th>
                <th>Quantity Sold</th>
                <th>Total</th>
                <th>Average</th>
            </tr>
            </thead>
            <tbody>
            @if(count($items) == 0)
            <tr>
                <td colspan="5">No Record Found</td>
            </tr>
            @endif
            @if(count($items) > 0)
                @foreach($items as $item)
            <tr>
                <td>{{ $item['name_code'] }}</td>
                <td>{{ $item['current_price'] }}</td>
                <td>{{ $item['quantity'] }}</td>
                <td>{{ $item['total'] }}</td>
                <td>{{ $item['total'] / $item['quantity'] }}</td>
            </tr>
                @endforeach
                @endif
            </tbody>
            <tfoot>
            <tr>
                <th id="total" colspan="3">Total :</th>
                <td><strong>${{ $totalAmount }}</strong></td>
                <td><strong>${{ round($totalAverage, 2) }} </strong></td>
            </tr>
            </tfoot>
        </table>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
    }
</style>