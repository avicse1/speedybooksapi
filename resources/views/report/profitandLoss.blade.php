<section class="panel panel-default" style="width: 80%; margin: auto;">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Profit and Loss Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br /> <br />
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12"><strong>Income</strong></div>
                    <div class="row">
                        @foreach($profitandLoss['income'] as $key=>$value)
                        <div class="col-md-12">
                            <span class="left-content-head">{{ $key }}</span>
                            @foreach($value as $account=>$amount)
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="left-content">{{ $account }}</span>
                                    <span class="left-content-value">{{ number_format($amount, 2, '.', ',') }}</span>
                                </div>
                            </div>
                                @endforeach
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <hr />
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Total Income</strong></span>
                            <span style="text-align: right;position: absolute;left: 100%;">${{ number_format($totalIncome, 2, '.', ',') }}</span>
                        </div>
                    </div>
                    <br />
                    <!-- <div class="row">
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Gross Profit</strong></span>
                            <span style="text-align: right;position: absolute;left: 100%;">{{ $totalIncome }}</span>
                        </div>
                    </div>
                </div>
                <br/> -->
                <div class="row">
                    <div class="col-md-12"><strong>Expense</strong></div>
                    <div class="row">
                        @foreach($profitandLoss['expense'] as $key=>$value)
                        <div class="col-md-12">
                            <span class="left-content-head">{{ $key }}</span>
                            @foreach($value as $account=>$amount)
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="left-content">{{ $account }}</span>
                                    <span class="left-content-value">{{ number_format($amount, 2, '.', ',') }}</span>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endforeach
                    </div>
                    <hr />
                    <div class="row">
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Total Expense</strong></span>
                            <span style="text-align: right;position: absolute;left: 100%;">${{ number_format($totalExpense, 2, '.', ',') }}</span>
                        </div>
                    </div>
                    <br/>
                    @if($totalIncome > $totalExpense)
                    <div class="row">
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Net Profit</strong></span>
                            <span style="text-align: right;position: absolute;left: 100%;">${{ number_format($totalIncome - $totalExpense, 2, '.', ',') }}</span>
                        </div>
                    </div>
                    @endif
                    <br/>
                    @if($totalIncome < $totalExpense)
                    <div class="row">
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Net Loss</strong></span>
                            <span style="text-align: right;position: absolute;left: 100%;">${{ number_format($totalIncome - $totalExpense, 2, '.', ',') }}</span>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    .left-content
    {
        margin-left: 100px; text-align: left;
    }
    .left-content-value
    {
        text-align: right; position: absolute; left: 100%;
    }
    .left-content-head
    {
        margin-left: 50px;
    } 
</style>