<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Invoice Receivables Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br/><br/>
        <div class="expenses">
            <div class="table-responsive">
                <table class="table table-bordered ">
                    <thead class="">
                    <tr>
                        <th>Date</th>
                        <th>Account</th>
                        <th>Type</th>
                        <th>Transaction</th>
                        <th>Reference</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Net Amount</th>
                        <th>Tax Amount</th>
                        <th>Discount</th>
                        <th>Tax Rate</th>
                        <th>Tax Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($invoiceReceivables as $data)
                    <tr>
                        <td>{{ $data['date'] }}</td>
                        <td>{{ $data['account'] }}</td>
                        <td>{{ $data['invoice_no'] }}</td>
                        <td>{{ $data['transaction'] }}</td>
                        <td>{{ $data['reference'] }}</td>
                        <td>{{ $data['quantity'] }}</td>
                        <td>{{ $data['unit_price'] }}</td>
                        <td>{{ $data['net_amount'] }}</td>
                        <td>{{ $data['tax_amount'] }}</td>
                        <td>{{ $data['discount'] }}</td>
                        <td>{{ $data['tax_rate'] }}</td>
                        <td>{{ $data['tax_name'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="7"><strong>Total</strong></td>
                        <td><strong>${{ $totalAmount }}</strong></td>
                        <td colspan="4"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
        font-size: 11px;
    }
</style>