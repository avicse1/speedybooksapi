<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Trial Balance Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br/><br/>
        <div class="panel panel-default">
            <div class="panel-body">
            <table>
                <thead>
                    <tr>
                        <th></th>
                        <th>Debit</th>
                        <th>Credit</th>
                    </tr>
                </thead>
                @foreach($trailBalanceData as $key=>$value)
                <tbody>
                    <tr>
                        <th colspan="3">{{ $key }}</th>
                    </tr>
                    @foreach($value as $k=>$v)
                    <tr>
                        <td style="padding-left: 50px;">{{ $v['name'] }}</td>
                        <td>{{ $v['debit'] }}</td>
                        <td>{{ $v['credit'] }}</td>
                    </tr>
                     @endforeach

                </tbody>
                @endforeach
                <tr><td colspan="3"><hr/></td></tr>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <td>{{ $totalDebit }}</td>
                        <td>{{ $totalCredit }}</td>
                    </tr>
                </tfoot>
            </table>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        /*border: 1px solid black;*/
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
    }
</style>