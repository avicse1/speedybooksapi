<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> General Ledger Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br/><br/>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Account</th>
                <th>Debit</th>
                <th>Credit</th>
            </tr>
            </thead>
            <tbody>
            @foreach($generalLedgerData as $data)
            <tr>
                <td>{{ $data['name'] }}</td>
                <td>{{ $data['debit'] }}</td>
                <td>{{ $data['credit'] }}</td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th id="total">Total :</th>
                <th>{{ $totalDebit }}</th>
                <th>{{ $totalCredit }}</th>
            </tr>
            </tfoot>
        </table>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
    }
</style>