<section>
<div class="panel-body">
    <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
    <h4 class="pdf-text-center"> Aged Payables Report</h4>
    <br /> <br />
    <div class="expenses">
        <div class="table-responsive">
            <table class="table table-bordered ">
                <thead class="">
                <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>{{ $currentMonth  }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($agedPayables as $data)
                <tr>
                    <td>{{ $data['date'] }}</td>
                    <td>{{ $data['vendor']}}</td>
                    <td>{{ number_format($data['total'], 2, '.', ',') }}</td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="2">Total Payables</td>
                    <td>${{ number_format($totalPayables, 2, '.', ',') }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
    }
</style>