<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Journal Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br/><br/>
        <div class="expenses">
            <div class="table-responsive">
                <table class="table table-bordered ">
                    <thead class="">
                    <tr>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Name</th>
                        <th>Account</th>
                        <th>Debit</th>
                        <th>Credit</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($journalReport as $data)
                    <tr>
                        <td>{{ $data['date'] }}</td>
                        <td>{{ $data['type'] }}</td>
                        <td>{{ $data['name'] }}</td>
                        <td>{{ $data['account'] }}</td>
                        <td>{{ $data['debit'] }}</td>
                        <td>{{ $data['credit'] }}</td>
                    </tr>
                        @endforeach
                    <tr>
                        <td colspan="4"><strong>Total</strong></td>
                        <td><strong>${{ $totalDebit }}</strong></td>
                        <td><strong>${{ $totalCredit }}</strong></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
        font-size: 13px;
    }
</style>