<section class="panel panel-default" style="width: 70%; margin: auto;">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Cash Summary Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br /> <br />
        <div class="panel panel-default" >
            <div class="panel-body">
                <table>
                    <thead>
                        <tr>
                            <th></th>
                            <th style="text-align: right;">Amount</th>
                        </tr>
                    </thead>
                    @foreach($incomeCashSummary as $key=>$value)
                        <tr>
                            <th colspan="2">{{ $key }}</th>
                        </tr>
                        @foreach($value as $k=>$v)
                        <tr>
                            <td style="padding-left: 50px;">{{ $k }}</td>
                            <td  style="text-align: right;">{{ number_format($v, 2, '.', ',') }}</td>
                        </tr>
                        @endforeach
                    @endforeach
                    <tr>
                        <th>Total Income</th>
                        <th style="text-align: right;">${{ number_format($totalIncome, 2, '.', ',') }}</th>
                    </tr>
                    @foreach($expenseCashSummary as $key=>$value)
                        <tr>
                            <th colspan="2">{{ $key }}</th>
                        </tr>
                        @foreach($value as $k=>$v)
                        <tr>
                            <td style="padding-left: 50px;">{{ $k }}</td>
                            <td style="text-align: right;">{{ number_format($v, 2, '.', ',') }}</td>
                        </tr>
                        @endforeach
                    @endforeach
                    <tr>
                        <th>Total Expense</th>
                        <th style="text-align: right;">${{ number_format($totalExpense, 2, '.', ',') }}</th>
                    </tr>
                    <tr>
                        <th>Net Cash</th>
                        <th style="text-align: right;">${{ number_format($totalIncome - $totalExpense, 2, '.', ',') }}</th>
                    </tr>
                    <tr>
                        <th>Cash Account</th>
                        <th style="text-align: right;">${{ number_format($totalIncome - $totalExpense, 2, '.', ',') }}</th>
                    </tr>
                </table>                
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        /*border: 1px solid black;*/
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
        font-size: 14px;
    }
</style>