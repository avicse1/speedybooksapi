<section class="panel panel-default">
    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Tax Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br/><br/>
        <div class="panel panel-default">
            <div class="panel-body">
            <table>
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Account</th>
                        <th>Transaction</th>
                        <th>Net Amount</th>
                        <th>Tax</th>
                        <th>Gross</th>
                    </tr>
                </thead>
                @foreach($taxReport as $key=>$value)
                <tbody>
                    <tr>
                        <th colspan="6">{{ $key }}</th>
                    </tr>
                     @foreach($value as $k=>$v)
                        <tr>
                            <td>{{ $v['date'] }}</td>
                            <td>{{ $v['account'] }}</td>
                            <td>{{ $v['transaction'] }}</td>
                            <td>${{ $v['net_amount'] }}</td>
                            <td>{{ $v['tax'] }}</td>
                            <td>${{ $v['gross'] }}</td>
                        </tr>
                     @endforeach
                </tbody>
                @endforeach
            </table>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        /*border: 1px solid black;*/
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
        font-size: 13px;
    }
</style>