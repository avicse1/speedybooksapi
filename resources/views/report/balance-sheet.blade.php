
<section style="width: 80%; margin: auto;">
    <div class="panel-body">
        <h2 class="text-center pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="text-center pdf-text-center"> Balance Sheet Report</h4>
        <h3 class="text-center pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>
        <br /> <br />
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12"><strong>Assets</strong></div>
                    <div class="row">
                        @foreach($balanceSheetAssets as $key=>$value)
                        <div class="col-md-12">
                            <span class="left-content-head" >{{ $key }}</span>
                            @foreach($value['account'] as $account=>$amount)
                            <div class="row">
                                <div>
                                    <span class="left-content" >{{ $account }}</span>
                                    <span class="left-content-value">{{ number_format($amount, 2, '.', ',') }}</span>
                                </div>
                               
                            </div>
                            @endforeach
                            <div class="row">
                                <div>
                                    <span class="left-content">Total {{ $key }}</span>
                                     <span class="left-content-value">{{ number_format($value['total'], 2, '.', ',') }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <hr/>
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Total Assets</strong></span>
                             <span style="text-align: right;position: absolute;left: 100%; font-weight: bold">${{ number_format($totalAssets, 2, '.', ',') }}</span>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <br />

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12"><strong>Liabilities</strong></div>
                    <div class="row">
                        @foreach($balanceSheetLiabilities as $key=>$value)
                        <div class="col-md-12">
                            <span class="left-content-head">{{ $key }}</span>
                            @foreach($value['account'] as $account=>$amount)
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="left-content">{{ $account }}</span>
                                    <span class="left-content-value">{{ number_format($amount, 2, '.', ',') }}</span>
                                </div>                                
                            </div>
                            @endforeach
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="left-content">Total {{ $key }}</span>
                                    <span class="left-content-value">{{ number_format($value['total'], 2, '.', ',') }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <hr/>
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Total Liabilities</strong></span>
                             <span style="text-align: right;position: absolute;left: 100%; font-weight: bold;">${{ number_format($totalLiabilities, 2, '.', ',')
                              }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />

        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12"><strong>Owner's Capital</strong></div>
                    <div class="row">
                        @foreach($balanceSheetOwners as $key=>$value)
                        <div class="col-md-12">
                            <span class="left-content-head">{{ $key }}</span>
                            @foreach($value['account'] as $account=>$amount)
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="left-content">{{ $account }}</span>
                                    <span class="left-content-value">{{ number_format($amount, 2, '.', ',') }}</span>
                                </div>
                            </div>
                            @endforeach

                            @if($netProfit > 0)
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="left-content">Net Profit</span>
                                         <span class="left-content-value">{{ number_format($netProfit, 2, '.', ',') }}</span>
                                    </div>
                                </div>
                            @endif

                                @if($netProfit < 0)
                                    <div class="row">
                                        <div class="col-md-6">
                                            <span class="left-content">Net Loss</span>
                                             <span class="left-content-value">{{ number_format($netProfit, 2, '.', ',') }}</span>
                                        </div>
                                    </div>
                                @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <span class="left-content">Total {{ $key }}</span>
                                    <span class="left-content-value">{{ number_format($value['total'] + $netProfit, 2, '.', ',') }}</span>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <hr/>
                        <div class="col-md-6">
                            <span style="text-align: left;"><strong>Total Owner's Capital</strong></span>
                            <span style="text-align: right;position: absolute;left: 100%; font-weight: bold;">${{ number_format($totalOwners, 2, '.', ',') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    .left-content
    {
        margin-left: 100px; text-align: left;
    }
    .left-content-value
    {
        text-align: right; position: absolute; left: 100%;
    }
    .left-content-head
    {
        margin-left: 50px;
    } 
</style>