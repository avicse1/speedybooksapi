<section class="panel panel-default" >

    <div class="panel-body">
        <h2 class="pdf-text-center">{{ $businessProfile->name }}</h2>
        <h4 class="pdf-text-center"> Accounting Transaction Report</h4>
        <h3 class="pdf-text-center">{{ $date['from'] }} - {{ $date['to'] }}</h3>

        <div class="expenses">
            <div class="table-responsive">
                <table class="table table-bordered ">
                    <thead class="">
                    <tr>
                        <th>Date</th>
                        <th>Account</th>
                        <th>Type</th>
                        <th>Transaction</th>
                        <th>Reference</th>
                        <th>Net Amount</th>
                        <th>Tax Amount</th>
                        <th>Gross</th>
                        <th>Tax Rate</th>
                        <th>Tax Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accountingTransactions as $data)
                    <tr>
                        <td>{{ $data['date'] }}</td>
                        <td>{{ $data['account'] }}</td>
                        <td>{{ $data['type'] }}</td>
                        <td>{{ $data['transaction'] }}</td>
                        <td>{{ $data['reference'] }}</td>
                        <td>{{ number_format($data['net_amount'], 2, '.', ',')}}</td>
                        <td>{{ number_format($data['tax_amount'], 2, '.', ',') }}</td>
                        <td>{{ number_format($data['gross'], 2, '.', ',') }}</td>
                        <td>{{ $data['tax_rate'] }}</td>
                        <td>{{ $data['tax_name'] }}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td colspan="5"><strong>Total</strong></td>
                        <td><strong>${{ number_format($totalAmount, 2, '.', ',')  }}</strong></td>
                        <td></td>
                        <td><strong>${{ number_format($totalGrossAmount, 2, '.', ',') }}</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>
<footer style="position: absolute; bottom: 0; float: right; ">
    Copyright &copy; {{date('Y')}} SpeedyBooks, All rights reserved.
</footer>
<style type="text/css">
    .pdf-text-center
    {
        text-align: center;
        color: #E5721A;
    }   
    table, td, th {
        border: 1px solid black;
    }
   table {
        border-collapse: collapse;
        width: 100%;
    }

    th {
        text-align: left;
        padding: 5px;
    }
    table, td
    {
        padding: 5px;
        font-size: 11px;
    }
</style>